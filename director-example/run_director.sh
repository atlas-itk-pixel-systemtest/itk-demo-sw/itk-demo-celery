#!/usr/bin/env bash
echo
env
echo

until pg_isready -d director -h postgres; do
    if [ $? -eq 2 ]; then
        echo "waiting for Postgres db ..."
    fi
done

director db upgrade
#director celery multi start 4 --loglevel=info --concurrency=1 # && tail -f /var/log/celery/*.log
director celery worker --loglevel=INFO -B
director celery worker --loglevel=INFO -B

