# celery-director-example

## Launch the stack


- Set the `DB_PASSWORD` variable in `config`
- Generate `.env`
```
./config
```
- Create the stack
```bash
docker compose up -d --force-recreate
```

If you want to see the logs
```bash
docker compose logs
```

## Run a workflow

Here is a curl example:

```bash
curl --location --request POST 'http://127.0.0.1:8000/api/workflows' \
--header 'Content-Type: application/json' \
--data-raw '{
  "project": "example",
  "name": "ETL",
  "payload": {}
}'
```

