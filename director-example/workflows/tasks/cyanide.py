# import cyanide
# import time
# from tasks import add, reverse,is_prime

# class CeleryTestApp(cyanide.CyanideApp):
#     def high_load_test(self):
#         # Simuler une charge élevée en envoyant de nombreuses tâches
#         for _ in range(1000):
#             add.delay()

#     def spike_load_test(self):
#         # Simuler un pic de charge en envoyant un grand nombre de tâches en même temps
#         for _ in range(100):
#             add.delay()

#     def long_execution_test(self):
#         # Simuler une longue durée d'exécution en effectuant un travail intensif dans une tâche
#         reverse.delay("okok")

# if __name__ == "__main__":
#     app = CeleryTestApp("interface.yaml")
#     app.run()
