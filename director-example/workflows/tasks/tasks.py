from director import task
import time

@task(name="ADD2")
def add(*args, **kwargs):
    payload = kwargs["payload"]
    x = payload.get("x", 2)
    y = payload.get("y", 3)
    time.sleep(2)
    return x + y

@task(name="REVERSE")
def reverse(*args, **kwargs):
    text = kwargs.get("text", "")
    return text[::-1]

@task(name="IS_PRIME")
def is_prime(*args, **kwargs):
    n = args[0]
    if n <= 1:
        return False
    elif n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True

@task(name="C")
def c(*args, **kwargs):
    print(args[0])

# @task
# def add3(payload):
#     # Traitement du payload de la troisième étape
#     print("Payload reçu pour la troisième étape :", payload)
#     # Votre logique de traitement ici

# # Définissez une route dans votre application Flask pour ajouter le payload à la troisième étape
# @route('/add_payload_to_third_step', methods=['POST'])
# def add_payload_to_third_step():
#     # Récupérez le payload depuis la requête HTTP
#     payload = request.json
#     # Envoyez la tâche vers Celery pour la troisième étape
#     add3.delay(payload)
#     return 'Payload ajouté avec succès à la troisième étape.'

# if __name__ == '__main__':
#     app.run(debug=True)