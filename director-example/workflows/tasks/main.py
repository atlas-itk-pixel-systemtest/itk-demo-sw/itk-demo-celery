# tasks/example.py
from director import task

@task(bind=True, name="BOUND_TASK")
def bound_task(self, *args, **kwargs):
    print(self.name)
