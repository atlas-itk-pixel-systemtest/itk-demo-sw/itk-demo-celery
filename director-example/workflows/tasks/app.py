# # -*- coding: utf-8 -*-
# # Import des modules Flask et requests
# from flask import Flask, render_template, request
# import requests

# # Création de l'application Flask
# app = Flask(__name__)

# # URL de l'API Celery Director pour démarrer un workflow
# CELERY_DIRECTOR_URL = "http://http://localhost:8000/#/definitions"

# # Route pour la page d'accueil avec le formulaire de sélection de tâche
# @app.route('/', methods=['GET', 'POST'])
# def index():
#     if request.method == 'POST':
#         # Récupération de la tâche sélectionnée depuis le formulaire
#         selected_task = request.form['selected_task']
#         # Envoi de la demande à Celery Director pour démarrer le workflow à partir de la tâche sélectionnée
#         response = requests.post(CELERY_DIRECTOR_URL, json={"workflow_name": "example.ONE_PARENT", "starting_task": selected_task})
#         if response.status_code == 200:
#             return "Workflow démarré avec succès à partir de la tâche : " + selected_task
#         else:
#             return "Échec du démarrage du workflow."
#     else:
#         # Liste des tâches disponibles dans le workflow (à remplacer par votre propre liste)
#         tasks = ["ADD2", "IS_PRIME", "C"]
#         return render_template('index.html', tasks=tasks)

# # Exécution de l'application Flask
# if __name__ == '__main__':
#     app.run(debug=True)
