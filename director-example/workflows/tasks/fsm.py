from director import task
import requests


# FSM

PORT=8000
fsm_url=f"http://fsm:{PORT}"

@task(name="TRIGGER")
def trigger(*args, **kwargs):
# def trigger(state_event='probe'):
    print("Trigger state event")
    r = requests.post(f'{fsm_url}/trigger/{state_event}')
    rj = r.json()
    return rj

@task(name="MARKUP")
def markup(*args, **kwargs):
    print("Getting markup")
    r = requests.get(f'{fsm_url}/markup')
    rj = r.json()
    return rj
