# tasks/orders.py
from director import task
from .utils import Order, Mail


@task(name="ORDER_PRODUCT")
def order_product(*args, **kwargs):
    order = Order(
        user=kwargs["payload"]["user"], product=kwargs["payload"]["product"]
    ).save()
    return {"id": order.id}


@task(name="SEND_MAIL")
def send_mail(*args, **kwargs):
    order_id = args[0]["id"]
    mail = Mail(
        title=f"Your order #{order_id} has been received",
        user=kwargs["payload"]["user"],
    )
    mail.send()
