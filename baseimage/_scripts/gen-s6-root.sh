#!/usr/bin/env bash

touch2() { mkdir -p "$(dirname "$1")" && touch "$1" ; }

gen_service() {
    _root=_root/etc/s6-overlay/s6-rc.d
    root=/etc/s6-overlay/s6-rc.d
    service=$1
    touch2 ${_root}/init-${service}/dependencies.d/init-config
    (
        # write configuration script
        cd ${_root}/init-${service} || return
        echo "oneshot" | tee type
        echo "${root}/init-${service}/run" > up
        cat <<-EOF >run
#!/usr/bin/with-contenv bash
echo [init-${service}] run
env
EOF
        chmod +x run
    )
    touch2 ${_root}/init-config-end/dependencies.d/init-${service}
    touch2 ${_root}/svc-${service}/dependencies.d/init-services
    (
        # write service script
        cd ${_root}/svc-${service} || return
        echo "longrun" | tee type
        echo "${root}/svc-${service}/run" > up
        cat <<-EOF >run
#!/bin/sh
echo [svc-${service}] run
exec 2>&1
exec s6-setuidgid itk env
EOF
        chmod +x run
        echo "${root}/svc-${service}/finish" > down
        cat <<-EOF >finish
#!/usr/bin/with-contenv bash
echo [svc-${service}] finish
ls -R /var/log
cat /var/log/${service}/*
EOF
        chmod +x finish
        echo log-${service} > producer-for
    )
    # register service with user bundle
    touch2 ${_root}/user/contents.d/init-${service}
    touch2 ${_root}/user/contents.d/svc-${service}

    # log-prepare
    touch2 ${_root}/log-prepare-${service}/dependencies.d/base
    (
        cd ${_root}/log-prepare-${service} || return
        echo "oneshot" | tee type
        cat <<-EOF >up
if { mkdir -p /var/log/${service} }
if { chown nobody:nobody /var/log/${service} }
chmod 02755 /var/log/${service}
EOF
    )
    # log
    touch2 ${_root}/log-${service}/dependencies.d/log-prepare-${service}
    (
        cd ${_root}/log-${service} || return
        echo "longrun" | tee type
        echo "${service}-pipeline" > pipeline-name
        echo "svc-${service}" > consumer-for
        cat <<-EOF > run
#!/bin/sh
exec logutil-service /var/log/${service}
EOF
        chmod +x run
    )
    touch2 ${_root}/user/contents.d/${service}-pipeline
}

#gen_service worker0
# ls -R _root
# cat ${_root}/init-worker0/run

"$@"
