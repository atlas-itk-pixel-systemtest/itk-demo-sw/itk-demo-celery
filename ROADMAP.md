# Roadmap for the itk-demo-celery Celery demonstration

Test scenarios and benchmarking / stress testing suite for Detector Microservices workflows.

## Optional insertion of Celery between Frontend and Backend

For testing and small setups like SCC setups making celery optional is desirable.
We can do this by interposing an *adapter service* to translate requests between 
frontend and backend.
Assuming the following conventions for frontend and backend:

**Frontends** call services *asynchronously* using the endpoint convention

* for commands: `[service]/[queue]/[trigger]/[subtrigger]?[params]`
* for queries: `[service]/[queue]/[query]/[subquery]?[params]`
(example: `felix/flx0-dev0/info/power`)

**Backends** call backend functions (i.e. trigger their FSM) *synchronously*.

To propagate the endpoint calls to the correct backend, an **Adapter Server** is needed.

* This can be a simple request translation server in the case of direct calls on local host.

```
FE /felix/flx0-dev0/info/power --> Adapter http://flx0/felix/dev0/info/power --> BE
```

* Or a full controller in case of more complex multi-target queues using celery.
In this case, the requests can be sent to celery using `apply_async()` tasks calls, 
or using requests to the Flower REST API.

### Celery middleware
For the insertion of celery between frontend and backend the opology package was created. It includes a python script that creates the docker compose files needed to start the celery middleware, or the request translation server.

Instructions for a first version of this package are available via the topology/README.md. But further improvements are needed.

#### TODOs
- Add compose file with useful tools like dozzle, portainer and flower
     - Also redis & rabbitmq which are required to run the middleware
- Seperate backend containers into their own compose file (currently they are part of the celery compose file)
- Bug?: Additional worker is added to celery compose file (in addition to the ones defined in the settings yaml)
     - Should probably be removed as this worker is subscribed to all queues and therefor bypasses the queue restrictions from the settings file
- Progress bar
- Improved support for backends on different hosts

## Test Cluster Models

### General points about organisation of the models

Celery models (worker test clusters) are a full-scale simulation of specific setups planned in ITk construction to develop and study the various workflows needed to configure and qualify the teststands.
The models should be set up completely independently from each other (subdirectory / configuration file for each). 
Ideally it should be possible to simply replace the test suites by "real" backends to add Celery to any microservice.

The configuration should contain:
- Worker configuration 
    - Mostly just Celery configuration passed to the Celery app via `.env` file.
    - This includes [message routing / queues configuration](https://docs.celeryq.dev/en/stable/userguide/configuration.html#message-routing).
        - Shared queues (for computing workload backends) *and* direct queues (for hardware backends) should be supported.
        - For shared queues deployment to physical nodes and to OpenShift should be supported.
- Container configuration
    - `compose.yaml` (or, if neccessary, input to `compose.yaml` generation?).
- Targets (nodes) configuration (eg. via Docker contexts?) in case of multiple nodes.
- OpenShift configuration in case of deployment to OpenShift.

The test clusters should be run completely independently from the task clients (both "real" backends and benchmarking / stress testing suite backends) because they should not have to depend on the actual backend code (and it's dependencies like eg. the FELIX SW).
But since the clients have to use task signatures compatible with the backends this could be achieved either 
- Through dependency inversion of the task definitions,
[implemented as abstract tasks like described in the Celery manual](https://docs.celeryq.dev/en/stable/userguide/application.html#abstract-tasks).
- Another option is to always use the generic signature `(*args, **kwargs)` (this solution is used by Celery Director).

In the case of lightweight tasks that just do HTTP requests for example, the dependency inversion may be unneccessary because the tasks only depend on eg. the `httpx` or `requests` package.

Tasks should be loadable at container startup, not only at image build time (--> mostly implemented in `celeryimage`, uses `include` Celery setting and needs the modules mounted into the container).

Celery Director compatibility of the models should be investigated.

Specific scenarios to set up + test:

### FELIX Scenario

Remote FSM Operation

- Tasks send *snychronous* HTTP requests to (dummy) FSM server to *control* FELIX.
    - Use `requests` or [httpx](https://www.python-httpx.org/).
    - No other workload in task itself (but should handle HTTP request failures).
- Tasks send asnychronous HTTP requests to (dummy) FSM server to *monitor* FELIX.
- If one of the HTTP requests fails 
    - Retry *N* times in case of server unreachable (`40x` HTTP return code)
        - implement circuitbreaker?
- Implement chain of task for auto-piloting FSM
    - e.g. Standard operation:
        - control FELIX: probe, init, configure, start dataflow apps
        - then monitor FELIX, repeated calls to info?
            - use Celery beat? or can we avoid polling?
    - Cancel rest of the chain in case of backend failure (`50x` HTTP return code) after one of the tasks.

(maybe check why "webhooks" are not mentioned so much in Celery now as they were in the beginning)

### Optoboard Scenario

- Tasks directly synchronously run different optoboard_FELIX functionality
    - [Already iplemented here](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard/-/blob/master/api/itk_demo_optoboard/worker/celeryTasks.py?ref_type=heads)
- Check with Bern group to factor out Celery functionality.
    - Separate client from backend worker.
    - Separate worker and use standard configs (see above).
- Update to new FSM architecture? (test)

### DAQ Style

- Test scenario tuning:
    - Check with YARR team and with itk-demo-felix team for realistic workloads
    - For LLS model needs to support 4x36 processes with 3 threads.
    - Load 50% - 100% CPU per process.
    - I/O to be confirmed.
    - 10 tasks chained with duration 1s, 10s, 100s.
- Check what is already in itk-demo-daqapi.
    - Factor out.
    - Update to standard config (see above).

### QC Analysis Style

- Check if and how to partition analysis into individual tasks.
- Obtain real example of data to analyse.
- Check duration and CPU load.
- Scale to 36 tasks.
- Deploy to OpenShift?

## Benchmarking

### Celery overhead

Goal is to measure the pure time of Celery task roundtrips.

- Total time for a given number of tasks (noop and with increasing load x time).
- Task rate for large number of noop tasks.

Baseline is one container with one worker per node. 
check if more containers / worker can accelerate treatment of tasks without negative impact
(eg. on memory):
- optimize number of workers. 
    - check if tasks are treated faster if more than one worker is used.
    - check if more workers or more containers uses less memory.
- optimize prefetch (`worker_prefetch_multiplier`).

### Speed of task processing

Metrics:

Description | Measurement
|--|--|
Total time | time before sending tasks (`t.apply_async()`) to retrieving all results (`tid.get()`) |
Time to populate MQ |
Time for all tasks to SUCCEED |

### Memory Consumption

check eg. with `celery inspect` or `docker info` or just `dstat`.

### I/O Performance

write some large (O(100)MB) files from several tasks and check if the threading pool performs better than prefork pool.

### Stress testing suite (DeMiCe)

This is a suite to test error / error recovery handling.
DeMiCe is based on a copy of Celery Cyanide.
In order to use it some work is needed:

- Upgrade to Celery 5
- Remove Support for Python 2
- Remove Vagrant (replaced by Docker stacks)
- Move configuration templates to externally generated in config scripts as environment variables and loaded from .env file)

Effectively the end result should become the benchmarking client



