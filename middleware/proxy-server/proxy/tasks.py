import httpx
import pybreaker
import redis
import time
import os
import requests

SR_URL = os.environ.get("SR_URL", "http://localhost:5111")



# Defining circuit breaker
backend_redis = redis.StrictRedis().from_url("redis://redis:6379/0")
task_breaker = pybreaker.CircuitBreaker(
    fail_max=5,
    reset_timeout=5,
    state_storage=pybreaker.CircuitRedisStorage(pybreaker.STATE_CLOSED, backend_redis),
)

def unique_task(url, key, path, type, query_params, body):
    
    if not url:
        url = get_url(key)

    task_name = "/" + path.strip("/")
    TASK_URL = url + f"{task_name}"
    output = {}

    # wrapper function for HTTP request with circuit breaker
    @task_breaker
    def perform_http_request():

        before_http = time.time()

        with httpx.Client() as client:
            if type == "get":
                response = client.get(
                    TASK_URL, params=query_params, follow_redirects=True
                )
            else:
                response = client.post(
                    TASK_URL,
                    params=query_params,
                    json=body,
                    follow_redirects=True,
                )
            print("task response :" + str(response.text))
            output["url"] = TASK_URL
            output["status_code"] = response.status_code
            try:
                output["response"] = response.json()
            except ValueError:
                output["response"] = response.text

        after_http = time.time()
        request_time = after_http - before_http
        output["time"] = request_time
        return output

    try:
        output = perform_http_request()
        return output
    except pybreaker.CircuitBreakerError as exc:
        return f"Circuitbreaker Error: {exc}, URL: {TASK_URL}"
    except httpx.HTTPError as exc:
        return f"HTTP Error: {exc}"
    
def get_url(url):
    response = requests.get(f"{SR_URL}/api/get", params={"key": url})
    if response.status_code != 200:
        raise Exception(f"Error getting URL from SR: {response.text}")
    key = response.json()[url]
    return key