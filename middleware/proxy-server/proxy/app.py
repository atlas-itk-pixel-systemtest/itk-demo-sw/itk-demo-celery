from fastapi import FastAPI
from fastapi.responses import FileResponse, JSONResponse, HTMLResponse, Response
import sys
import uvicorn
import time
import requests
from proxy.tasks import unique_task
from proxy.model import Tasks, Task
from proxy.tools import create_tasks

app = FastAPI(docs_url="/api/docs")


@app.get("/health")
async def health():
    return Response(status_code=200)


@app.post("/tasks")
async def execute_tasks(tasks: Tasks):
    if tasks.system:
        all_tasks = create_tasks(tasks.tasks)
        return process_tasks(all_tasks, tasks.html)
    else:
        return process_tasks(tasks.tasks, tasks.html)


def process_tasks(tasks: list[Task], html: bool = False):
    if len(tasks) == 0:
        return FileResponse("client/templates/index.html")
    else:
        before = time.time()
        output = routing(tasks)
        after = time.time()
        response = {}
        response["total_time"] = after - before
        response["tasks"] = output
        # print(f"Data: {output}")
        if html:
            # Convert the output dictionary to an HTML list
            html_content = "<ul>"
            for key, value in output.items():
                html_content += f"<li>{key}: {value}</li>"
            html_content += "</ul>"
            return HTMLResponse(content=html_content)
        else:
            return JSONResponse(content=response)


def routing(tasks: list[Task]):
    results = []
    for task in tasks:
        results.append(
            unique_task(
                task.url, task.key, task.path, task.type, task.query_params, task.body
            )
        )
    return results


def main():
    uvicorn.run(app, host="0.0.0.0", port=8211)


if __name__ == "__main__":
    sys.exit(main())
