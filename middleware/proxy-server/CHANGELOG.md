# Changelog

All notable changes[^1] to the `configdb-server`

This python package is used to access the configDB staging area and storage database.
The itk-demo-configdb microservices provide a REST API to this package.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
(always keep this section to take note of untagged changes at HEAD)

## [0.2.0] - 2024-07-16
- Added support for POST-requests
- Added support for passing through parameters and bodys

## [0.1.1] - 2024-07-16
- Started changelog