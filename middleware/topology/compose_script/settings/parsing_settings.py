import yaml
from pydantic import BaseModel
from typing import List
import os 

ORIGIN_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

class Settings(BaseModel):
    workers: dict
    tasks: dict
    system: BaseModel

def generate_datas(file) -> Settings:
    with open(file, 'r') as file:
        donnees_yaml = yaml.safe_load(file)
        system_name = donnees_yaml["system"]["name"].capitalize()
    print(system_name)

    parametres = Settings(**donnees_yaml)
    parametres.system = donnees_yaml["system"]
    print(parametres)
    return parametres

def main(file):
    return generate_datas(file)

if __name__ == "__main__" :
    main()
