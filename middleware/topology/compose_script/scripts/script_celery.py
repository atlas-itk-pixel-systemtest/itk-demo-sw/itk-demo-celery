import os 

#ORIGIN_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
#ORIGIN_PATH = os.environ.get("ORIGIN_PATH",".")
ORIGIN_PATH = os.getcwd()
#ORIGIN_PATH = os.path.dirname(os.path.abspath(__file__))

TARGET_PATH = os.path.join(ORIGIN_PATH, "celery")

def main(queues):
    celery_content = """import os


def is_docker():
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )


if is_docker():
    # print ("Running in Docker")

    broker = "pyamqp://guest:guest@rabbitmq"
    backend = "redis://redis:6379/0"
    # backend="rpc://"

else:
    broker = "pyamqp://guest:guest@localhost"
    backend = "redis://localhost:6379/0"

class CeleryConfig:\n        
    enable_utc = True
    # timezone = 'UTC' # "Europe/Paris"
    result_backend = backend
    broker_url = broker
    broker_connection_retry_on_startup = True
    include = ("topology.server.tasks",)
    worker_send_task_events = True
    task_send_sent_event = True
    task_queues = {\n    """
    #for i in queues:
    for i in queues:
        celery_content +=         f""" "{str(i)}" """+""": {"routing_key": " """+ f"{str(i)}"+""" "},\n    """
    celery_content += "}"
    with open(f"{TARGET_PATH}/celeryconfig.py", "w+") as file:
        file.write(celery_content)
    
    compose_content = ""
    compose_content += f"""
    worker:
        container_name: worker
        image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-celery/topology-server:1
        working_dir: /config
        volumes:
            - {TARGET_PATH}/celeryconfig.py:/topology/server/celeryconfig.py
        entrypoint: ["celery", "-A", "topology.server.app_cel", "worker", "--loglevel=info","-Q","{str(queues)[1:-1].replace("'","").replace(" ","")}", "--pool=prefork", "--concurrency=1" ]
"""
    with open(f"{TARGET_PATH}/compose.yaml", "a") as file:
        file.write(compose_content)


if __name__ == "__main__.py":
    main()