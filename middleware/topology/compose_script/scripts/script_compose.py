import os

#ORIGIN_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
#ORIGIN_PATH = os.environ.get("ORIGIN_PATH",".")
ORIGIN_PATH = os.getcwd()
TARGET_PATH_TOPO = os.path.join(ORIGIN_PATH, "celery")
os.makedirs(TARGET_PATH_TOPO, exist_ok=True)
TARGET_PATH_PROXY = os.path.join(ORIGIN_PATH, "proxy")
os.makedirs(TARGET_PATH_PROXY, exist_ok=True)

def main(settings, SYSTEM, backend):
    compose_content_celery = f"""
networks:
    default:
        name: deminet
        external: true
services:
    topology-server:
        container_name: topology-server
        hostname: topology-server
        image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-celery/topology-server:1
        ports: 
            - 8210:8210
        volumes:
            - {TARGET_PATH_TOPO}/celeryconfig.py:/topology/server/celeryconfig.py
            - {TARGET_PATH_TOPO}/index.html:/client/templates/index.html
        environment:
            - SYSTEM={SYSTEM}
 #           - systems=
 #           - tasks=tasks_list
        entrypoint: ["topology-server" ]
"""
    services_content = ""

    for i in settings.workers:
        queues = f""",  "-Q", {str(settings.workers[i]["queues"])[1:-1].replace("', '", ",")} """ if settings.workers["worker1"]["queues"] else ""

        services_content += f"""
    {i}:
        container_name: {i}
        image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-celery/topology-server:1
        working_dir: /config
        volumes:
            - {TARGET_PATH_TOPO}/celeryconfig.py:/topology/server/celeryconfig.py
        entrypoint: ["celery", "-A", "topology.server.app_cel", "worker", "--loglevel=info" {queues} , "--pool=prefork", "--concurrency=1" ]
"""
    compose_content_celery += services_content
    compose_content_proxy = f"""
networks:
    default:
        name: deminet
        external: true
services:
    proxy-server:
        build: .
        container_name: proxy-server
        hostname: proxy-server
        image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-celery/proxy-server:1
        ports: 
            - 8211:8211
        volumes:
            - {ORIGIN_PATH}/proxy/index.html:/proxy/index.html
        entrypoint: ["proxy"]
"""
    curl_script_content = "#!/bin/bash\n\n"
    backend_content = ""
    PORT = (7200)
    for i in backend:
        PORT += 1
        backend_content += f"""\n    {i}:
        container_name: {i}
        image: {settings.system["be_image"]}
        ports: 
            - {PORT}:8013
#         working_dir: /config
        command: ["topology-backend" ]
"""
        curl_script_content += """response=$(curl -s -o /dev/null -w "%{http_code}" """+f""" "http://localhost:{PORT}")

if [ "$response" != "200" ]; then
    echo "Request on http://localhost:{PORT} failed\n"
else
    echo "Request on http://localhost:{PORT} succeeded\n"
fi\n"""
    compose_content_celery += backend_content

    with open(f"{TARGET_PATH_TOPO}/compose.yaml", "w+") as file:
        file.write(compose_content_celery)
    
    with open(f"{TARGET_PATH_PROXY}/compose.yaml", "w+") as file:
        file.write(compose_content_proxy)

    with open(f"{TARGET_PATH_TOPO}/check_queues.sh", "w+") as file:
        file.write(curl_script_content)

    top_level = """include:
  - celery/compose.yaml
  - proxy/compose.yaml
"""

    with open(f"{ORIGIN_PATH}/compose.yaml", "w+") as file:
        file.write(top_level)
    print("Script 'check_queues.sh' successfully generated.")