import copy
import os
import logging
import typer
import yaml
from rich import print

import compose_script.settings.parsing_settings as parsing_datas
from compose_script.scripts import script_celery, script_compose, script_html
from typing_extensions import Annotated

log = logging.getLogger(__name__)

# ORIGIN_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
# ORIGIN_PATH = os.path.dirname(os.path.abspath(__file__))
# ORIGIN_PATH = os.environ.get("ORIGIN_PATH",".")
ORIGIN_PATH = os.getcwd()

TARGET_PATH = ORIGIN_PATH  # os.path.join(ORIGIN_PATH, "example")


##################################################################################


def generate_compose_file(settings):
    SYSTEM = config_file(settings)
    backend = backend_building(settings)
    script_compose.main(settings, SYSTEM, backend)


##################################################################################


def generate_celery_file(settings):
    queues = backend_building(settings)
    script_celery.main(queues)
    print("Backend and Queues")
    for q in queues:
        print(q)


##################################################################################


def generate_html_file(settings):
    celery = True
    script_html.main(celery)
    celery = False
    script_html.main(celery)


##################################################################################


def backend_building(settings):
    model = settings.system
    system_list = list(model.items())
    queues = recursive(system_list[2:], "")
    return queues


def recursive(system_list, to_return):
    result_list = []
    system_size = len(system_list)
    if system_size == 0:
        result_list.append(to_return)
    else:
        for j in range(system_list[0][1][0], system_list[0][1][1] + 1):
            temp = copy.deepcopy(to_return)
            temp += str(system_list[0][0]) + str(j)
            result_list.extend(recursive(system_list[1:], temp))

    return result_list


def config_file(settings):
    SYSTEM = []
    for key, value in settings.system.items():
        if key not in ['name','be_image']:  # Ignore key name and be_image
            SYSTEM.append((key, tuple(value)))
    os.environ["sys_names"] = str([key for key in settings.system.keys() if key not in ['name','be_image'] ])
    print(SYSTEM)
    return SYSTEM


def tasks(settings):
    tasks_list = []
    print("Tasks :")
    for task in settings.tasks["names"]:
        print(task)
        tasks_list.append(task)
    return tasks_list


def datas(settings):
    keys_ranges = config_file(settings)
    max_key = len(keys_ranges)

    def generate_children(key_index, ranges):
        if max_key == key_index:
            return
        print(ranges[key_index])
        key, (start, end) = ranges[key_index]
        return (
            [
                {key: str(i), "children": generate_children(key_index + 1, ranges)}
                for i in range(start, end + 1)
            ]
            if key_index < len(ranges)
            else []
        )

    return generate_children(0, keys_ranges)

def main(settings_yaml: Annotated[str, typer.Option("--file", "-f", help="Path of yaml file")]):
    logging.basicConfig(level=logging.INFO)
    log.info(f"Loading {settings_yaml}")

    with open(settings_yaml, "r") as file:
        data = yaml.safe_load(file)

    print("YAML datas downloaded successfully : ", data)
    print(TARGET_PATH)
    settings = parsing_datas.main(settings_yaml)
    print(settings)
    os.environ["data"] = str(datas(settings))
    os.environ["tasks"] = str(tasks(settings))

    generate_compose_file(settings)
    generate_celery_file(settings)
    generate_html_file(settings)

def entry_point() -> None:
    typer.run(main)

if __name__ == "__main__":
    typer.run(main)
