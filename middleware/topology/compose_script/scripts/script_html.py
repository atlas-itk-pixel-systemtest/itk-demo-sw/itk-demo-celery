from jinja2 import Environment, FileSystemLoader
import os

#ORIGIN_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
#ORIGIN_PATH = os.environ.get("ORIGIN_PATH",".")
ORIGIN_PATH = os.getcwd()
#ORIGIN_PATH = os.path.dirname(os.path.abspath(__file__))

TARGET_PATH = os.path.join(ORIGIN_PATH, "celery")

def generate_checkboxes(celery, data, systems, parent_id='', parent_name=''):
    checkboxes_html = ''
    for system in data:
        system_id = f"{system[systems[0]]}" if parent_id else system[systems[0]]
        system_name = f"{parent_name}" if parent_name else systems[0]
        all_system = f"{system_name}_{system_id}"
        current_system = f"{systems[0]} {system[systems[0]]}"
        if len(systems) > 0:
            child_data = system['children']
            if child_data:
                checkboxes_html += '<div class="checkbox-column">\n    '
                checkboxes_html += f'<label class="optobox-title" for="{all_system}">{current_system}</label><br>\n   '
                if celery :
                    checkboxes_html += f' <input type="checkbox" id="{all_system}" name="queue" value="{all_system}">\n  '
                    checkboxes_html += f'  <label for="{all_system}">All {current_system}</label><br><hr>\n  '
                
                child_system_name = f"{all_system}_{systems[1]}"
                checkboxes_html += generate_checkboxes(celery,child_data, systems[1:], f"{system_id}_", child_system_name)
                checkboxes_html += '</div>\n'
            else :
                checkboxes_html += f' <input type="checkbox" id="{all_system}" name="queue" value="{all_system}">\n  '
                checkboxes_html += f'  <label for="{all_system}">{current_system}<span class="task-status"></span></label><br>\n  '
        

    return checkboxes_html

def main(celery):
    # Creating Jinja2 environment
    env = Environment(loader=FileSystemLoader('.'))

    env.globals['generate_checkboxes'] = generate_checkboxes
    template = env.from_string('''
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Click on the task to execute</title>
        <script src="script.js"></script>
        <style>
            .checkbox-column {
                float: left;
                width: 49%;
                border: 1px solid #ccc; /* Bordure de la colonne */
                box-sizing: border-box; /* Pour inclure la bordure dans la largeur de la colonne */
                padding: 10px; /* Espacement autour des cases à cocher */
            }
            .task-status {
                display: inline-block;
                width: 10px; /* Largeur du point */
                height: 10px; /* Hauteur du point */
                background-color: red; /* Couleur initiale */
                border-radius: 50%; /* Pour obtenir une forme circulaire */
                margin-left: 5px; /* Espacement entre le point et le texte */
            }
            .task-status.success {
                background-color: green; /* Couleur verte pour indiquer le succès */
            }


        </style>
    </head>
    <body>
        <h1 class="title">{{systems[0]|capitalize}}</h1>
        <h2>Click on the task to execute :</h2>
        <ul>
        {% for task in tasks %}
            <li><a href="/{{ task }}">{{ task }}</a></li>
        {% endfor %}
        </ul>
        {% if celery %}
            <h2>Queues:</h2>
            <input type="checkbox" id="all" name="queue" value="all">
            <label for="all">All</label><br>
        {% endif %}

        <form id="queueForm">
            {% set checkboxes = generate_checkboxes(celery,data, systems) %}
            {{ checkboxes | safe }}
        </form>
        <h3>Progress Bar On Going</h3>
        <div id="progress"></div>
        <script>
          function executeTask(taskLink) {
              const checkedBoxes = document.querySelectorAll('input[name="queue"]:checked');
              if (checkedBoxes.length === 0) {
                  alert("Please select a queue before executing the task.");
                  return;
              }

              let taskURL = taskLink.getAttribute('href');
              let taskName = taskURL.substring(taskURL.lastIndexOf('/') + 1); // Extract task name from URL

              var checkboxValues = Array.from(checkedBoxes).map(box => box.value.replace(/_/g, '/'));

              // Sort checkbox values by length
              checkboxValues.sort((a, b) => a.length - b.length);

              current_url = window.location.href

              if (checkboxValues.some(value => value.startsWith('all'))) {
                  newUrl = current_url + taskName;
              } else {
                  newUrl = current_url + checkboxValues[0] + '/' + taskName; // Select the shortest value
              }

              console.log(newUrl);
              window.location.href = newUrl;
          }


    const taskLinks = document.querySelectorAll('a');
    taskLinks.forEach(link => {
        link.addEventListener('click', function(event) {
            event.preventDefault();
            executeTask(this);
        });
    }); 

    document.addEventListener("DOMContentLoaded", function() {
    const checkboxes = document.querySelectorAll('input[name="queue"]');
    checkboxes.forEach(checkbox => {
        checkbox.addEventListener('change', function() {
            const value = this.value;
            if (value === 'all') {
                checkboxes.forEach(box => {
                    if (box !== this) {
                        box.checked = this.checked;
                    }
                });
            } else {
                if (this.checked) {
                    checkboxes.forEach(box => {
                        if (box !== this && box.value.startsWith(value)) {
                            box.checked = true;
                        }
                    });
                } else {
                    checkboxes.forEach(box => {
                        if (box !== this && box.value.startsWith(value)) {
                            box.checked = false;
                        }
                    });
                }
            }
        });
    });
});

      </script>          
    </body>
    </html>
    ''')


    systems = eval(os.environ["sys_names"])

    tasks = eval(os.environ["tasks"])

    #data = [
    #    {'felix': '1', 'children': [{'card': '1', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}, {'card': '2', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}, {'card': '3', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}]},
    #    {'felix': '2', 'children': [{'card': '1', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}, {'card': '2', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}, {'card': '3', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}]},
    #    {'felix': '3', 'children': [{'card': '1', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}, {'card': '2', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}, {'card': '3', 'children': [{'dev': '1', 'children': []},{'dev': '2', 'children': []}]}]},
    #]
    data = eval(os.environ["data"])
    output = template.render(tasks=tasks,data=data,systems=systems,celery=celery)

    if celery:
        with open(f'{TARGET_PATH}/index.html', 'w+') as f:
            f.write(output)
    else:
        with open(f'{ORIGIN_PATH}/proxy/index.html', 'w+') as f:
            f.write(output)
if __name__ == "__main__.py":
    main(celery=True)