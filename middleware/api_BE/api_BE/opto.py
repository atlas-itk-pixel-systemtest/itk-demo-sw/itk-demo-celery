from cpu_load_generator import load_single_core, load_all_cores, from_profile
import time

def health():
    return ("Celery is running! The connection to the microservice backend works!")

def add():
    a = 3
    b = 5
    return (f"Result of {a}+{b} is "+str(a+b))

def minus():
    a = 3
    b = 5
    return (f"Result of {a}-{b} is "+str(a-b))

def write():
    return ("Writing function, to be written")

def state():
    return ("State function !!!")

def shutdown():
    return ("Should shutting down !")

def load_cpu_100(no,cpu,seconds):
    load_all_cores(duration_s=seconds, target_load=cpu)
    return f"CPU {cpu*100} during "+str(seconds)

def load_cpu_50(no,cpu,seconds):
    load_all_cores(duration_s=seconds, target_load=cpu)
    time.sleep(seconds)
    return f"CPU {cpu*50} during "+str(seconds)
