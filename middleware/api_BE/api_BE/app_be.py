from microdot import Microdot
import logging
from rich.logging import RichHandler
from microdot.cors import CORS
import logging_tree
from . import opto

app = Microdot()

cors = CORS(
    app,
    allowed_origins=["*"],
    allow_credentials=True,
    allowed_methods=["GET", "POST"]  
)

log = logging.getLogger("rich")
log.info("Booting Topology server ...")

if log.isEnabledFor(logging.DEBUG):
    logging_tree.printout()

@app.route('/')
async def index(request):
    return "on BE server\n"


@app.get('/health')
def health(request):
    arg = request.args.get('arg', '') 

    result = opto.health()

    print("microdot "+result)
    return (result+"\n")


@app.get('/add')
def add(request):
    arg = request.args.get('arg', '') 

    result = opto.add()

    print("microdot "+result)
    return (result+"\n")

@app.get('/minus')
def minus(request):
    arg = request.args.get('arg', '') 

    result = opto.minus()

    print("microdot "+result)
    return (result+"\n")

@app.get('/write')
def write(request):
    arg = request.args.get('arg', '') 

    result = opto.write()

    print("microdot "+result)
    return (result+"\n")


@app.get('/state')
def state(request):
    arg = request.args.get('arg', '') 

    result = opto.state()

    print("microdot "+result)
    return (result)

@app.get('/shutdown')
def shutdown(request):
    arg = request.args.get('arg', '') 

    result = opto.shutdown()

    print("microdot "+result)
    return (result)

@app.get('/load_cpu_100')
def load_cpu_100(request):
    arg = request.args.get('arg', '') 

    result = opto.load_cpu_100(None,1, 3)

    print("microdot "+result)
    return (result)


@app.get('/load_cpu_50')
def load_cpu_50(request):
    arg = request.args.get('arg', '') 

    result = opto.load_cpu_50(None,1, 3)

    print("microdot "+result)
    return (result)

def main(port=8013):    
    logging.basicConfig(
        level=logging.INFO,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler()],
    )
    for method, pattern, function in app.url_map:
        print(method, pattern.url_pattern, function.__name__)

    app.run(port=int(port), debug=True)    

if __name__ == "__main__":
    main()