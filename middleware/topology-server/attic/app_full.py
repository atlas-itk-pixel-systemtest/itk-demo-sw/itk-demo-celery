from fastapi import FastAPI, Response
from fastapi.responses import FileResponse
import sys
import uvicorn
import copy
import os
import httpx
from topology.server.tasks import unique_task
import click

app = FastAPI()

SYSTEM = os.environ.get("SYSTEM", "[]")
nb = len(SYSTEM.split("),")) // 2
celery_enabled = False
proxy_enabled = False


def proxy_routing(backend, task_name, outputs):
    BASE_URL = f"http://{backend}:8013"
    TASK_URL = BASE_URL + f"/{task_name}"
    print(TASK_URL)
    try:
        with httpx.Client() as client:
            response = client.get(TASK_URL)
            outputs += (
                "Backend: " + str(TASK_URL) + "\nResponse: " + response.text + "\n"
            )
    except httpx.HTTPError as exc:
        outputs += f"HTTP Error: {exc}"
    return outputs


def celery_routing(inputs, task_name, outputs):
    list_system = eval(SYSTEM)
    nb_pairs = len(inputs) / 2
    dif = int(nb - nb_pairs)
    worksys = ""
    i = 0
    while i < (len(inputs)):
        worksys += inputs[i] + inputs[i + 1]
        i += 2

    if dif == 0:
        return unique_task.apply_async(
            args=(str(task_name), worksys, None), queue=worksys
        ).get()

    else:
        for i in range(
            list_system[int(nb) - int(dif)][1][0],
            list_system[int(nb) - int(dif)][1][1] + 1,
        ):
            temp = copy.deepcopy(inputs)
            temp.append(list_system[int(nb) - int(dif)][0])
            temp.append(str(i))
            outputs += celery_routing(temp, task_name, outputs)
    outputs += "\n"
    return outputs


def route_task(segments):
    print("THERE  " + str(segments))
    global celery_enabled
    global proxy_enabled
    if len(segments) == 0:
        print("Celery :" + str(celery_enabled))
        print("proxy :" + str(proxy_enabled))
        return FileResponse("/client/templates/index.html")
    else:
        segments_list = segments.split("/")
        task_name = segments_list[-1]
        outputs = ""
        if celery_enabled:
            inputs = segments_list[:-1]  # Others are the path
            return Response(content=celery_routing(inputs, task_name, outputs))
        elif proxy_enabled:
            backend = "".join(segments_list[:-1])
            print(backend)
            return Response(content=proxy_routing(backend, task_name, outputs))
        else:
            return "Tag not known"


@app.get("/{segments:path}")
async def process_task(segments: str):
    return route_task(segments)


@click.command()
@click.option("--celery", is_flag=True, help="with Celery")
@click.option("--proxy", is_flag=True, help="without Celery")
def main(celery, proxy):
    global celery_enabled
    global proxy_enabled

    if celery:
        celery_enabled = True
        proxy_enabled = False
    elif proxy:
        proxy_enabled = True
        celery_enabled = False
    else:
        print("Specify either --celery or --proxy flag.")
        return
    uvicorn.run(app, host="0.0.0.0", port=8310)


if __name__ == "__main__":
    sys.exit(main())
