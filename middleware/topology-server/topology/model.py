from pydantic import BaseModel, model_validator, ValidationError, Field
from typing import Literal, Optional, Annotated
from fastapi import HTTPException


class Task(BaseModel):
    type: Annotated[Literal["get", "post"], Field(description="type of the request (currently support get and post)")]
    path: Annotated[str, Field(description="endpoint path")]
    key: Annotated[Optional[str], Field(description="sr key to service url")] = None
    url: Annotated[Optional[str], Field(description="service url")] = None
    query_params: Annotated[dict[str, str], Field(description="query param dict")] = {}
    body: Annotated[dict[str, str], Field(description="body dict")] = {}
    queue: Annotated[str, Field(description="queue to use")] = "default"

    @model_validator(mode="before")
    def check_key_or_url(cls, values):
        key = values.get("key")
        url = values.get("url")
        if not key and not url:
            raise HTTPException(status_code=422, detail='Either "key" or "url" must be provided.')
        return values


class Tasks(BaseModel):
    tasks: list[Task]
    html: Annotated[bool, Field(description="give back response as html")] = False
    system: Annotated[bool, Field(description="use Jordans system logic to route the requests")] = False
