from topology.model import Task
import os

SYSTEM = os.environ.get(
    "SYSTEM", "[('felix', (1, 1)), ('card', (0, 1)), ('dev', (0, 1)), ('dma', (0, 4))]"
)


def create_tasks(tasks: list[Task]):
    for task in tasks:
        backend = task.key.strip("/")
        elements = backend.split("/")

        result = []
        for i in range(0, len(elements) - 1, 2):
            result.append((elements[i], elements[i + 1]))

        task.url = [result]
    new_tasks = []
    system_list = eval(SYSTEM)
    for task in tasks:
        backend = task.url
        dif = len(system_list) - len(backend[0])
        if dif == 0:
            url = "".join("".join(pair) for pair in backend)
            url = f"http://{url}:8013"
            new_task = task.model_copy()
            new_task.url = url
            new_tasks.append(new_task)
        else:
            while dif > 0:
                new_backends = []
                layer = system_list[-dif]
                for i in range(layer[1][0], layer[1][1] + 1):
                    for b in backend:
                        new_b = b.copy()
                        new_b.append((layer[0], str(i)))
                        new_backends.append(new_b)
                backend = new_backends
                dif = len(system_list) - len(backend[0])
            for b in backend:
                url = "".join("".join(pair) for pair in b)
                url = f"http://{url}:8013"
                new_task = task.model_copy()
                new_task.url = url
                new_tasks.append(new_task)
    return new_tasks
