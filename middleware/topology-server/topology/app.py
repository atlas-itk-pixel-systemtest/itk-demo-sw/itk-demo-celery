from fastapi import FastAPI
from fastapi.responses import FileResponse, JSONResponse, HTMLResponse, Response
from celery import group
from celery.result import GroupResult
import sys
import uvicorn
import time
from topology.tools import create_tasks
from topology.model import Tasks, Task
from topology.server.tasks import unique_task

app = FastAPI(docs_url="/api/docs")  # custom location for swagger UI)

import logging
from rich.logging import RichHandler

logging.basicConfig(
    level=logging.INFO, format="%(message)s", datefmt=" ", handlers=[RichHandler()]
)
log = logging.getLogger("rich")

@app.get("/health")
async def health():
    return Response(status_code=200)


@app.post("/tasks")
async def execute_tasks(tasks: Tasks):
    if tasks.system:
        all_tasks = create_tasks(tasks.tasks)
    else:
        all_tasks = tasks.tasks
    before = time.time()
    output = process_tasks(all_tasks)
    after = time.time()
    response = {}
    response["total_time"] = after - before
    response["tasks"] = output
    return JSONResponse(content=response)

@app.post("/task")
async def execute_task(task: Task):
    before = time.time()
    output =  process_task(task) 
    after = time.time()
    response = {}
    response["total_time"] = after - before
    response["tasks"] = output
    return JSONResponse(content=response)

def process_task(task: Task):
    result = unique_task.s(task.url, task.key, task.path, task.type, task.query_params, task.body).set(queue=task.queue).apply_async() 
    return result.id

def process_tasks(tasks: list[Task]):
    task_signatures = []

    for task in tasks:

        task_signatures.append(unique_task.s(task.url, task.key, task.path, task.type, task.query_params, task.body).set(queue=task.queue))

    group_result: GroupResult = group(task_signatures)()
    group_result.save()
    # log.info(f"group_result.id: {group_result.id}")
    return group_result.id

# HTML stuff
# def process_tasks(tasks: list[Task], html: bool = False):
#     if len(tasks) == 0:
#         return FileResponse("client/templates/index.html")
#     else:
#         # print(f"Data: {output}")
#         if html:
#             # Convert the output dictionary to an HTML list
#             html_content = "<ul>"
#             for key, value in output.items():
#                 html_content += f"<li>{key}: {value}</li>"
#             html_content += "</ul>"
#             return HTMLResponse(content=html_content)
#         else:
#             return JSONResponse(content=response)

def main():
    uvicorn.run(app, host="0.0.0.0", port=8210)


if __name__ == "__main__":
    sys.exit(main())
