import pybreaker
import redis

backend_redis = redis.StrictRedis().from_url("redis://redis:6379/0")


class BreakerManager:
    breakers: dict

    def __init__(self):
        self.breakers = {}

    def get_breaker(
        self,
        task_target: str,
        fail_max: int = 5,
        reset_timeout: int = 5,
        state_storage=pybreaker.CircuitRedisStorage(
            pybreaker.STATE_CLOSED, backend_redis
        ),
        throw_new_error_on_trip: bool = False,
        **kwargs,
    ):
        """
        Get a circuit breaker for a given task target.

        Optional configuration parameters for the pybreaker.CircuitBreaker can be profided.
        They will be used to create a new circuit breaker if one does not already exist for the given task target.
        """
        if task_target not in self.breakers:
            task_breaker = pybreaker.CircuitBreaker(
                fail_max=fail_max,
                reset_timeout=reset_timeout,
                state_storage=state_storage,
                throw_new_error_on_trip=throw_new_error_on_trip,
                **kwargs,
            )
            self.breakers[task_target] = task_breaker
        else:
            task_breaker = self.breakers[task_target]
        return task_breaker


manager = BreakerManager()
