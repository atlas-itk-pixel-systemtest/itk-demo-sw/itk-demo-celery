import os


def is_docker():
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )


if is_docker():
    # print ("Running in Docker")

    broker = "pyamqp://guest:guest@rabbitmq"
    backend = "redis://redis:6379/0"
    # backend="rpc://"

else:
    broker = "pyamqp://guest:guest@localhost"
    backend = "redis://localhost:6379/0"


class CeleryConfig:
    enable_utc = True
    # timezone = 'UTC' # "Europe/Paris"
    result_backend = backend
    broker_url = broker
    broker_connection_retry_on_startup = True
    include = ("topology.server.tasks",)
    worker_send_task_events = True
    task_send_sent_event = True
    worker_concurrency = 1
    worker_pool = "solo"  # [prefork|eventlet|gevent|solo|processes|threads|custom]
    task_queues = {
        "default": {"routing_key": "default_queue"},
        # "obox1oboard1": {"routing_key": "obox1oboard1"},
        # "obox1oboard2": {"routing_key": "obox1oboard2"},
        # "obox1oboard3": {"routing_key": "obox1oboard3"},
        # "obox1oboard4": {"routing_key": "obox1oboard4"},
        # "obox1oboard5": {"routing_key": "obox1oboard5"},
        # "obox1oboard6": {"routing_key": "obox1oboard6"},
        # "obox2oboard1": {"routing_key": "obox2oboard1"},
        # "obox2oboard2": {"routing_key": "obox2oboard2"},
        # "obox2oboard3": {"routing_key": "obox2oboard3"},
        # "obox2oboard4": {"routing_key": "obox2oboard4"},
        # "obox2oboard5": {"routing_key": "obox2oboard5"},
        # "obox2oboard6": {"routing_key": "obox2oboard6"},
        # "obox3oboard1": {"routing_key": "obox3oboard1"},
        # "obox3oboard2": {"routing_key": "obox3oboard2"},
        # "obox3oboard3": {"routing_key": "obox3oboard3"},
        # "obox3oboard4": {"routing_key": "obox3oboard4"},
        # "obox3oboard5": {"routing_key": "obox3oboard5"},
        # "obox3oboard6": {"routing_key": "obox3oboard6"}
    }
    # task_create_missing_queues = False
