from celery import Celery
from topology.server.celeryconfig import CeleryConfig

app = Celery('server')
app.config_from_object(CeleryConfig)

if __name__ == "__main__":
    app.start()
