from topology.server.app_cel import app
import httpx
import time
import os
import requests
import pybreaker
from celery_progress.backend import ProgressRecorder
from topology.server.circuit_breaker import manager as breaker_manager

SR_URL = os.environ.get("SR_URL", "http://localhost:5111")

# Defining circuit breaker
task_breakers = {}


@app.task(bind=True)
def unique_task(self, url, key, path, type, query_params, body):

    progress_recorder = ProgressRecorder(self)
    
    if not url:
        url = get_url(key)
    
    task_breaker = breaker_manager.get_breaker(url)

    task_name = "/" + path.strip("/")
    TASK_URL = url + f"{task_name}"
    output = {}

    # wrapper function for HTTP request with circuit breaker
    @task_breaker
    def perform_http_request():

        before_http = time.time()

        with httpx.Client() as client:
            if type == "get":
                response = client.get(
                    TASK_URL, params=query_params, follow_redirects=True
                )
            else:
                response = client.post(
                    TASK_URL,
                    params=query_params,
                    json=body,
                    follow_redirects=True,
                )
            print("task response :" + str(response.text))
            output["url"] = TASK_URL
            output["status_code"] = response.status_code
            try:
                output["response"] = response.json()
            except ValueError:
                output["response"] = response.text

        after_http = time.time()
        request_time = after_http - before_http
        output["time"] = request_time
        return output

    try:
        output = perform_http_request()
        progress_recorder.set_progress(100, 100)  # set 100%
        return output
    except pybreaker.CircuitBreakerError as exc:
        return f"Circuitbreaker Error: {exc}, URL: {TASK_URL}"
    except httpx.HTTPError as exc:
        return f"HTTP Error: {exc}"


def get_url(url):
    response = requests.get(f"{SR_URL}/api/get", params={"key": url})
    if response.status_code != 200:
        raise Exception(f"Error getting URL from SR: {response.text}")
    key = response.json()[url]
    return key
