from tasks import state

import time

start_time = time.time()
result = state.delay().get()
end_time = time.time()
total = end_time - start_time
print("state execution_time = " + str(total))
