from celery import shared_task, current_app
import requests
import time
import logging
import collections

logger = logging.getLogger(__name__)

# FSM

PORT = 8000
fsm_url = f"http://localhost:{PORT}"


@shared_task()
async def state(request):
    result = 2 + 2
    time.sleep(3)
    return result


@shared_task
def state_cel(request):
    r = requests.get(f"{fsm_url}/markup")
    return r


@shared_task
def trigger(state_event="probe"):
    r = requests.post(f"{fsm_url}/trigger/{state_event}")
    rj = r.json()
    return rj


@shared_task
def markup():
    r = requests.get(f"{fsm_url}/markup")
    rj = r.json()
    return rj


@shared_task
def health(request):
    r = requests.get(f"{fsm_url}/health")


@shared_task
def gather(request):
    r = requests.get(f"{fsm_url}/markup")


@shared_task
def states(request, machinename):
    pass


@shared_task
def graph(request):
    r = requests.get(f"{fsm_url}/markup/graph")


@shared_task
def markup_json(request):
    r = requests.get(f"{fsm_url}/markup/json")


@shared_task
def markup_yaml(request):
    r = requests.get(f"{fsm_url}/markup/yaml")


@shared_task
def trigger(request, triggername):
    r = requests.post(f"{fsm_url}/trigger/<triggername>")


@shared_task
def shutdown(request):
    r = requests.get(f"{fsm_url}/shutdown")

    msg = "The server is shutting down..."
    print(msg)
    request.app.shutdown()
    return msg


# MONITORING


@shared_task()
def celery_inspect_stats():
    print("Inside get_celery_stats")
    inspect_output = current_app.control.inspect()
    print("Stats ", inspect_output.stats(), flush=True)
    return inspect_output.stats()


@shared_task()
def celery_inspect_worker(workername=None):
    methods = (
        "stats",
        "active_queues",
        "registered",
        "scheduled",
        "active",
        "reserved",
        "revoked",
        "conf",
    )

    workers = collections.defaultdict(dict)

    for method in methods:
        inspect = current_app.control.inspect(timeout=5, destination=workername)

        # print('%s inspect %s', workername, method)
        print(f"{workername} {method}")
        start = time.time()
        result = (
            getattr(inspect, method)()
            if method != "active"
            else getattr(inspect, method)(safe=True)
        )
        logger.debug(
            "Inspect command %s took %.2fs to complete", method, time.time() - start
        )

        if result is None or "error" in result:
            logger.warning("Inspect method %s failed", method)
            # return

        print(result)

        for worker, response in result.items():
            if response is not None:
                info = workers[worker]
                info[method] = response
                info["timestamp"] = time.time()

    return workers


"""

from director import task

# /
@task(name="INDEX",queue='index_queue')
def index_d(request):

    return "Index page"

# /public/<path:path>
@task(name="STATIC",queue='static_queue')
def static_d(path):

    return f"Static file {path}"

# /state/<machinename>
@task(name="STATE",queue='state_queue')
def states_d(request, machinename):

    return f"State of machine {machinename}"

# /trigger/<triggername>
@task(name="TRIGGER",queue='trigger_queue')
def trigger_d(request, triggername):

    return f"Trigger {triggername} executed"

# /gather
@task(name="GATHER",queue='gather_queue')
def gather_d(request):

    return "Gather data"

# /markup/json
@task(name="MARKUP_JSON",queue='markup_json_queue')
def markup_json_d(request):

    return "Markup JSON"

# /markup/yaml
@task(name="MARKUP_YAML",queue='markup_yaml_queue')
def markup_yaml_d(request):

    return "Markup YAML"

# /markup/graph
@task(name="MARKUP_GRAPH",queue='markup_graph_queue')
def markup_graph_d(request):

    return "Markup Graph"

# /health
@task(name="HEALTH",queue='health_queue')
def health_d(request):

    return "Health check"

# /shutdown
@task(name="SHUTDOWN",queue='shutdown_queue')
def shutdown_d(request):

    return "Server shutdown"



"""
