from celery import Celery
# from fsm_celery


class CeleryConfig:
    enable_utc = True
    # timezone = 'UTC' # "Europe/Paris"
    result_backend = "redis://localhost:6379/0"
    broker_url = "amqp://guest:guest@localhost:5672//"
    broker_connection_retry_on_startup = True
    include = ("fsm_celery.tasks",)
    worker_send_task_events = True
    task_send_sent_event = True
    worker_concurrency = 1
    worker_pool = "solo"  # [prefork|eventlet|gevent|solo|processes|threads|custom]


app = Celery(
    "fsm_celery",
    # backend = "redis://localhost:6379/0",
)

app.config_from_object(CeleryConfig())
# app.autodiscover_tasks()

# print(app.conf.humanize())
