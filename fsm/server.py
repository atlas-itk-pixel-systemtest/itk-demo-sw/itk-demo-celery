#!/usr/bin/env python
# import subprocess

import json
import logging

# import logging_tree
import yaml
from microdot.asgi import Microdot, send_file
from rich import inspect, print
from rich.logging import RichHandler
from transitions import MachineError

# import uuid
from fsm_demi.event_bus import EventBus
from fsm_demi.machine_factory import create_machine, draw_graph

# from microdot.cors import CORS

logging.basicConfig(
    level=logging.DEBUG,
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler()],
)
log = logging.getLogger("rich")
log.info("Booting FSM server ...")


class Model(object):
    def __init__(self, name):
        self.name = name


model = Model("backend-model")

machine = create_machine(model=model)
draw_graph(machine)

bus = EventBus()
bus.add_member(model)

app = Microdot()
# cors = CORS(
#     app,
#     allowed_origins=["*"],
#     allow_credentials=True,
# )

# if log.isEnabledFor(logging.DEBUG):
#     logging_tree.printout()

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/fsm"
# Microdot Routes


@app.get("/state")
async def state_c(request):
    # subprocess.run(["docker", "compose", "up","--build","-d"],cwd=f"{ORIGIN_PATH}/stacks/containers")

    response = await state(request)
    return str(response)


@app.get("/")
async def index(request):
    return send_file("public/index.html", max_age=0)


@app.get("/public/<path:path>")
async def static(request, path):
    if ".." in path:
        # directory traversal is not allowed
        return "Not found", 404
    return send_file("public/" + path, max_age=0)


@app.get("/health")
async def health(request):
    return "Ok", 200


@app.get("/gather")
async def gather(request):
    return bus.gather()


@app.get("/state/<machinename>")
async def states(request, machinename):
    return machine.model[machinename].states


@app.get("/markup")
async def markup(request):
    response = machine.markup
    # print(response)
    return response


@app.get("/markup/graph")
async def graph(request):
    return str(machine.get_graph())


@app.get("/markup/json")
async def markup_json(request):
    return json.dumps(machine.markup, indent=4)


@app.get("/markup/yaml")
async def markup_yaml(request):
    return yaml.dump(machine.markup)


@app.get("/trigger/<triggername>")
async def trigger(request, triggername):
    global machine
    global model
    try:
        res = bus.broadcast(triggername)
        res["reachable"] = machine.get_triggers(model.state)
        res["status"] = machine.get_state(model.state).status

    except MachineError as mexc:
        log.exception(f"trigger failed {mexc}")

    # print(machine.get_state(model.state).status)

    draw_graph(machine)
    # resend index with updated graph
    return res
    # send_file("public/index.html")


@app.post("/shutdown")
async def shutdown(request):
    msg = "The server is shutting down..."
    print(msg)
    request.app.shutdown()
    return msg


# @app.errorhandler(500)
# async def internal_error(request):
#     print(dir(request))
#     return{'error':'internal error'}, 500

if __name__ == "__main__":
    # inspect(m, methods=True)
    for method, pattern, function in app.url_map:
        print(f"{method} {pattern.url_pattern} {function.__name__}")

    app.run(port=7000, debug=True)
