# Generic DeMi Finite State Machine

Implemented with `pytransitions` with some tweaks to simplify the functionality and
keep it programmatically.



Features:

- minimalist [ASGI](https://en.wikipedia.org/wiki/Asynchronous_Server_Gateway_Interface) webserver to control FSM using [Microdot](https://microdot.readthedocs.io/en/latest/index.html)
  - start using Docker

    ```docker compose up --build```

  - start locally with 
  
    ```python serve.py```
  
  - or use `uvicorn`:

    ```uvicorn python serve.py```

  - control website is at `http://localhost:8000/`

Disabled:

- auto_transitions
- convenience methods on Machine

To Do:

- event queueing
- event persistence with history/log
