import os
import socket
import sys
import uuid
import json
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

SOCK_FILE = "/var/run/fsm-ipc.socket"

# Init socket object
if not os.path.exists(SOCK_FILE):
    print(f"File {SOCK_FILE} doesn't exists")
    sys.exit(-1)

s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
s.connect(SOCK_FILE)

# Send request
msg = dict(
    action="initialize",
    uuid=str(uuid.uuid4()),
)
jmsg = json.dumps(msg).encode("utf-8")
print(f"Sending: {jmsg}")
s.sendall(jmsg)

# Wait for response
data = s.recv(1024)
print(data)
print(data.decode("utf-8"))
msg = json.loads(data)

print(msg["action"])
print(uuid.UUID(msg["uuid"]))

# rcv_uuid = uuid.UUID(bytes=data)
# print(f"Received bytes: {repr(rcv_uuid)}")
