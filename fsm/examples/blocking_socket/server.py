import os
import socket
import uuid
import json
import logging

# from transitions import Machine

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

SOCK_FILE = "/var/run/fsm-ipc.socket"

# Setup socket
if os.path.exists(SOCK_FILE):
    os.remove(SOCK_FILE)

s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
s.bind(SOCK_FILE)
os.chmod(SOCK_FILE, 0o666)
s.listen()

log.info("Start listening loop")
while True:
    # Accept 'request'
    conn, addr = s.accept()
    log.info("Connection by client")
    # Process 'request'
    while True:
        data = conn.recv(1024)
        if not data:
            break

        msg = json.loads(data)

        log.info(msg["action"])
        log.info(uuid.UUID(msg["uuid"]))

        # Send 'response'
        conn.sendall(data)

# os.remove(SOCK_FILE)
