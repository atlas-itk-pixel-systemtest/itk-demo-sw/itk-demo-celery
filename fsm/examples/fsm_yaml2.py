from transitions.extensions import MachineFactory
# from transitions.extensions.nesting import HierarchicalMachine

import logging
import yaml

# from model2 import Model
from rich import print, inspect

logging.basicConfig(level=logging.INFO)

config = {}
with open("machine_2.yaml") as f:
    yaml_config = f.read()
    config = yaml.safe_load(yaml_config)

# print(config)

outer_conf = config["outerconf"]
inner_conf = config["innerconf"]

# print(outer_conf)
# print(inner_conf)

# def get_models():
#   nmax=100
#   models = [ Model(f"flx{i}") for i in range(nmax) ]
#   return models

# config['model'] = get_models()
# model = Model("model2")
# outer_conf['model'] = model

# machine = HierarchicalMachine(**outer_conf, auto_transitions=False)
# inner_machine = HierarchicalMachine(**inner_conf, auto_transitions=False)
# machine.add_state(inner_machine)

machine_cls = MachineFactory.get_predefined(nested=True)

outer_machine = machine_cls(**outer_conf)
inner_machine = machine_cls(**inner_conf)


# machine.add_ordered_transitions(
#    ['Unknown', 'Started', 'Initialized', 'Configured', 'Running', 'Stopped'],
#    conditions='a_coin_toss',
#   loop=False
# )

# print('Model')
# print(model.__dict__)

# print('Machine')
# print(machine.__dict__)
# print(machine.states)

for state in outer_machine.states:
    print(state, outer_machine.get_triggers(state))

# model.trigger('start')
