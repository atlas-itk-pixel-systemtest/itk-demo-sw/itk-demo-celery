# from transitions_gui import WebMachine
from transitions import Machine

# from transitions.extensions import MachineFactory
# from transitions.exception import MachineError
import yaml
import pickle

from itk_demo_celery.fsm.model import Model

from rich import print

# # Set up logging; The basic log level will be DEBUG
# import logging
# logging.basicConfig(level=logging.DEBUG)
# # Set transitions' log level to INFO; DEBUG messages will be omitted
# logging.getLogger('transitions').setLevel(logging.DEBUG)


class CustomMachine(Machine):
    """ """

    # @staticmethod
    # def resolve_callable(func, event_data):
    #     # manipulate arguments here and return func, or super() if no manipulation is done.
    #     print(f"resolve_callable({func}, {event_data})")
    #     super(CustomMachine, CustomMachine).resolve_callable(func, event_data)


config = {}
with open("machine.yaml") as f:
    yaml_config = f.read()
    config = yaml.safe_load(yaml_config)

# print(config)

# adding a model to the configuration
nmax = 100
models = [Model(f"flx{i}") for i in range(nmax)]
config["model"] = models

# **config unpacks arguments as kwargs
machine = Machine(**config, auto_transitions=False)
machine.add_ordered_transitions(
    ["Unknown", "Started", "Initialized", "Configured", "Running", "Stopped"],
    conditions="a_coin_toss",
    loop=False,
)
# print('Model')
# print(model.__dict__)

# print('Machine')
# print(machine.__dict__)

# for state in machine.states:
#     print(state, machine.get_triggers(state))

for model in machine.models:
    Model.stats[machine.initial].append(model)

# while model.next_state():
#   print(model.state, machine.get_triggers(model.state))
#   if model.state=="Error":
#     break

# print(machine.transitions)

# for t in [ "start", "initialize", "configure", "run", "stop" ]

dump = pickle.dumps(machine)

while len(Model.stats["Stopped"]) < nmax:
    machine = pickle.loads(dump)

    for model in models:
        # assert model.is_Unknown()
        try:
            if model.state == "Error":
                model.trigger("reset")
            if not model.state == "Stopped":
                model.next_state()
        except Exception as e:
            # print(e)
            model.trigger("error")

    print({state: len(Model.stats[state]) for state in machine.states})
    dump = pickle.dumps(machine)
