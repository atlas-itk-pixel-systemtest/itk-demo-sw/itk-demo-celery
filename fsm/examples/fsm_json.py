from transitions import Machine
import json


class Model:
    def say_hello(self, name):
        print(f"Hello {name}!")


# import json
json_config = """
{
  "name": "MyMachine",
  "states": [
    "A",
    "B",
    { "name": "C", "on_enter": "say_hello" }
  ],
  "transitions": [
    ["go", "A", "B"],
    {"trigger": "hello", "source": "*", "dest": "C"}
  ],
  "initial": "A"
}
"""

model = Model()

config = json.loads(json_config)
config["model"] = model  # adding a model to the configuration
m = Machine(**config)  # **config unpacks arguments as kwargs
assert model.is_A()
model.go()
assert model.is_B()
model.hello("world")  # >>> Hello world!
assert model.state == "C"
