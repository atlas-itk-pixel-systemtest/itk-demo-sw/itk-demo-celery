# export
from transitions.extensions.markup import MarkupMachine
import json
import yaml


class Model:
    def say_hello(self, name):
        print(f"Hello {name}!")


model = Model()
m = MarkupMachine(model=None, name="ExportedMachine")
m.add_state("A")
m.add_state("B")
m.add_state("C", on_enter="say_hello")
m.add_transition("go", "A", "B")
m.add_transition(trigger="hello", source="*", dest="C")
m.initial = "A"
m.add_model(model)
model.go()

print("JSON:")
print(json.dumps(m.markup, indent=2))
print("\nYAML:")
print(yaml.dump(m.markup))

config2 = json.loads(json.dumps(m.markup))  # simulate saving and loading
m2 = MarkupMachine(markup=config2)
model2 = m2.models[0]  # get the initialized model
assert model2.is_B()  # the model state was preserved
model2.hello("again")  # >>> Hello again!
assert model2.state == "C"
