import asyncio

from microdot.test_client import TestClient
from rich import print

from server import app


async def test_hello_world():
    client = TestClient(app)
    res = await client.get("/health")
    # res = await client.get('/trigger/probe')
    print(f"Status Code: {res.status_code}")
    print(f"Reason: {res.reason}")
    print(f"Headers: {res.headers}")
    print("Body:")
    if res.json:
        print(res.json)
    elif res.text:
        print(res.text[0:100])
        print(" ... + " + str(len(res.text) - 100) + " more chars")
    else:
        print(res.body[0:100])

    assert res.status_code == 200
    # assert res.text == 'Hello, World!'


if __name__ == "__main__":
    asyncio.run(test_hello_world())
