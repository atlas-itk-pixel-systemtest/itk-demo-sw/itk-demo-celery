import json
import logging

from rich import inspect, print
from rich.pretty import pprint
from ruamel.yaml import YAML
from transitions import Machine as M
from transitions.extensions.markup import MarkupMachine as MM

# from transitions.extensions.nesting import NestedState
from fsm_demi.machines import DemiMachine

log = logging.getLogger(__name__)

yaml = YAML()
# NestedState.separator = "↦"

# print(HGM.__mro__)
# inspect(HGM)

status_tags = ["ok", "warning", "error", "undefined"]


def get_device_conf(name):
    return {
        "name": name,
        "states": [
            {
                "name": "idle",
                "tags": status_tags,
            },
            {
                "name": "running",
                "tags": status_tags,
            },
            {
                "name": "error",
                "tags": status_tags,
            },
        ],
        "transitions": [
            ["run", "idle", "running"],
            ["stop", "running", "idle"],
            ["dev-reset", ["running", "error"], "idle"],
            ["dev-error", ["idle", "running"], "error"],
        ],
        "initial": "idle",
    }


card_conf = {
    "name": "card",
    "states": [
        {
            "name": "initial",
            "tags": status_tags,
            "status": "ok",
        },
        {
            "name": "unknown",
            "tags": status_tags,
        },
        {
            "name": "waiting",
            "tags": status_tags,
        },
        {
            "name": "initialized",
            "tags": status_tags,
        },
        {
            "name": "devices",
            "parallel": [
                get_device_conf("dev0"),
                get_device_conf("dev1"),
            ],
        },
        "card-error",
    ],
    "transitions": [
        ["probe", "initial", "waiting"],
        ["not-found", "initial", "unknown"],
        ["initialize", "waiting", "initialized"],
        [
            "configure",
            "initialized",
            "devices",  # <--dest: device_conf
        ],
        ["card-error", ["waiting", "initialized"], "card-error"],
        ["card-reset", ["devices", "card-error"], "waiting"],
    ],
    "initial": "initial",
    "send_event": True,
    "queued": True,
    # --- machine callbacks ------------------------
    # 'prepare_event': ['prepare_event_callback'],
    # 'before_state_change': [],
    # 'after_state_change': [],
    # 'on_exception': [],
    # 'finalize_event': [],
}

graph_conf = {
    "title": "Card/Device",
    "use_pygraphviz": False,
    "show_conditions": False,
    "show_auto_transitions": False,
    "show_state_attributes": True,
}

machine_conf = {
    "auto_transitions": False,
    "ignore_invalid_triggers": True,
    # "model_attribute": "card"
}

# CALLBACK ORDER
# 'machine.prepare_event'	    source	# executed once before individual transitions are processed
# 'transition.prepare'	        source	# executed as soon as the transition starts, set up conditions
# 'transition.conditions'	    source	# conditions may fail and halt the transition
# 'transition.unless'	        source	# conditions may fail and halt the transition
# 'machine.before_state_change'	source	# default callbacks declared on model
# 'transition.before'	        source	# transition will happen at this point
# 'state.on_exit'	            source  # callbacks declared on the source state
# ---
# 'state.on_enter'	            dest    # callbacks declared on the destination state
# 'transition.after'	        dest
# 'machine.after_state_change'	dest    # default callbacks declared on model
# 'machine.on_exception'		        # callbacks will be executed when an exception has been raised
# 'machine.finalize_event'		        # callbacks will be executed even if no transition took place or an exception has been raised


def create_machine(*argv, **kwargs):
    m = DemiMachine(
        *argv,
        **kwargs,
        **card_conf,
        # **device_conf,
        **graph_conf,
        **machine_conf,
    )

    # inspect(m, methods=True)
    # print(vars(m))
    # print(m.markup)
    # print(m.states)
    # print(m.events)
    return m


def dump_machine(m):
    print("JSON:")
    print(json.dumps(m.markup, indent=2))

    print("\nYAML:")
    print(yaml.dump(m.markup))

    with open("fsm_export_nested.dump.yaml", "w") as yf:
        d = yaml.dump(m.markup, yf, indent=4)


def draw_graph(m):
    g = m.get_graph(
        # show_roi=True,  # region of interest = (previous state, active state and all reachable states)
    )
    # print(g)
    g.draw("./public/graph.png", prog="dot")


def load_machine(filename="fsm_export_nested.yaml"):
    # config2 = json.loads(json.dumps(m.markup))  # simulate saving and loading

    with open(filename) as yf:
        config2_ry = yaml.load(stream=yf)

    config2 = dict(config2_ry)

    del config2[".counter_template"]
    pprint(config2, expand_all=True)
    m = HGM(markup=config2)
    inspect(m)
    draw_machine(m)

    # model2 = m2.models[0]  # get the initialized model
    # assert model2.is_B()  # the model state was preserved
    # model2.hello('again')  # >>> Hello again!
    # assert model2.state == 'C'

    return m


class Model:
    pass


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    m = create_machine(model=None)
    # m = DM(model=None)

    # m.show_graph
    # dm = jsons.dump(M())
    # print(m.markup)

    print("Machine:")
    # print(m)
    print(vars(m))

    print("Model:")
    mod = Model()
    m.add_model(mod)
    # inspect(m.model, methods=True)
    # print(m.model)
    print(vars(mod))
