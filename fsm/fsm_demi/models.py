# import random
import logging
from collections import defaultdict

log = logging.getLogger(__name__)


class Model:
    # model class statistics
    stats = defaultdict(list)

    def __init__(self, name) -> None:
        self.name = name

    ## Machine Callbacks

    def prepare_event(self, event):
        log.debug(f"machine.prepare_event {event.args} {event.kwargs}")
        return

    def before_state_change(self, event):
        log.debug(f"machine.before_state_change {event.args} {event.kwargs}")
        Model.stats[self.state].remove(self)
        return

    def after_state_change(self, event):
        log.debug(f"machine.after_state_change {event.args} {event.kwargs}")
        Model.stats[self.state].append(self)
        return

    def on_exception(self, event):
        log.debug(f"on_exception {event.args} {event.kwargs}")
        return

    def finalize_event(self, event):
        log.debug(f"machine.finalize_event {event.args} {event.kwargs}")
        return

    ## TRANSITION CALLBACKS
    # Prepare
    def prepare_start(self, event):
        log.debug("Preparing start")
        return True

    # Conditions
    def check_start(self, event):
        log.info(f"Checking if start possible...")
        return True

    def check_start_2(self, event):
        log.debug(f"Check 2 if start possible...")
        return True

    def check_start_3(self, event):
        log.debug(f"Check 3 if start possible...")
        return True

    def check_configuration_available(self, event):
        log.debug(f"Check if configuration available")
        return True

    # Unless (Vetoes)
    def unless_start(self, event):
        log.debug("Start possible unless...")
        return False

    # Before
    def before_start(self, event):
        log.debug("Before start...")
        return

    # After
    def after_start(self, event):
        log.debug("After start...")
        return

    ## State Callbacks

    def on_enter_Started(self, event):
        log.debug(f"{self.name}: Entering started")
        return

    def on_exit_Started(self, event):
        log.debug(f"{self.name}: Exiting started")
        return

    # Properties

    # @property
    # def a_coin_toss(self):
    #     """Basically a coin toss."""
    #     r = random.random()
    #     log.debug(f"Coin toss: {r}")
    #     if r < 0.2:
    #         raise Exception('error')
    #     return r < 0.5

    # @property
    # def a_random_error(self):
    #     """ """
    #     r = random.random() < 0.5
    #     log.debug(f"a_random_error: {r}")
    #     if not r:
    #         raise Exception('error')
    #     return r
