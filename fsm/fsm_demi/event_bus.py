import inspect
import logging
from readline import insert_text

from rich import inspect, print

# from transitions.core import listify

# logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


class EventBus:
    def __init__(self):
        self.members = []
        # result store for callback results of last broadcast
        self.results = {}

    def add_member(self, member):
        """from FAQ: Member can be a model or a machine acting as a model
        We decorate each member with an 'emit' function to fire events.
        EventBus will then broadcast that event to ALL members, including the one that triggered the event.
        Furthermore, we can pass a payload in case there is data that needs to be sent with an event.
        """
        log.info(f"Adding {member}.emit()")
        setattr(member, "emit", self.broadcast)
        self.members.append(member)

    def broadcast(self, event, payload=None):
        for member in self.members:
            rs = self.results[member.name] = {}
            try:
                log.debug("Sending trigger '%s' to '%s'", event, member.name)
                member.trigger(event, payload, result_store=rs)
                log.info("New state of '%s' is '%s'", member.name, member.state)
            except Exception as exc:
                log.error("Cancelling broadcast due to %s", exc)
                # pass on to server
                raise

        print("Results:")
        print(self.results)
        return self.results

    def gather(self):
        r = []
        for member in self.members:
            r.append(
                {
                    "name": member.name,
                    "state": member.state,
                    "reachable": member.get_triggers(member.state),
                }
            )
        return r


# bus = EventBus()
# machine = Machine()
# bus.add_member(machine)
# bus.broadcast('boot')
