import logging
from functools import partial
from re import S

from rich.pretty import pprint
from transitions import EventData
from transitions.core import State, listify
from transitions.extensions.diagrams import HierarchicalGraphMachine as HGM
from transitions.extensions.nesting import NestedState
from transitions.extensions.states import Tags, add_state_features

from fsm_demi.states import DynamicNestedState

log = logging.getLogger(__name__)


@add_state_features(Tags)  # ("ok", "warning", "error", "unknown"))
class DemiMachine(HGM):
    # state_cls = StatusState
    state_cls = DynamicNestedState

    # def _checked_assignment(self, model, name, func):
    #     """ keep my machine clean
    #     don't add convenience methods, except 'trigger'
    #     """
    #     if name == "trigger":
    #         setattr(model, name, func)
    #         return
    #     # log.debug(f"not setting {name}")

    # def _on_after_state_change(self, event: EventData):
    #     to_state = event.transition.dest
    #     self._history_df.append(
    #         {"start_index": self._index, "state": to_state}, ignore_index=True
    #     )

    # Machine callbacks

    def prepare_event_callback(self, event_data):
        """prepare_event callback"""
        log.info("prepare_event on %s", event_data.machine)

        result_store = event_data.kwargs["result_store"]
        rs = result_store["prepare_event"] = {}
        rs["event_data"] = event_data

        # pprint(vars(event_data))
        # pprint(vars(event_data.state))
        # pprint(vars(event_data.event))


""" library of features to eventually use
"""


class FeatureDemiMachine:
    @staticmethod
    def resolve_callable(func, event_data):
        """From FAQ
        manipulate arguments here and return func, or super() if no manipulation is done.
        """
        print(f"resolving {func}")
        super(DemiMachine, DemiMachine).resolve_callable(func, event_data)

    # From FAQ: PeekMachine

    def _can_trigger(self, model, *args, **kwargs):
        """We can omit the first two arguments state and event since they are only needed for
        actual state transitions. We do have to pass the machine (self) and the model as well as
        args and kwargs meant for the callbacks.
        """
        e = EventData(None, None, self, model, args, kwargs)

        return [
            trigger_name
            for trigger_name in self.get_triggers(model.state)
            if any(
                all(c.check(e) for c in t.conditions)
                for t in self.events[trigger_name].transitions[model.state]
            )
        ]

    # override Machine.add_model to assign 'can_trigger' to the model
    def add_model(self, model, initial=None):
        for mod in listify(model):
            mod = self if mod is self.self_literal else mod
            if mod not in self.models:
                setattr(mod, "can_trigger", partial(self._can_trigger, mod))
                super(PeekMachine, self).add_model(mod, initial)

    # From FAQ: OverrideMachine(Machine):

    # def _checked_assignment(self, model, name, func):
    #     setattr(model, name, func)

    # From FAQ: class CallingMachine(Machine):

    # def _checked_assignment(self, model, name, func):
    #     if hasattr(model, name):
    #         predefined_func = getattr(model, name)
    #         def nested_func(*args, **kwargs):
    #             predefined_func()
    #             func(*args, **kwargs)
    #         setattr(model, name, nested_func)
    #     else:
    #         setattr(model, name, func)
