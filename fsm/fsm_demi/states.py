import logging

from transitions.extensions.nesting import NestedState

# custom pytransitions States
log = logging.getLogger(__name__)


class DynamicNestedState(NestedState):
    """From FAQ:
    Need to dynamically get the on_enter and on_exit callbacks since the
    model can not be registered to the Machine due to Memory limitations
    """

    def __init__(self, *args, **kwargs):
        self.status = kwargs.pop("status", "")
        super(DynamicNestedState, self).__init__(*args, **kwargs)

    def enter(self, event_data):
        """Triggered when a state is entered."""
        log.debug(
            "%sEntering state '%s'. Processing callbacks...",
            event_data.machine.name,
            self.name,
        )
        if hasattr(event_data.model, f"on_enter_{self.name}"):
            event_data.machine.callbacks(
                [getattr(event_data.model, f"on_enter_{self.name}")], event_data
            )
        log.debug(
            "%sFinished processing state '%s' enter callbacks.",
            event_data.machine.name,
            self.name,
        )
        self.status = "ok"

    def exit(self, event_data):
        """Triggered when a state is exited."""
        log.debug(
            "%sExiting state '%s'. Processing callbacks...",
            event_data.machine.name,
            self.name,
        )
        if hasattr(event_data.model, f"on_exit_{self.name}"):
            event_data.machine.callbacks(
                [getattr(event_data.model, f"on_exit_{self.name}")], event_data
            )
        log.debug(
            "%sFinished processing state '%s' exit callbacks.",
            event_data.machine.name,
            self.name,
        )
