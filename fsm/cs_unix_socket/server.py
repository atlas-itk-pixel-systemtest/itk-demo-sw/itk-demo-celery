#!/usr/bin/env python

import json
import logging
import os
import socket
import sys
import uuid
import traceback
from logging_tree import printout

from rich.logging import RichHandler
from rich import inspect

# from transitions import Machine
from event_bus import EventBus
from machine_factory import create_machine

logging.basicConfig(
    level=logging.NOTSET, format="%(message)s", datefmt="[%X]", handlers=[RichHandler()]
)

# logging.getLogger('transitions').setLevel(logging.DEBUG)
# logging.getLogger('transitions').handlers=[RichHandler()]

log = logging.getLogger("rich")
log.info("Booting FSM server ...")
printout()

SOCK_FILE = "/dev/shm/fsm-ipc.socket"


def run(bus: EventBus):
    # Setup socket
    if os.path.exists(SOCK_FILE):
        os.remove(SOCK_FILE)

    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    s.bind(SOCK_FILE)
    os.chmod(SOCK_FILE, 0o666)
    s.listen()
    log.info("Start listening loop")
    while True:
        # Accept 'request'
        conn, addr = s.accept()
        log.info("Connection by client")

        # Process 'request'
        while True:
            data = conn.recv(4096)
            if not data:
                break
            msg = json.loads(data)
            event = msg["event"]
            payload = uuid.UUID(msg["payload"])
            log.info(f"Received message: {event} ({payload})")

            try:
                bus.broadcast(event, payload)
            except Exception as err:
                log.error(f"Broadcast '{event}' failed due to {err}")
                # exc_info = sys.exc_info()
                # exc_str=''.join(traceback.format_exception(*exc_info))
                msg = {"error": f"{err}"}

            # Send 'response'
            data = bus.gather()
            jdata = json.dumps(data)
            log.info(f"Response: {jdata}")
            conn.sendall(jdata.encode("utf-8"))


if __name__ == "__main__":
    bus = EventBus()
    m = create_machine()
    # inspect(m, methods=True, help=False)
    # print(m)
    bus.add_member(m)
    # bus.broadcast('boot')

    try:
        run(bus)
    except KeyboardInterrupt:
        log.info("Seen CTRL^C, exiting...")
