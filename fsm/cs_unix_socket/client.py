import os
import socket
import sys
import uuid
import json
import logging

from rich import print

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

SOCK_FILE = "/dev/shm/fsm-ipc.socket"


def send_msg(event, payload=""):
    # Init socket object
    if not os.path.exists(SOCK_FILE):
        print(f"File {SOCK_FILE} doesn't exist")
        sys.exit(-1)

    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    s.connect(SOCK_FILE)

    # Send request
    msg = dict(
        event=event,
        payload=str(uuid.uuid4()),
    )
    jmsg = json.dumps(msg)
    print(f"Sending:  {jmsg}")
    s.sendall(jmsg.encode("utf-8"))

    # Wait for response
    data = s.recv(1024)
    # print(data)
    # print(data.decode('utf-8'))
    response = json.loads(data.decode("utf-8"))

    print(f"Response: {response}")
    # print(msg['event'])
    # print(uuid.UUID(msg['uuid']))

    # rcv_uuid = uuid.UUID(bytes=data)
    # print(f"Received bytes: {repr(rcv_uuid)}")


if __name__ == "__main__":
    send_msg("probe")
    # send_msg('init')
    # send_msg('config')
    # send_msg('run')
    # send_msg('stop')
    # send_msg('reset')
