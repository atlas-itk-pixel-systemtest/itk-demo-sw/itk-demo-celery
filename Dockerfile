FROM python:slim
# ENV PYTHONUNBUFFERED 1
# ENV PYTHONDONTWRITEBYTECODE 1

# ENV USER=celery
# WORKDIR /app
# RUN useradd -ms /bin/bash ${USER}
# COPY . /app
# RUN chown -R ${USER}:${USER} /app
# USER ${USER}

# ENV USER=itk
# RUN groupadd -r ${USER} && useradd --no-log-init -r -g ${USER} ${USER}
# ENV PATH="/home/${USER}/.local/bin:${PATH}"
# WORKDIR /home/${USER}
# USER ${USER}



WORKDIR /root
# COPY --chown=${USER}:${USER} . .

COPY requirements.txt .
RUN pip --no-cache-dir install -r requirements.txt


COPY . .
ENV PYTHONUNBUFFERED=TRUE

RUN pip --no-cache-dir install .


# ENTRYPOINT [ "celery", "-A", "itk_demo_celery" ]
