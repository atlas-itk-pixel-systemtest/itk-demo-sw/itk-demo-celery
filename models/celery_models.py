
from pydantic import BaseModel, Field

class TaskRoutesModel(BaseModel):
    routers : tuple

class TaskRoutersModel(BaseModel):
    routers : list

class TaskRouteModel(BaseModel):
    queue: str
    routing_key: str



# Kombu models
class ChannelT(BaseModel):
    _type: str

class ExchangeModel(BaseModel):
    name: str
    _type: str = Field(default = 'direct')
    channel: ChannelT


class QueueModel(BaseModel):
    name: str
    exchange: ExchangeModel
    routing_key: str
    # channel: ChannelT
    # bindings: str
    # on_declared: callable
    # RabbitMQ options
    queue_arguments: dict
