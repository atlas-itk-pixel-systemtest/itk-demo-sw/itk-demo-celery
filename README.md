<a name="readme-top"></a>
![GitLab Issues](https://img.shields.io/gitlab/issues/open/atlas-itk-pixel-systemtest%2Fitk-demo-sw%2Fitk-demo-celery?gitlab_url=https%3A%2F%2Fgitlab.cern.ch&style=flat)
![GitLab Merge Requests](https://img.shields.io/gitlab/merge-requests/open/atlas-itk-pixel-systemtest%2Fitk-demo-sw%2Fitk-demo-celery?gitlab_url=https%3A%2F%2Fgitlab.cern.ch&style=flat)


# itk-demo-celery

## Generic Celery instrumentation for DeMi

This repo contains several sub-projects serving as extensions, tests and examples to use Celery with DeMi.
Roughly ordered by development from advanced down to draft:

| Subdirectory | Description |
|--|--|
| `topology`  | celery middleware tool |
| `api_BE` |  celery middleware example backend |
| `benchmarking` | celery topology and tasks benchmarking suite |
| `stacks` | docker compose stacks for Celery cluster monitoring and infrastructure services (eg. Flower and RabbitMQ) |
| `baseimage` | Generic Alma9 Docker image with s6-init and itk user --> *depreceated*, please switch to [demi-baseimage](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi-baseimage) |
| `celeryimage` | Generic Celery Docker image = baseimage + base_celery  |
| &nbsp;&nbsp;&nbsp;&nbsp;/base_celery | Custom Celery app, tasks and helpers |
| `original` | initial shell based Celery example by Matthias Wittgen |
| `fsm` | Backend FSM example |
| `models` | drafts for Pydantic models to describe celery cluster |
| `itk_demo_celery` | microservice template (top-level project) |
| `demice` | cluster stress testing suite |
| `tui` | Textual based monitoring tool |
| `swrod-model` | ITk Celery cluster simulation |
| `director-example` | example how DeMi could be used via celery-director |
| `.devcontainer` | Development container with preinstalled tools |


### Built With

- [Celery](https://docs.celeryq.dev/en/stable/getting-started/introduction.html)
- [pytransitions](https://github.com/pytransitions)
- [Microdot](https://github.com/miguelgrinberg/microdot)

## Getting Started

### Prerequisites

### Installation

## Usage

## Contributing

--> see CONTRIBUTING.md

## License

--> see LICENSE

## Contact

--> see AUTHORS

## Acknowledgments

--> see AUTHORS

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[issues-shield]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-celery/-/issues
[issues-url]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-celery/-/issues
