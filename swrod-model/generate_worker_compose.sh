#!/usr/bin/env bash

# _root=$(dirname "${BASH_SOURCE[0]}")
# _root=$(cd "${_root}/" >/dev/null 2>&1 && pwd)
m_cmd=${1:-start}
n_device=4
n_swrod=4
# echo "$n_worker"
# CELERY_WORKDIR=$_root/itk_demo_celery
# CELERY_NODE_NAME=card #DAQ card basename

# CELERY_PID_FILE="$_root/var/run/celery/%n.pid"
# CELERY_LOG_FILE="$_root/var/log/celery/%n%I.log"

# mkdir -p "$CELERY_PID_FILE"
# mkdir -p "$CELERY_LOG_FILE"

cat <<- EOF
version: "3"
name: workers
networks:
  default:
    name: deminet
    external: true
services:
  builder:
    build: 
      context: ..
      dockerfile: ${PWD}/Dockerfile
    image: flx-worker
EOF
  # worker:
  #   image: itk-demo-celery
  #   build: .

write_compose() {
  cat <<- EOF
  ${q}:
    container_name: ${q}
    hostname: ${q}
    image: flx-worker
    command: celery -A itk_demo_celery worker -n ${q} -S state.db -l INFO -c 1 -P solo -E -Q ${q}
EOF
}

    # image: itk-demo-celery

  # | tee workers.compose.yaml

qnames=""
q=""
for ((j = 0; j < n_swrod; j++)); do
  for ((i = 0; i < n_device; i++)); do
    q="flx${j}_dev${i}"
    # echo # $q
    qnames="$qnames -Q:${i} $q"
    nodes="$nodes dev${i}@flx${j}" #@localhost"

    write_compose
  done
done

# echo "# command: $m_cmd"
# echo "# nodes:  $nodes"
# echo "# queues: $qnames"

# --workdir="$CELERY_WORKDIR" \

