#!/usr/bin/env python

import json

# from celery.utils.log import get_logger
# logger = get_logger(__name__)
import logging
import os
from pathlib import Path
# from inspect import signature
from pprint import pprint

from itk_demo_celery.cel.app import app
from itk_demo_celery.cel.tasks import cleanup, error_handler

from celery import group, signature

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

devices = 4
swrods = 4
# configdir = os.path.dirname(os.path.abspath(__file__))
configdir = Path('..')
configfile = configdir / "data/scanConfig.json"
resultfile = configdir / "data/scanResult.json"
config = None
result = None

with open(configfile, "r") as f:
    config = json.load(f)

with open(resultfile, "r") as f:
    result = json.load(f)

task_list = list()


def on_raw_message(body):
    if body["status"] == "PROGRESS":
        return
    #     result = body["result"]
    #     print(body["task_id"], result["stage"])
    # else:
    print(body)


def create_tasks():
    for j in range(0, swrods):
        for i in range(0, devices):
            name = f"flx{j}_dev{i}"
            logger.info(f"Start device {name}")
            task = signature(
                "itk_demo_celery.cel.tasks.run",
                ("config.json", "result.json"),
                kwargs={},
                queue=name,
            )
            task_list.append(task)
            # logger.info("signature:")
            
            print(json.dumps(task))

    return task_list

def send_tasks(task_list):
    task_group = group(task_list)
    async_result = task_group.apply_async()
    logger.info(f"Sent {len(task_list)} task(s)")

    return async_result

task_list = create_tasks()

try:
    async_result = send_tasks(task_list)
    res = async_result.get(
        on_message=on_raw_message,
        propagate=False,
    )
    print(res)
except Exception as e:
    logger.error(f"Sending task raised: {type(e).__name__} {e.args}")
