#!/usr/bin/env python

import json
import os
from rich import print
from swrod_model.celery import app

print(os.environ["CELERY_BROKER_URL"])

app.conf.update(
    broker_url=os.environ["CELERY_BROKER_URL"],
)


class CeleryEventsHandler:
    def __init__(self, celery_app):
        self._app = celery_app
        self._state = celery_app.events.State()

    def _event_handler(self, event):
        self._state.event(event)
        if event["type"] == "worker-heartbeat":
            return
        # print(json.dumps(event, default=str))
        if event["type"].startswith("task-"):
        #     task = self._state.tasks.get(event["uuid"])
        #     print(json.dumps(vars(task), default=str))
            print(f'{event["type"]} ({event["hostname"]}): {event["uuid"]}')
        else:
            print(f'{event["type"]} ({event["hostname"]})')
        # handler(self, event)


    def start_listening(self):
        with self._app.connection() as connection:
            recv = self._app.events.Receiver(
                connection, handlers={"*": self._event_handler}
            )
            recv.capture(limit=None, timeout=3600)


if __name__ == "__main__":
    events_handler = CeleryEventsHandler(app)
    events_handler.start_listening()
