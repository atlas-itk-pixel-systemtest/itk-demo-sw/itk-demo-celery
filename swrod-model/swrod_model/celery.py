from celery import Celery
import time

# CELERY_BROKER_URL = os.environ["CELERY_BROKER_URL"]

app = Celery(__name__)

app.conf.update(
    # broker_url=os.environ["CELERY_BROKER_URL"],
    result_backend="redis://localhost:6379/0",
    # result_backend="rpc://",
    broker_connection_retry_on_startup=True,
    enable_utc=True,
    timezone=time.tzname,
    imports=("swrod_model.tasks",),
    # task_ignore_result=True,
    # task_routes=("task_router.TaskRouter"),
    # task_serializer="json",
    # result_serializer="json",
    # accept_content=["json"],
)

