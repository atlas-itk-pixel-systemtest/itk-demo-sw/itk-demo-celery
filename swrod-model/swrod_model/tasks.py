from __future__ import annotations
from .celery import app
from celery import Task
from celery.result import AsyncResult
from celery import shared_task
import random
from time import sleep
from celery.utils.log import get_task_logger

log = get_task_logger(__name__)


# import os
# import fcntl
# def lock_file(f):
#     if f.writable(): fcntl.lockf(f, fcntl.LOCK_EX)
# def unlock_file(f):
#     if f.writable(): fcntl.lockf(f, fcntl.LOCK_UN)
# class AtomicOpen:
#     def __init__(self, path, *args, **kwargs):
#         self.file = open(path,*args, **kwargs)
#         lock_file(self.file)

#     def __enter__(self, *args, **kwargs): return self.file

#     def __exit__(self, exc_type=None, exc_value=None, traceback=None):        
#         self.file.flush()
#         os.fsync(self.file.fileno())
#         unlock_file(self.file)
#         self.file.close()
#         if (exc_type != None): return False
#         else:                  return True 
        

# with AtomicOpen('tasks.lock','a') as f:
#     f.write('0')

counter = 0

class DebugTask(Task):

    def __call__(self, *args, **kwargs):
        log.info(f'TASK STARTING: {self.name}[{self.request.id}]')
        return self.run(*args, **kwargs)
 
@app.task(
        bind=True, 
        # base=DebugTask,
        )
def info(self, cmd : str):
    """
    """
    return counter
    # return vars(self.request)

@shared_task(        
# @app.task(
        bind=True, 
        # base=DebugTask,
        )
def action(self, cmd : str):
    """
    Request action
    """
    global counter
    t = random.randint(0,1000)
    sleep(t/1000)
    # with AtomicOpen('tasks.lock','a') as f:
    #     counter = int(f.read())
    #     print (f'lock acquired {counter}')
    counter += 1
    #     f.write(counter)

    if not self.request.called_directly:
        self.update_state(state='PROGRESS',
            meta={'counter': counter, 'total': 1000})

    return self.request.__dict__

# @app.get("/result/<id>")
# @app.task(
@shared_task(        
        bind=True, 
        # base=DebugTask,
        )
def result(self, id: str):
    result = AsyncResult(id)
    # print(vars(result))
    return {
        "ready": result.ready(),
        "successful": result.successful(),
        "value": result.result if result.ready() else None,
    }
