# from celery import Task
from swrod_model.celery import app
from swrod_model.tasks import info, result, action, DebugTask
from celery.result import AsyncResult
from time import sleep
from rich import print

# app.Task=DebugTask

print(app.tasks)

def send_tasks():
    for i in range(10):
        r = action.apply_async((), {'cmd':'test'}, queue='svc0.node0')

        # while not r.ready():
        print(r.id, r.state)
        
        # print(r.get(), r.state)

        while True:
            r2 = result.apply_async(
                (), {'id':r.id},
                queue='svc0.node0',
                # ignore_result=False,
            )
            print('--> ', r2.id, r2.state, r2.get())
            # rd = r2.get()
            # print(rd)
            if r2.get()['ready']: 
                print(r.id, r.state)            
                break
            sleep(0.5)

        print(app.events.State())

# print(vars(r))
# print(r2)

sig = action.s((), {'cmd':'test'}, queue='svc0.node0')
print(sig)
print(sig.__dict__)

while True:
  send_tasks()
  sleep(5)
