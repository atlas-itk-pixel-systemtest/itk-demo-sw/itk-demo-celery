#
# write compose files for felix nodes
#
# /stacks/flxnode{{xx}}/compose.yaml
#
import pathlib
import logging
import subprocess

logging.basicConfig(level=logging.INFO)
log=logging.getLogger(__name__)

node_prefix='node'
service_prefix='svc'
image = 'swrod-model'
app = 'swrod_model'
broker = "amqp://guest:guest@rabbitmq:5672//"
# result_backend = "rpc://"
result_backend="redis://redis:6379/0"

def run(cmd):
    try:
        r = subprocess.run(
            [c for c in cmd.split()],
            capture_output=True,
            check=True
            )
    except Exception as e:
        log.exception(e)
    
    return r

def get_compose_yaml(node):


    cy = f"""
version: "3"
name: {node}
services:
"""
    for svc in [f'{service_prefix}{i}' for i in range(4)]:
        cy += f"""
  {svc}:
    image: {image}
    restart: no
    container_name: {svc}.{node}
    hostname: {svc}.{node}
    environment:
        - CELERY_TRACE_APP=1
    # entrypoint: ["bash", "-c"]
    command: [ 
        "celery", 
        "--app={app}",
        "--broker={broker}",
        "--result-backend={result_backend}",
        # "--loader=",
        # "--config=",
        # "--workdir=",
        "worker",
        ### Worker Options
        "--hostname=worker@%h",
        "--statedb={svc}.state.db",
        # "--detach",
        "--loglevel=INFO",
        # "--optimization=fair",
        # "--prefetch-multiplier=4",
        ### Pool Options
        # "--concurrency=1",
        # "--pool=solo",
        "--events",
        "--without-gossip",
        "--without-mingle",
        # "--without-heartbeat",
        "--queues={svc}.{node}"
        ]
"""
        # log.info(f'{svc}_{node}')

    cy += """
networks:
  default:
    name: deminet
    external: true
"""

    # log.info(cy)
    return cy

def gen_node(p, node):

    pn = p / node
    # p = pathlib.Path(pi)
    pn.mkdir(parents=True, exist_ok=True)

    log.info(pn)

    fn = "compose.yaml"
    filepath = pn / fn
    with filepath.open("w", encoding ="utf-8") as f:
        cy = get_compose_yaml(node)
        f.write(cy)

    run(f'docker compose -f {filepath} config')

def gen_stacks(stacks):
    p = pathlib.Path("stacks/")
    p.mkdir(parents=True, exist_ok=True)

    for i in stacks:
        gen_node(p,i)

if __name__ == '__main__':

    gen_stacks([f'{node_prefix}{n}' for n in range(4)])
    run('tree')

