from celery import Celery
from celery.signals import task_success
from time import sleep

app = Celery(
    "tasks",
    broker=f"pyamqp://guest:guest@wuppo-charon.cern.ch",
    backend=f"redis://wuppo-charon.cern.ch:6379/0",
)


@app.task
def add(x, y):
    sleep(5)
    return x + y


@app.task
def reverse(text):
    return text[::-1]


@app.task
def is_prime(n):
    if n % 2 == 0:
        return False
    else:
        i = 3
        while i < n / 2:
            print(i)
            if n % i == 0:
                return False
            i = i + 1
        return True


@task_success.connect
def on_task_success(sender, result, **kwargs):
    if sender.name == "tasks.add":
        print(f"Task {sender.name} executed successfully with result: {result}")
        print(f"{result} is prime : " + str(is_prime(result)))
    else:
        print(f"Task {sender.name} executed successfully with result: {result}")


# add.delay(4,5)
# reverse.delay("signal")
