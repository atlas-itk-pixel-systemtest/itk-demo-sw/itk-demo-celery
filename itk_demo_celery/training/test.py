from itk_demo_celery.training.tasks import add, reverse, is_prime
from itk_demo_celery.cel.signals import task_success


results = []
for i in range(5):
    result = add.delay(i, 4)
    results.append(result)

    # print(result.ready())
for i in range(5):
    print(results[i].get())

# result.get(propagate=False)
# In case the task raised an exception, get() will re-raise the exception, but you can override this by specifying the propagate argument

# result2 = reverse.apply_async(args=("test",),countdown=3)
# # print(result2.ready())
# print(result2.get())

# result3 = is_prime.delay(5)
# print(str(result.get())+" is prime : " + str(result3.get()))

