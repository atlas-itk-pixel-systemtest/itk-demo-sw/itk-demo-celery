import json
import time
import os
import signal
import sys
from celery.exceptions import SoftTimeLimitExceeded
from celery.utils.log import get_task_logger
from celery.signals import task_success
from itk_demo_celery.cel.app import app
#from celery import current_task
from rich.pretty import pprint


logger = get_task_logger(__name__)

@app.task(bind=True)
def noop(self, *args, **kwargs):
    logger.info(f'[noop]: {args} {kwargs}')
    pprint(self.request, expand_all=True)
    return (args, kwargs)


@app.task(
    # name='run',
    bind=True,
    # serializer='json',
)
def run(self, config, result):
    logger.info(f"starting {self.request.id}")
    logger.info(json.dumps(f"{self.request}"))
    self.update_state(state="CONFIGURE", meta={"config_size": len(json.dumps(config))})
    n = 16
    for i in range(0, n):
        if not self.request.called_directly:
            self.update_state(state="PROGRESS", meta={"stage": i})
        time.sleep(0.25)

    # return {"result_size": len(json.dumps(result))}
    return len(json.dumps(result))


@app.task
def cleanup(result_size):
    print(f"Cleaning up {result_size}")


@app.task
def error_handler(request, exc, traceback):
    print(f"Task {request.id} raised exception: {exc!r}\n{traceback!r}")



@app.task
def add(x, y):
    print(os.environ.get("sleep_time"))
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t)

    return x + y


@app.task
def reverse(text):
    return text[::-1]


@app.task
def is_prime(n):
    if n % 2 == 0:
        return False
    else:
        i = 3
        while i < n / 2:
            print(i)
            if n % i == 0:
                return False
            i = i + 1
        return True


@app.task
def any_(*args, **kwargs):
    """Task taking any argument, returning nothing.

    This is useful for testing related to large arguments: big values,
    an insane number of positional arguments, etc.

    :keyword sleep: Optional number of seconds to sleep for before returning.

    """
    wait = kwargs.get('sleep')
    if wait:
        time.sleep(wait)

@app.task
def kill(sig=getattr(signal, 'SIGKILL', None) or signal.SIGTERM):
    """Task sending signal to process currently executing itself.

    :keyword sig: Signal to send as signal number, default is :sig:`KILL`
      on platforms that supports that signal, for other platforms (i.e Windows)
      it will be :sig:`TERM`.

    """
    os.kill(os.getpid(), sig)


@app.task
def sleeping(i, **_):
    """Task sleeping for ``i`` seconds, and returning nothing."""
    time.sleep(i)


@app.task
def sleeping_ignore_limits(i, **_):
    """Task sleeping for ``i`` seconds, while ignoring soft time limits.

    If the task is signalled with
    :exc:`~celery.exceptions.SoftTimeLimitExceeded` the signal is ignored
    and the task will sleep for ``i`` seconds again, which will trigger
    the hard time limit (if enabled).

    """
    try:
        time.sleep(i)
    except SoftTimeLimitExceeded:
        time.sleep(i)



@app.task
def exiting(status=0):
    """Task calling ``sys.exit(status)`` to terminate its own worker
    process."""
    sys.exit(status)


@app.task
def any_returning(*args, **kwargs):
    """The same as :task:`any` except it returns the arguments given
    as a tuple of ``(args, kwargs)``."""
    any_(*args, **kwargs)
    return args, kwargs

@task_success.connect
def on_task_success(sender, result, **kwargs):
    if sender.name == "tasks.add":
        logger.info("okok")
        print(f"Task {sender.name} executed successfully with result: {result}")
        print(f"{result} is prime : " + str(is_prime(result)))
    else:
        print(f"Task {sender.name} executed successfully with result: {result}")

