from invoke import task

@task
def start_worker(c,app='itk_demo_celery',help=False):
    """ Start celery worker """
    c.run(f'celery -A {app} worker', pty=True)

# import invoke
# from io import StringIO

# outStream = StringIO()
# result = invoke.run("wg genkey", out_stream=outStream)
# stdout_str = outStream.getvalue()
# print("Result: ", stdout_str)

