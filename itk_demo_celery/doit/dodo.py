#import doit


def task_start_worker():
    """ Start celery worker """
    return {
        'actions': [
            'celery worker --help'
        ]
    }    
