# print(f'[celery]Importing celeryconfig from:\n'
#       f'[celery]{__file__}')

broker_url = "amqp://guest:guest@localhost:5672//"
# broker_connection_retry_on_startup = False
result_backend = "redis://localhost:6379/0"

# enable_utc=True
# timezone='Europe/Paris'
# imports = ('base_celery.tasks',)
# auto_discover_tasks = True

# # task_annotations = {'tasks.add': {'rate_limit': '10/s'}}

# celeryd_hijack_root_logger = False
# worker_hijack_root_logger = False
# worker_redirect_stdouts = False
