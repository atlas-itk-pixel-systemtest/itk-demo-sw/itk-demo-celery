import json
import time
from rich import inspect
from rich.live import Live
from rich.panel import Panel
from rich.progress import Progress
from rich.table import Table
from rich.console import Console
from rich.text import Text


from celery import signals
from celery.result import AsyncResult
from celery.states import PENDING, STARTED

from base_celery.celery import app
from base_celery.progress.backend import TaskProgress
from base_celery.tasks.tasks import echo, noop, abort, long_task, PROGRESS


# ar = echo.delay()
# print(ar)
# r = ar.get()
# pprint(vars(r))
class TestClient:
    total = 100

    def run(self):
        # r = noop.apply()
        result = long_task.apply_async((self.total,), countdown=2)

        # inspect(result)

        # with Progress() as progress:
        progress = Progress(auto_refresh=False)
        task_bar = progress.add_task(f"{result.state} [{result.id}]")

        # columns = Columns()
        # md = Markdown("")
        # panel = 

        def get_progress_table(txt): 
            progress_table =Table.grid()
            progress_table.add_row(
                Panel.fit(
                    txt, title="Progress Log", border_style="green", padding=(2, 2),
                )
            )
            progress_table.add_row(
                Panel.fit(progress, title="[b]Jobs", border_style="red", padding=(1, 2)),
            )
            return progress_table
        with Live(get_progress_table(""), refresh_per_second=4, vertical_overflow='crop') as live:

            def on_message(body):
                # progress.console.print(
                # inspect(result)

                console = Console()
                with console.capture() as capture:
                    console.print(body)
                    # inspect(result)
                # txt = Text.from_ansi(capture.get()).plain

                # live.console.print(body)   
                live.update(get_progress_table(capture.get()))

                if result.state == PENDING:
                    return

                if result.state == STARTED:
                    progress.start_task(task_bar)

                # task_progress = TaskProgress(AsyncResult(task_id))
                # i = task_progress.get_info()

                if result.state == PROGRESS:

                    progress.update(
                        task_bar,
                        total=body['result']['total'],
                        completed=body['result']['current'],
                        description=result.state,
                        refresh=True
                    )

                return

            result.get()
            # result.get(on_message=on_message, propagate=False)

            # p = self.set_task_progress(result)
            # time.sleep(0.1)

        # self.set_task_progress(result)


# tc = TestClient()
# tc.run()


# while 1:
# if 1:

def on_message(msg):
    print(msg)
    # inspect(result)

# r{}
for i in range(1000000):
    echo.delay(i)
    if i%1000 == 0:
      print(i)
    # r2 = abort.delay('Hallo Echo')
    time.sleep(0.02)
    # r2.abort()
    # app.control
    # time.sleep(2)
    # r.get(on_message=on_message)
    # r2.get(on_message=on_message)
    # print(r2)

