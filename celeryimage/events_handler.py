#!/usr/bin/env python

import json

from rich import print, inspect

from base_celery.celery import app
from base_celery.camera import DumpCam

class CeleryEventsHandler:

    def __init__(self, app):
        self.app = app
        self.state = app.events.State()
        print(f"{self.state}")

    def worker_event_handler(self, event):
        self.state.event(event)
        # if event["type"] != "worker-heartbeat":
            # print(json.dumps(event, default=str))
        print(f'{event["type"]}')

    def task_event_handler(self, event):
        self.state.event(event)
        # if event["type"].startswith("task-"):
            # task = self._state.tasks.get(event["uuid"])
        try:
            print(f'{event["type"]} [{event["uuid"]}]')
        except Exception as e:
            inspect(event)
        # print(json.dumps(vars(task), default=str))

    def start_listening(self):
        with self.app.connection() as connection:
            recv = self.app.events.Receiver(
                connection, handlers={
                    "worker-online": self.worker_event_handler,
                    "worker-offline": self.worker_event_handler,
                    "*": self.task_event_handler,
                    }
            )
            recv.capture(limit=None, timeout=3600, wakeup=True)


def main(app, freq=2):
    state = app.events.State()
    with app.connection() as connection:
        recv = app.events.Receiver(connection, handlers={'*': state.event})
        with DumpCam(state, freq=freq):
            recv.capture(limit=None, timeout=None)


if __name__ == "__main__":
    # events_handler = CeleryEventsHandler(app)
    # events_handler.start_listening()
    main(app)
