[demiurl]: https://demi.docs.cern.ch
[appurl]: https://almalinux.org

[![demi](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/defaults/-/raw/a2b52ffa2261b381fb09e7d81b718ae2d1b54035/assets/atlas_itk_logo.png)][demiurl]
## Contact information

| Type | Address/Details |
| :---: | --- |
| Docs | [demi.docs.cern.ch][demiurl] |

[![]()]()

# DeMi Celery baseimage

## Overview

This image runs

- 1 read-only celery worker (with prefork pool) as s6 service
- 4 write celery workers (with solo pool) as s6 service
- 1 celery app as CMD

