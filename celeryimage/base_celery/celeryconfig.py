#
# Default DeMi Celery Config
#

# import os
# import time
# from kombu import Queue

print(f'[celery]Importing celeryconfig from: {__file__}')

#──── General Settings ─────────────────────────────────
enable_utc = True
# timezone = "Europe/Paris"
# timezone = time.tzname

#──── Task Settings ────────────────────────────────────
# task_annotations = {'*': {}}
# task_annotations = {'tasks.add': {'rate_limit': '10/s'}}

#──── Task execution settings ──────────────────────────
# task_remote_tracebacks = True # needs tblib
# task_ignore_result = True
# task_store_errors_even_if_ignored = True
task_track_started = True
# task_acks_late = True
# task_acks_on_failure_or_timeout

#──── Task result backend settings ─────────────────────
# result_backend_thread_safe = True
# result_extended = True
# result_expires
# override_backends

#──── RPC backend settings
# result_backend = 'rpc://'
# result_persistent = True

#──── Redis backend settings 
result_backend = "redis://redis:6379/0"

#──── File-system backend settings  ────────────────────
# result_backend = f"file://{os.getcwd()}/_results"

#──── Message Routing ──────────────────────────────────
task_default_queue = "default"
# task_queues = (
#     Queue("default", routing_key="task.#"),
#     Queue("query_tasks", routing_key="query.#"),
#     Queue("command_tasks", routing_key="command.#"),
# )
# task_routes = {
#     "base_celery.tasks.echo": {
#         "queue": "query_tasks",
#         "routing_key": "query.echo",
#     },
# }
worker_direct = True
task_create_missing_queues = False
# task_default_exchange = 'tasks'
# task_default_exchange_type = 'topic'
# task_default_routing_key = 'task.default'

#──── Broker Settings ────────────────────
broker_url = "amqp://guest:guest@rabbitmq:5672//"
# broker_connection_retry = False
broker_connection_max_retries = 5
broker_connection_retry_on_startup = True
broker_transport_options = {'max_retries': 5}

#──── Worker ────────────────────────────────
imports = (
    # "base_celery.signals.app", # needs to be imported before import_modules hook
    # "base_celery.signals.logger",
    # "base_celery.signals.client",
    # "base_celery.signals.worker",
    # "base_celery.signals.task",
)
# include = ("base_celery.tasks",)
# worker_concurrency = 4
worker_prefetch_multiplier = 1
# worker_state_db = "worker_state.db"
# worker_enable_remote_control = False
worker_cancel_long_running_tasks_on_connection_loss = True

#──── Events ─────────────────────────────────────────────
worker_send_task_events = True
task_send_sent_event = True
# event_queue_ttl
# event_queue_expires

# Remote Control Commands ────────────────────────────────

# Logging ────────────────────────────────────────────────
# worker_hijack_root_logger = False
# worker_redirect_stdouts=False
# worker_redirect_stdouts_level='INFO'

# Security ────────────────────────────────────────────────

# Custom Component Classes ────────────────────────────────
# worker_pool = 'solo' # [prefork|eventlet|gevent|solo|processes|threads|custom]
# celeryd_hijack_root_logger = False

