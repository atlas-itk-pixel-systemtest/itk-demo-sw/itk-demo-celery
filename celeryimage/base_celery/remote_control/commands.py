from celery.worker.control import control_command, inspect_command


@control_command(
    args=[("n", int)],
    signature="[N=1]",  # <- used for help on the command-line.
)
def increase_prefetch_count(state, n=1):
    state.consumer.qos.increment_eventually(n)
    return {"ok": "prefetch count incremented"}


@inspect_command()
def current_prefetch_count(state):
    return {"prefetch_count": state.consumer.qos.value}
