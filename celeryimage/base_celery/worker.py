#!/usr/bin/env python
#
# Celery worker
#
import sys
from rich.console import Console
from rich.theme import Theme
from base_celery.celery import app
# from base_celery.logging import connect_logging_hooks

console = Console(theme=Theme({"celery": "bold cyan"}))
print = console.print

args = [
    "worker",
    "--loglevel=DEBUG",
]

def main(argv=args):
    print("[celery]──── Celery Worker ────────────────────────────────────────────")
    # app.finalize()
    # print(app.tasks)
    app.start(args)
    print("[celery]──── Celery Worker ────────────────────────────────────────────")


if __name__ == "__main__":
    sys.exit(main(args))

# test
