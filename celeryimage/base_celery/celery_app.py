#!/usr/bin/env python
#
# DeMi Celery app
#
# from rich import inspect
from rich.pretty import pprint

import importlib
import sys
from types import ModuleType

from kombu.entity import Exchange, Queue

from celery import Celery

from rich.console import Console
from rich.theme import Theme

from base_celery.signals.app import import_modules_hook

console = Console(theme=Theme({"celery": "bold bright_green"}))
print = console.print

# #: Exchange for worker direct queues.
# WORKER_DIRECT_EXCHANGE = Exchange("C.dq2")

# #: Format for worker direct queue names.
# WORKER_DIRECT_QUEUE_FORMAT = "{hostname}.dq2"

# def worker_direct(hostname: str | Queue) -> Queue:
#     if isinstance(hostname, Queue):
#         return hostname
#     return Queue(
#         WORKER_DIRECT_QUEUE_FORMAT.format(hostname=hostname),
#         WORKER_DIRECT_EXCHANGE,
#         hostname,
#     )

# def setup_queues(app, hostname):
#     app.amqp.queues.select_add(worker_direct(hostname))

def dict_from_module(app, module):
    context = {}
    for setting in dir(module):
        # if not setting.startswith("_"):
        if setting in list(app.conf.defaults[1].keys()):
            # print(setting)
            # v = getattr(module, setting)
            # if not isinstance(v, ModuleType):
            context[setting] = getattr(module, setting)

    return context


# Set up app
def get_celery_app(
    update_configs=(
        # "local_celeryconfig",
        # "base_celery.configs.filesystem",
    ),
    silent=True,
):
    if not silent:
        print("[celery]──── Celery App ───────────────────────────────────────────────")
    app = Celery(
        # 'baseapp',
        # task_cls="base_celery.basetask:BaseTask",
    )
    # inspect(
    # app,
    # all=True,
    # )

    # app.add_defaults({})
    # try:
    #     os.environ["CELERY_CONFIG_MODULE"]
    #     try:
    #         app.config_from_envvar("CELERY_CONFIG_MODULE", silent=False)
    #         if not silent:
    #             print("[celery]Using $CELERY_CONFIG_MODULE")
    #     except Exception as e:
    #         if not silent:
    #             print(f"[celery]Cannot configure from $CELERY_CONFIG_MODULE\n{e}")
    # except Exception as e:
    # if not silent:
    #     print(f"[celery] CELERY_CONFIG_MODULE not set\n{e.__class__}:{e}")

    # hardcoded internal defaults
    app.config_from_object("base_celery.celeryconfig")

    # trigger imports
    app.conf.table("imports")

    if not silent:
        print(app.conf.table())

    # 3. update from modules in update_config

    for module in update_configs:
        try:
            m = importlib.import_module(module)
            d = dict_from_module(app, m)
            if not silent:
                print(f"[celery]Updating config from '{module}':")
                pprint(d)

            app.conf.update(d)

        except Exception as e:
            # if not silent:
            print(f"[red]Could not update config from '{module}': {e}")
            sys.exit(1)

    if not silent:
        print()
        print("[celery]Final configuration:")
        print(
            app.conf.table(
                # with_defaults=True,
                censored=False,
            )
        )
        # print(app.conf.humanize())

        # print("[celery] finalize:")
        # app.finalize()
        # for t in app.tasks:
        #     print(t)
        #     t.__class__.mro()

        print("[celery]──── Celery App ───────────────────────────────────────────────")

    return app


app = get_celery_app(silent=True)

if __name__ == "__main__":
    app = get_celery_app()
#     app.start()
