import time
from celery.contrib.abortable import AbortableTask
#, shared_task

print('[BaseTask] importing')

class BaseTask(AbortableTask):

    # autoretry_for = (TypeError,)
    # max_retries = 5
    # retry_backoff = True
    # retry_backoff_max = 700
    # retry_jitter = False

    track_started = True

    # from config
    # ('serializer', 'task_serializer'),
    # ('rate_limit', 'task_default_rate_limit'),
    # ('priority', 'task_default_priority'),
    # ('track_started', 'task_track_started'),
    # ('acks_late', 'task_acks_late'),
    # ('acks_on_failure_or_timeout', 'task_acks_on_failure_or_timeout'),
    # ('reject_on_worker_lost', 'task_reject_on_worker_lost'),
    # ('ignore_result', 'task_ignore_result'),
    # ('store_eager_result', 'task_store_eager_result'),
    # ('store_errors_even_if_ignored', 'task_store_errors_even_if_ignored'),

    def __init__(self):
        self.current = 0
        self.total = 100
        # print('[BaseTask] __init__')

    # shared_task(bind=True)
    # def progress(self, current):
    #     if not self.request.called_directly:
    #         self.update_state(state='PROGRESS',
    #             meta={'current': self.current, 'total': self.total})

    def __call__(self, *args, **kwargs):
        print(f'[BaseTask] ({args}, {kwargs}): [{self.request.id}] {self.name} @ {self.request.hostname}')
        return self.run(*args, **kwargs)
    
    # Handlers

    def before_start(self, task_id, args, kwargs):
        """Handler called before the task starts."""
        print(f"[BaseTask] [{task_id}] before_start: ({args},{kwargs})")
        # print(self.state)
        # time.sleep(5)

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        """Handler called after the task returns."""
        print(f"[BaseTask] [{task_id}] after_return: {status} --> {retval} ({einfo}) ({args},{kwargs})")

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """Error handler."""
        print(f"[BaseTask] [{task_id}] failure: Exception {exc} ({einfo}) ({args},{kwargs})")

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        """Retry handler."""
        print(f"[BaseTask] [{task_id}] retry: Exception {exc} ({einfo}) ({args},{kwargs})")

    def on_success(self, retval, task_id, args, kwargs):
        """Success handler."""
        print(f"[BaseTask] [{task_id}] success: --> {retval} ({args},{kwargs})")



