
# from base_celery.celery import app

import time
import random

from celery import shared_task
from celery.contrib.abortable import AbortableTask
from celery.utils.log import get_task_logger

from rich import inspect

logger = get_task_logger(__name__)

# from base_celery.progress.backend import ProgressRecorder

# Signal hooks
# import base_celery.task_signals

# Custom state

PROGRESS='PROGRESS'

# @app.task(bind=True)
@shared_task(bind=True)
def noop(self, *args, **kwargs):
    pass

# @app.task(bind=True)
@shared_task(bind=True)
def fail(self, *args, **kwargs):
    raise Exception("fail")


# @app.task(bind=True)
@shared_task(bind=True, base=AbortableTask)
def abort(self, *args, **kwargs):
    time.sleep(random.random()*5.)
    
    for i in range(10):
        r = self.AsyncResult(self.request.id)
        inspect(r)
        if self.is_aborted():
            logger.warning('Task aborted')
            return
        time.sleep(1)
        logger.info(f"{self.request.id} still going ...")
        # self.update_state(state='PROGRESS', meta={'count': i})
    # print (f"{self.request.id} aborted ...")
    return

# @app.task(bind=True)
@shared_task(bind=True)
def echo(self, *args, **kwargs):
    # if args:
    #     print(f'args: {args}')
    # if kwargs:
    #     print(f'kwargs: {kwargs}')
    # print('[echo]: ')
    # print(vars(self))
    # print(vars(self.request))
    # return vars(self.request)
    return { 'args': args, 'kwargs': kwargs }

# @app.task(bind=True)
@shared_task(bind=True)
def long_task(self, seconds=100):

    # progress_recorder = ProgressRecorder(self)

    result = 0
    for i in range(seconds):
        time.sleep(0.1)
        result += i/10.

        self.update_state(state=PROGRESS, meta={'current': i, 'total': seconds})
        # progress_recorder.set_progress(i + 1, seconds)

    return result
