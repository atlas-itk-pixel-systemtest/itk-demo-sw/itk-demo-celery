#
# Default DeMi Celery Config
#

import os
# import time
# from kombu import Queue
from pydantic import BaseModel
from pydantic_yaml import to_yaml_str, parse_yaml_raw_as
from rich import print
from celery import Celery

print(f'[celery]Importing celeryconfig from: {__file__}')

class CeleryConfigModel(BaseModel):

    #──── General Settings ─────────────────────────────────
    enable_utc: bool = True
    timezone: str = "Europe/Paris"
    # timezone: str = time.tzname

    #──── Task Settings ────────────────────────────────────
    task_annotations: dict = {'*': {}}
    task_annotations: dict = {'tasks.add': {'rate_limit': '10/s'}}

    #──── Task execution settings ──────────────────────────
    task_remote_tracebacks: bool = True # needs tblib
    task_ignore_result: bool = True
    task_store_errors_even_if_ignored: bool = True
    task_track_started: bool = True
    task_acks_late: bool = True
    # task_acks_on_failure_or_timeout

    #──── Task result backend settings ─────────────────────
    result_backend_thread_safe: bool = True
    result_extended: bool = True
    # result_expires
    # override_backends

    #──── RPC backend settings
    # result_backend: str = 'rpc://'
    result_persistent: bool = True

    #──── Redis backend settings 
    result_backend: str = "redis://redis:6379/0"

    #──── File-system backend settings  ────────────────────
    # result_backend: str = f"file://{os.getcwd()}/_results"

    #──── Message Routing ──────────────────────────────────
    task_default_queue: str = "default"
    # task_queues = (
    #     Queue("default", routing_key="task.#"),
    #     Queue("query_tasks", routing_key="query.#"),
    #     Queue("command_tasks", routing_key="command.#"),
    # )
    # task_routes = {
    #     "base_celery.tasks.echo": {
    #         "queue": "query_tasks",
    #         "routing_key": "query.echo",
    #     },
    # }
    worker_direct: bool = True
    task_create_missing_queues: bool = False
    task_default_exchange: str = 'tasks'
    task_default_exchange_type: str = 'topic'
    task_default_routing_key: str = 'task.default'

    #──── Broker Settings ────────────────────
    broker_url: str = "amqp://guest:guest@rabbitmq:5672//"
    broker_connection_retry: bool = False
    broker_connection_max_retries: int = 5
    broker_connection_retry_on_startup: bool = True
    broker_transport_options: dict = {'max_retries': 5}

    #──── Worker ────────────────────────────────
    imports: tuple = (
        # "base_celery.signals.app", # needs to be imported before import_modules hook
        # "base_celery.signals.logger",
        # "base_celery.signals.client",
        # "base_celery.signals.worker",
        # "base_celery.signals.task",
    )
    include: tuple = (
        #"base_celery.tasks",
        )
    worker_concurrency: int = 4
    worker_prefetch_multiplier: int = 1
    worker_state_db: str = "worker_state.db"
    worker_enable_remote_control: bool = False
    worker_cancel_long_running_tasks_on_connection_loss: bool = True

    #──── Events ─────────────────────────────────────────────
    worker_send_task_events: bool = True
    task_send_sent_event: bool = True
    # event_queue_ttl
    # event_queue_expires

    # Remote Control Commands ────────────────────────────────

    # Logging ────────────────────────────────────────────────
    worker_hijack_root_logger: bool = False
    worker_redirect_stdouts: bool = False
    worker_redirect_stdouts_level: str = 'INFO'

    # Security ────────────────────────────────────────────────

    # Custom Component Classes ────────────────────────────────
    # worker_pool = 'solo' # [prefork|eventlet|gevent|solo|processes|threads|custom]
    # celeryd_hijack_root_logger = False

if __name__ == "__main__":

    """Test the YAML round-trip
    """

    cc = CeleryConfigModel()
#    print(cc.model_dump())
    cc_yaml = to_yaml_str(cc)
    print(cc_yaml)
    cc2 = parse_yaml_raw_as(CeleryConfigModel, cc_yaml)
    print(cc2.model_dump())

    app = Celery()
    app.config_from_object(cc2.model_dump())
    app.loader.import_default_modules()
    print(app.bugreport())
