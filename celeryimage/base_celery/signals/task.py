from celery.signals import (
    task_internal_error,
    task_prerun,
    task_postrun,
    task_success,
    task_rejected,
    task_unknown,
    task_retry,
    task_failure,
    task_revoked,
)

from .logger import signal_logger


# - Task (sent by task)
@task_prerun.connect
@signal_logger
def task_prerun(*args, **kwargs):
    """'task_id, task, args, kwargs'"""
    pass


@task_postrun.connect(retry=True)
@signal_logger
def task_postrun(*args, **kwargs):
    """'task_id, task, args, kwargs, retval'

    Runs after a task has finished.
    This will update the result backend to include the IGNORED result state.
    """

    if kwargs.pop("state") == "IGNORED":
        task = kwargs.pop("task")
        task.update_state(state="IGNORED", meta=str(kwargs.pop("retval")))


@task_success.connect
@signal_logger
def task_success(*args, **kwargs):
    """'result'"""
    pass


@task_retry.connect
@signal_logger
def task_retry(*args, **kwargs):
    """'request, reason, einfo'"""
    pass


@task_failure.connect
@signal_logger
def task_failure(*args, **kwargs):
    """
    'task_id, exception, args, kwargs, traceback, einfo(*args, **kwargs):
    """
    pass


@task_internal_error.connect
@signal_logger
def task_internal_error(*args, **kwargs):
    """
    'task_id, args, kwargs, request, exception, traceback, einfo'
    """
    pass


@task_revoked.connect
@signal_logger
def task_revoked(*args, **kwargs):
    """
    'request, terminated, signum, expired(*args, **kwargs):
    """
    pass


@task_rejected.connect
@signal_logger
def task_rejected(*args, **kwargs):
    """'message, exc'"""
    pass


@task_unknown.connect
@signal_logger
def task_unknown(*args, **kwargs):
    """'message, exc, name, id'"""
    pass
