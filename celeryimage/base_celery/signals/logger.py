import logging

from celery import signals
from rich.logging import RichHandler

# FORMAT = "%(funcName)s: %(message)s"
FORMAT = "%(message)s"
logging.basicConfig(
    level="INFO", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)

log = logging.getLogger(__name__)


def signal_logger(func):
    def inner(*args, **kwargs):
        r = func(*args, **kwargs)

        msg = f"<Signal {kwargs.get('signal').name}> from {kwargs.get('sender')} "
        # log.info(f"{args}{kwargs}")
        if 'task_id' in kwargs['signal'].providing_args:
            msg += f"[{kwargs['task_id']}]"

        # log.info(f'{kwargs['signal'].name}')

        log.info(msg)

        return r

    return inner


# GENERAL LOGGING


# Disable celery log hijacking
@signals.setup_logging.connect
@signal_logger
def setup_logging_hook(**kwargs):
    # 'loglevel, logfile, format, colorize(*args, **kwargs):
    # print("[green]setup_logging_hook: -don't touch logging-")
    pass


@signals.after_setup_logger.connect
@signal_logger
def after_setup_logger_hook(*args, **kwargs):
    # 'logger, loglevel, logfile, format, colorize(*args, **kwargs):
    # print("[red]after_setup_logger_hook: we shouldn't be here...")
    pass


@signals.after_setup_task_logger.connect
@signal_logger
def after_setup_task_logger_hook(*args, **kwargs):
    # 'logger, loglevel, logfile, format, colorize(*args, **kwargs):
    # print("[red]after_setup_task_logger_hook: we shouldn't be here...")
    pass


