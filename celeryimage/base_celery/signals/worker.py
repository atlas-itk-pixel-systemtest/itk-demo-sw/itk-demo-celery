from celery.signals import (
    celeryd_after_setup,
    celeryd_init,
    task_received,
    worker_init,
    worker_process_init,
    worker_process_shutdown,
    worker_ready,
    worker_shutdown,
    worker_shutting_down,
)

from .logger import signal_logger


# - Program(*args, **kwargs): `celery worker`
@celeryd_init.connect
@signal_logger
def celeryd_init(*args, **kwargs):
    """'instance, conf, options'"""


@celeryd_after_setup.connect
@signal_logger
def celeryd_after_setup(*args, **kwargs):
    """'instance, conf'"""
    # def setup_direct_queue(sender, instance, **kwargs):
    #     queue_name = '{0}.dq'.format(sender)  # sender is the nodename of the worker
    #     instance.app.amqp.queues.select_add(queue_name)


# - Worker
# @import_modules.connect
# def import_modules(*args, **kwargs):
#     """ """


@worker_init.connect
@signal_logger
def worker_init(*args, **kwargs):
    """ """


@worker_process_init.connect
@signal_logger
def worker_process_init(*args, **kwargs):
    """ """


@worker_process_shutdown.connect
@signal_logger
def worker_process_shutdown(*args, **kwargs):
    """ """


@worker_ready.connect
@signal_logger
def worker_ready(*args, **kwargs):
    """ """


# - Task (sent by consumer)
@task_received.connect
@signal_logger
def task_received(*args, **kwargs):
    """'request'"""
    pass


@worker_shutdown.connect
@signal_logger
def worker_shutdown(*args, **kwargs):
    """ """


@worker_shutting_down.connect
@signal_logger
def worker_shutting_down(*args, **kwargs):
    """ """


# - Programs
# @user_preload_options.connect
# def user_preload_options(*args, **kwargs):
#     """'app, options'"""
