
from celery import signals
from .logger import signal_logger

# - Task (sent by client)
@signals.before_task_publish.connect
@signal_logger
def before_task_publish(*args, **kwargs):
    """
    'body, exchange, routing_key, headers(*args, **kwargs):
    'properties, declare, retry_policy(*args, **kwargs):
    """
    pass

# - Task (sent by client)
@signals.after_task_publish.connect
@signal_logger
def after_task_publish(*args, **kwargs):
    """'body, exchange, routing_key'"""
    pass
