### App Signals

from celery import signals
from .logger import signal_logger

@signals.import_modules.connect
@signal_logger
def import_modules_hook(*args, **kwargs):
    print(f'[celery]{kwargs["sender"]}.import_modules_hook: updating imports')
    # if len(args): print(args)
    # if len(kwargs): print(kwargs)

    app = kwargs["sender"]
    app.conf.update({"include": ("base_celery.tasks.tasks",)})

    print(f"[celery]{app.conf['include']}")

