import json
import time
import os
import signal
import sys
from celery.exceptions import SoftTimeLimitExceeded
from celery.utils.log import get_task_logger
from celery.signals import task_success
from benchmarking.app import app
#from celery import current_task
#from rich.pretty import pprint
from cpu_load_generator import load_single_core, load_all_cores, from_profile
import subprocess

logger = get_task_logger(__name__)

@app.task
def noop(inp):
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t)
    res = inp+1
    return res

@app.task
def noop_e():
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t)
    return 1+1

@app.task(
    # name='run',
    bind=True,
    # serializer='json',
)
def run(self, config, result):
    logger.info(f"starting {self.request.id}")
    logger.info(json.dumps(f"{self.request}"))
    self.update_state(state="CONFIGURE", meta={"config_size": len(json.dumps(config))})
    n = 16
    for i in range(0, n):
        if not self.request.called_directly:
            self.update_state(state="PROGRESS", meta={"stage": i})
        time.sleep(0.25)

    # return {"result_size": len(json.dumps(result))}
    return len(json.dumps(result))


@app.task
def cleanup(result_size):
    print(f"Cleaning up {result_size}")


@app.task
def error_handler(request, exc, traceback):
    print(f"Task {request.id} raised exception: {exc!r}\n{traceback!r}")



@app.task
def add(no, x, y):
    print(os.environ.get("sleep_time"))
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(0.1*t)

    return x + y

@app.task
def minus(x, y):
    print(os.environ.get("sleep_time"))
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t)

    return x - y

@app.task
def nothing(x):
    print(os.environ.get("sleep_time"))
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t*10)

    return x

@app.task
def write_octet(file, fileSizeInBytes):
    os.makedirs(os.path.dirname(file), exist_ok=True)
    print(os.environ.get("sleep_time"))
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t)
    with open(file, 'wb') as fout:
        fout.write(os.urandom(fileSizeInBytes))

    print("file in write octet : " + str(file))

    #return str(file)


@app.task
def write_octet_c(no, file, fileSizeInBytes):
    print("file in write octet : " + str(file))
    #os.makedirs(file, exist_ok=True)
    #print(os.environ.get("sleep_time"))
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t)
    with open(file, 'wb') as fout:
        fout.write(os.urandom(fileSizeInBytes))
        

    #return str(file)

@app.task
def write_datas(ret, file,datas=None):
    #os.makedirs(os.path.dirname(file), exist_ok=True)
    print(os.environ.get("sleep_time"))
    t = float(os.environ.get("sleep_time", 0))
    time.sleep(t)
    with open(file, 'a') as fout:
        if ret :
            fout.write(str(ret)+"\n")
        if datas:
            fout.write(str(datas)+"\n")

    print("file in write octet : " + str(file))

    #return str(file)

@app.task
def load_cpu_100(no,cpu,seconds):
    load_all_cores(duration_s=seconds, target_load=cpu)
    return 1
@app.task
def load_cpu_50(no,cpu,seconds):
    load_all_cores(duration_s=seconds, target_load=cpu)
    time.sleep(seconds)
    return 1

@app.task
def reverse(text):
    return text[::-1]


@app.task
def is_prime(n):
    if n % 2 == 0:
        return False
    else:
        i = 3
        while i < n / 2:
            print(i)
            if n % i == 0:
                return False
            i = i + 1
        return True


@app.task
def any_(*args, **kwargs):
    """Task taking any argument, returning nothing.

    This is useful for testing related to large arguments: big values,
    an insane number of positional arguments, etc.

    :keyword sleep: Optional number of seconds to sleep for before returning.

    """
    wait = kwargs.get('sleep')
    if wait:
        time.sleep(wait)

@app.task
def kill(sig=getattr(signal, 'SIGKILL', None) or signal.SIGTERM):
    """Task sending signal to process currently executing itself.

    :keyword sig: Signal to send as signal number, default is :sig:`KILL`
      on platforms that supports that signal, for other platforms (i.e Windows)
      it will be :sig:`TERM`.

    """
    os.kill(os.getpid(), sig)


@app.task
def sleeping(i, **_):
    """Task sleeping for ``i`` seconds, and returning nothing."""
    time.sleep(i)


@app.task
def sleeping_ignore_limits(i, **_):
    """Task sleeping for ``i`` seconds, while ignoring soft time limits.

    If the task is signalled with
    :exc:`~celery.exceptions.SoftTimeLimitExceeded` the signal is ignored
    and the task will sleep for ``i`` seconds again, which will trigger
    the hard time limit (if enabled).

    """
    try:
        time.sleep(i)
    except SoftTimeLimitExceeded:
        time.sleep(i)



@app.task
def exiting(status=0):
    """Task calling ``sys.exit(status)`` to terminate its own worker
    process."""
    sys.exit(status)


@app.task
def any_returning(*args, **kwargs):
    """The same as :task:`any` except it returns the arguments given
    as a tuple of ``(args, kwargs)``."""
    any_(*args, **kwargs)
    return args, kwargs

@task_success.connect
def on_task_success(sender, result, **kwargs):
    if sender.name == "tasks.add":
        logger.info("okok")
        print(f"Task {sender.name} executed successfully with result: {result}")
        print(f"{result} is prime : " + str(is_prime(result)))
    else:
        print(f"Task {sender.name} executed successfully with result: {result}")

