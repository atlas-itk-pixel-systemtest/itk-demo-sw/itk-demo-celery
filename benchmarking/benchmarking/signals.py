from celery.signals import (
    before_task_publish,
    after_task_publish,
    task_internal_error,
    task_prerun,
    task_postrun,
    task_success,
    task_received,
    task_rejected,
    task_unknown,
    task_retry,
    task_failure,
    task_revoked,
    celeryd_init,
    celeryd_after_setup,
    worker_init,
    worker_process_init,
    worker_process_shutdown,
    worker_ready,
    worker_shutdown,
    worker_shutting_down,
    setup_logging,
    after_setup_logger,
    after_setup_task_logger,
    beat_init,
    beat_embedded_init,
    heartbeat_sent,
    eventlet_pool_started,
    eventlet_pool_preshutdown,
    eventlet_pool_postshutdown,
    eventlet_pool_apply,
)

import logging
#from rich import print

# logging.basicConfig(level=logging.INFO)
# log = logging.getLogger(__name__)

#from rich.logging import RichHandler
#from rich.pretty import pretty_repr

# FORMAT = "%(funcName)s: %(message)s"
FORMAT = "%(message)s"
#logging.basicConfig(
#   level="INFO", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
#)

log = logging.getLogger(__name__)
# log.setLevel="INFO"


def signal_logger(func):
    def inner(*args, **kwargs):
        r = func(*args, **kwargs)

        #log.info(f"<Signal {kwargs.get('signal').name}>")
        # log.info(f"{func.__name__} {args} {kwargs.keys()}")
        # log.info(f"{args} {kwargs.keys()}")
        # for kwarg in kwargs['signal'].providing_args:

        # log.info(f'{kwargs['signal'].name}')

        # msg=""
        # for kwarg in kwargs.keys():
        #     if kwarg not in ['conf','options']:
        #         msg += f'\n{kwarg}: '+pretty_repr(kwargs[kwarg])
        # log.info(msg)

        return r

    return inner


# - Task (sent by client)
@before_task_publish.connect
@signal_logger
def before_task_publish(*args, **kwargs):
    """
    'body, exchange, routing_key, headers(*args, **kwargs):
    'properties, declare, retry_policy(*args, **kwargs):
    """


# - Task (sent by client)
@after_task_publish.connect
@signal_logger
def after_task_publish(*args, **kwargs):
    """'body, exchange, routing_key'"""


# - Task (sent by consumer)
@task_received.connect
@signal_logger
def task_received(*args, **kwargs):
    """'request'"""


# - Task (sent by task)
@task_prerun.connect
@signal_logger
def task_prerun(*args, **kwargs):
    """'task_id, task, args, kwargs'"""


@task_postrun.connect
@signal_logger
def task_postrun(*args, **kwargs):
    """'task_id, task, args, kwargs, retval'"""


@task_success.connect
@signal_logger
def task_success(*args, **kwargs):
    """'result'"""


@task_retry.connect
@signal_logger
def task_retry(*args, **kwargs):
    """'request, reason, einfo'"""


@task_failure.connect
@signal_logger
def task_failure(*args, **kwargs):
    """
    'task_id, exception, args, kwargs, traceback, einfo(*args, **kwargs):
    """


@task_internal_error.connect
@signal_logger
def task_internal_error(*args, **kwargs):
    """
    'task_id, args, kwargs, request, exception, traceback, einfo'
    """


@task_revoked.connect
@signal_logger
def task_revoked(*args, **kwargs):
    """
    'request, terminated, signum, expired(*args, **kwargs):
    """


@task_rejected.connect
@signal_logger
def task_rejected(*args, **kwargs):
    """'message, exc'"""


@task_unknown.connect
@signal_logger
def task_unknown(*args, **kwargs):
    """'message, exc, name, id'"""


# - Program(*args, **kwargs): `celery worker`
@celeryd_init.connect
@signal_logger
def celeryd_init(*args, **kwargs):
    """'instance, conf, options'"""


@celeryd_after_setup.connect
@signal_logger
def celeryd_after_setup(*args, **kwargs):
    """'instance, conf'"""
    # def setup_direct_queue(sender, instance, **kwargs):
    #     queue_name = '{0}.dq'.format(sender)  # sender is the nodename of the worker
    #     instance.app.amqp.queues.select_add(queue_name)


# - Worker
# @import_modules.connect
# def import_modules(*args, **kwargs):
#     """ """


@worker_init.connect
@signal_logger
def worker_init(*args, **kwargs):
    """ """


@worker_process_init.connect
@signal_logger
def worker_process_init(*args, **kwargs):
    """ """


@worker_process_shutdown.connect
@signal_logger
def worker_process_shutdown(*args, **kwargs):
    """ """


@worker_ready.connect
@signal_logger
def worker_ready(*args, **kwargs):
    """ """


@worker_shutdown.connect
@signal_logger
def worker_shutdown(*args, **kwargs):
    """ """


@worker_shutting_down.connect
@signal_logger
def worker_shutting_down(*args, **kwargs):
    """ """


@heartbeat_sent.connect
@signal_logger
def heartbeat_sent(*args, **kwargs):
    """ """


# - Logging
@setup_logging.connect
@signal_logger
def setup_logging(*args, **kwargs):
    """
    'loglevel, logfile, format, colorize(*args, **kwargs):
    """


@after_setup_logger.connect
@signal_logger
def after_setup_logger(*args, **kwargs):
    """
    'logger, loglevel, logfile, format, colorize(*args, **kwargs):
    """


@after_setup_task_logger.connect
@signal_logger
def after_setup_task_logger(*args, **kwargs):
    """
    'logger, loglevel, logfile, format, colorize(*args, **kwargs):
    """


# - Beat
@beat_init.connect
@signal_logger
def beat_init(*args, **kwargs):
    """ """


@beat_embedded_init.connect
@signal_logger
def beat_embedded_init(*args, **kwargs):
    """ """


# - Eventlet
@eventlet_pool_started.connect
@signal_logger
def eventlet_pool_started(*args, **kwargs):
    """ """


@eventlet_pool_preshutdown.connect
@signal_logger
def eventlet_pool_preshutdown(*args, **kwargs):
    """ """


@eventlet_pool_postshutdown.connect
@signal_logger
def eventlet_pool_postshutdown(*args, **kwargs):
    """ """


@eventlet_pool_apply.connect
@signal_logger
def eventlet_pool_apply(*args, **kwargs):
    """'target, args, kwargs'"""


# - Programs
# @user_preload_options.connect
# def user_preload_options(*args, **kwargs):
#     """'app, options'"""
