from celery import Celery

import os

import benchmarking.signals

#os.environ("BACKEND_HOST")

def is_docker():
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )


class MyCelery(Celery):
    """Override automatic naming scheme"""

    def gen_task_name(self, name, module):
        if module.endswith(".tasks"):
            module = module[:-6]
        return super().gen_task_name(name, module)


#app.conf.broker_url = "pyamqp://guest:guest@localhost",
#app.conf.result_backend = "redis://localhost:6379/0"

if is_docker():

     # print ("Running in Docker")

     broker = "pyamqp://guest:guest@rabbitmq"
     backend = "redis://redis:6379/0"
     #backend="rpc://"

else:

     # print ("Running locally")

     broker = "pyamqp://guest:guest@localhost"
     #backend="rpc://"
     backend = "redis://localhost:6379/0"
     # broker="redis://localhost:6379/0",

#broker = "pyamqp://guest:guest@rabbitmq"
#backend = "redis://redis:6379/0"


app = Celery(
    "benchmarking",
    broker=broker,
    backend=backend,
    include=["benchmarking.tasks"],
)


# app.conf.broker_channel_error_retry = True
# app.conf.broker_connection_retry = False
app.conf.broker_connection_retry_on_startup = True

app.conf.task_send_sent_event = True

app.conf.enable_utc=True
app.conf.timezone="Europe/Paris"

app.conf.worker_direct=True


if __name__ == "__main__":
    app.start()
