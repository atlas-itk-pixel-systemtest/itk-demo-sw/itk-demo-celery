from benchmarking.benchmarking.tasks import add, reverse, is_prime
from benchmarking.benchmarking.signals import task_success

import random
import time

results = []
execution = 0
for i in range(100):        

    if random.choice([True, False]):
        start_time = time.time()
        result = add.apply_async((i, 4),queue='worker1_queue')
        end_time = time.time()
        total = end_time - start_time
        execution = execution +total
        print(f"add {i} execution_time = "+str(total))
    else:
       start_time = time.time()
       result = is_prime.apply_async(args=(i,),queue='worker2_queue')
       end_time = time.time()
       print(f"is_prime {i} execution_time = "+str(end_time - start_time))
    results.append(result)


#for i in range(3500):
#    print(results[i].get())

# result.get(propagate=False)
# In case the task raised an exception, get() will re-raise the exception, but you can override this by specifying the propagate argument

# result2 = reverse.apply_async(args=("test",),countdown=3)
# # print(result2.ready())
# print(result2.get())

# result3 = is_prime.delay(5)
# print(str(result.get())+" is prime : " + str(result3.get()))
