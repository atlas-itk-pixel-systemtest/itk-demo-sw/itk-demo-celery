from typing import Annotated, Dict, List, Literal, Tuple
from pydantic import BaseModel


class Settings(BaseModel):
    container_num: int = [36]
    # worker_num: int = [3]
    concurrency_num: int = [1]
    sleep_time: float = [1]
    task_amount: int = 1000
    pool: int = 1  # 0 = solo // 1 = prefork // 2 = threads
    input_output: int = [104857600] #100ko 1Mo 100Mo
    director: bool = False
    specific_queue: bool = True
    cpu: int = 50
