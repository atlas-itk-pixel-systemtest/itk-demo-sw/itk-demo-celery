from compose_script.settings import Settings

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/benchmarking"

def generate_compose_file(container_number, concurrency_num, sleep_time_num):
    settings = Settings()
    compose_content = """
version: "3"
networks:
    default:
        name: deminet
        external: true
services:"""
    services_content = ""
    if settings.director:
        for i in range(1, container_number + 1):
            queue = f" -Q queue{i}" if settings.specific_queue else ""
            services_content += f"""
        container{i}:
            container_name: container{i}
            command: director celery worker --loglevel=INFO {queue}
            build: ..
            environment:
            - sleep_time={sleep_time_num}
            image: #script bash 
            environment:
            - DB_PASSWORD
            - DIRECTOR_HOME
            - DIRECTOR_AUTH_ENABLED
            - DIRECTOR_DATABASE_URI
            - DIRECTOR_BROKER_URI
            - DIRECTOR_RESULT_BACKEND_URI
            volumes:
            - ./workflows:${{DIRECTOR_HOME}} 
    """
        services_content += """

        director-webserver:
            container_name: director-webserver
            command: director webserver -b 0.0.0.0:8000
            image: director
            ports:
            - "8000:8000"
            environment:
            - DB_PASSWORD
            - DIRECTOR_HOME
            - DIRECTOR_AUTH_ENABLED
            - DIRECTOR_DATABASE_URI
            - DIRECTOR_BROKER_URI
            - DIRECTOR_RESULT_BACKEND_URI
            - DIRECTOR_API_URL
            - DIRECTOR_FLOWER_URL
            - DIRECTOR_ENABLE_HISTORY_MODE
            - DIRECTOR_REFRESH_INTERVAL
            - DIRECTOR_REPO_LINK
            - DIRECTOR_DOCUMENTATION_LINK
            - DIRECTOR_VUE_DEBUG=1
            volumes:
            - ./workflows:${{DIRECTOR_HOME}}

        postgres:
            image: "postgres"
            container_name: postgres
            environment:
            - POSTGRES_USER=admin
            - POSTGRES_PASSWORD=${{DB_PASSWORD}}
            - POSTGRES_DB=director
            ports:
            - "5432:5432"
            volumes:
            - database-data:/var/lib/postgresql/data/

        pgadmin:
            image: dpage/pgadmin4
            container_name: pgadmin
            restart: always
            ports:
            - "8008:80"
            environment:
            - PGADMIN_DEFAULT_EMAIL: user-name@domain-name.com
            - PGADMIN_DEFAULT_PASSWORD:${{DB-PASSWORD}}
            volumes:
            - pgadmin-data:/var/lib/pgadmin

        volumes:
        local_pgdata:
        pgadmin-data:
        database-data:
            #external: true

"""
    else:
        for i in range(1, container_number + 1):
            queue = f""",  "-Q", "{i}" """ if settings.specific_queue else ""
            pool = ",--pool=prefork"   if settings.pool == 1 else ",--pool=threads"

            services_content += f"""
    container{i}:
        container_name: container{i}
        hostname: container{i}
        image: benchmarking
        environment:
            - sleep_time={sleep_time_num}
        entrypoint: ["celery", "-A", "benchmarking.tasks", "worker", "--loglevel=info"{pool}, "--concurrency={concurrency_num}" {queue}]
        labels:
            - demi.${{STACK}}.benchmarking.worker.host=${{HOST}}
            - demi.${{STACK}}.benchmarking.worker.description=Worker {i} for celery
            - demi.${{STACK}}.benchmarking.worker.category=Microservice Worker

        """
    compose_content += services_content
    with open(f"{ORIGIN_PATH}/stacks/containers/compose.yaml", "w") as file:
        file.write(compose_content)


if __name__ == "__main__":
    generate_compose_file()
