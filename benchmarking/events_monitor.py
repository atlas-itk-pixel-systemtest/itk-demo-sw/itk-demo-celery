#!/usr/bin/env python

import json

from rich import print

from benchmarking.app import app
from compose_script.settings import Settings
import asyncio

import time

class CeleryEventsHandler:
    def __init__(self, celery_app):
        self._app = celery_app
        self._state = celery_app.events.State()
        self.event_counts = {
            "task-sent": 0,
            "task-received": 0,
            "task-started": 0,
            "task-succeeded": 0,
            "task-failed": 0
        }
        self.event_timers = {event_type: None for event_type in self.event_counts}

    def _event_handler(self, event):
        self._state.event(event)
        event_type = event["type"]
        settings = Settings()
        num = settings.task_amount
        if event_type in self.event_counts:
            self.event_counts[event_type] += 1
            if self.event_counts[event_type] == 1:
                self.event_timers[event_type] = time.time() 
            elif self.event_counts[event_type] == num:
                elapsed_time = time.time() - self.event_timers[event_type]
                self.handle_threshold_reached(event_type, elapsed_time)
        

    def handle_threshold_reached(self, event_type, elapsed_time):
        print(f"Threshold reached for {event_type}. Count: {self.event_counts[event_type]}. Elapsed time: {elapsed_time} seconds.")


    async def start_listening(self, recv):
  
        recv.capture(limit=None, timeout=3600)
  
  
async def main():      
    
    events_handler = CeleryEventsHandler(app)
    with events_handler._app.connection() as connection:
        handlers = {
            "task-sent": events_handler._event_handler,
            "task-received": events_handler._event_handler,
            "task-started": events_handler._event_handler,
            "task-succeeded": events_handler._event_handler,
            "task-failed": events_handler._event_handler
        }    
        recv = events_handler._app.events.Receiver(
        connection, handlers=handlers
        ) 

    await events_handler.start_listening(recv)
    
    while events_handler.event_counts["task-succeeded"] < Settings().task_amount :
        pass
    
    recv.should_stop()

if __name__ == "__main__":
    asyncio.run(main())