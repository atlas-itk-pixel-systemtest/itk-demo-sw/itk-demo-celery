from __future__ import absolute_import, unicode_literals
import subprocess
import os
import psutil
import matplotlib
matplotlib.use('TkAgg',force=True)
import matplotlib.pyplot as plt
from time import sleep,time
from celery import group,chain, signature, chord
import itertools
from celery.result import AsyncResult
from compose_script.settings import Settings
from benchmarking.tasks import (
    add,noop_e,noop, minus
)
from compose_script.script import generate_compose_file
import events_monitor

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/benchmarking"


class StressTestSuite:
    def __init__(self,container_number, concurrency_num, sleep_time_num,tasks_num,output_dir,pool):
        pass
        #output_file_path = os.path.join(output_dir,f"output_{container_number}_{concurrency_num}_{sleep_time_num}_{tasks_num}.txt")
        #self.output_file = open(output_file_path, "w")
        #self.events_monitor = 

    def write_output(self, text):
        pass
        #self.output_file.write(text + "\n")

    def down(self):
        pass
        #self.output_file.close()
        #subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)



    def up(self,container_number, concurrency_num, sleep_time_num):
        start_time_building = time()
        #subprocess.run(["docker", "compose", "down"],cwd="stacks/containers")#, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #generate_compose_file(container_number, concurrency_num, sleep_time_num)
        #subprocess.run(["docker", "compose", "up","-d"],cwd=f"{ORIGIN_PATH}/stacks/containers2", stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        pass




    def manyshort(self, num_task, num_queue, concurrency): 
        #self.write_output(f"Numbers of tasks: {num_task}")
  
        start_time_creating = time()   
        print("I'm here 6")
        task_chains = []
        '''for i in range(num_task//10):  
            queue = next(queue_cycle)
            signature_task_1 = signature('noop', args=("message qui sera traité par task_1 puis par task_2",))
            tasks = signature_task_1
            for j in range(i*10):
                tmp = signature('noop')
                tasks = (tasks | tmp)
            task_chain = tasks.apply_async(queue=queue) 
            task_groups.append(task_chain)
            task_lists.append(tasks)'''

        queue = "20"


        #for i in range(num_task//10):#num_queue[0]+1):
            #for j in range(concurrency[0]+1):

        #task_chain = chain(add.s(4, 4), noop.s(), noop.s(),noop.s(),noop.s(),noop.s(),noop.s()).apply_async(queue=queue)
        #task_chain = noop.apply_async((1,),link=noop.s(), queue=queue)


        '''mychain = chain(add.s(4, 4).set(queue=queue)
        subtask1 = noop.s().set(queue=queue)
        subtask2 = noop.s().set(queue=queue)
        subtask3 = noop.s().set(queue=queue
        subtask4 = noop.s().set(queue=queue))'''
        #mychain.apply_async()

            #task_chains.append(task_chain)

        # Define your tasks
        task1 = add.s(1, 2)
        task2 = minus.s(5)
        task3 = add.s(1)

        task1.set(queue=queue)
        task2.set(queue="11")
        task3.set(queue=queue)


        # Create a chain
        my_chain = chain(task1, task2, task3)

        # Set the queue for all tasks in the chain
       # my_chain.set(queue=queue)

        # Apply the chain
        result = my_chain.apply_async()


        end_time_creating = time()  
        total_time_creating = end_time_creating - start_time_creating
        print(f"I am here 7 - tasks created - {total_time_creating}")



        end_time_preparing = time()
        total_time_preparing = end_time_preparing - end_time_creating


        print(f"I am here 8 - tasks ready - {total_time_preparing}")
        print(result.get())
        #print("parent result : " + str(mychain.collect()))
        #print(("result : " + str(mychain.get())))
       
        end_time_executing = time()
        total_time = end_time_executing - start_time_creating
        total_time_executing = end_time_executing - end_time_preparing
        print(f"I am here 10 - results - {round(total_time,1)}")

        # Calculate average CPU usage
        #cpu_usages = [psutil.cpu_percent(interval=interval) for _ in range(int(total_time_executing // interval))]
        #avg_cpu = sum(cpu_usages) / len(cpu_usages)
        #self.write_output(f"Average CPU Usage : {avg_cpu}%")

        #end_ram_usage = psutil.virtual_memory().used

        #self.write_output(f"Peak RAM Usage : {(max(start_ram_usage, end_ram_usage) - start_ram_usage) / 1000000000} Go")

        return [total_time_creating, total_time_executing, 0,total_time]

def count_files(directory):

    files = os.listdir(directory)
    num_files = len(files)
    return num_files

def initialisation(dir):
    os.makedirs(dir, exist_ok=True)
    directory = f"{dir}_files_{count_files(dir)+1}"
    directory_path = os.path.join(dir, directory)
    os.makedirs(directory_path, exist_ok=True)
    return directory_path


import numpy as np

if __name__ == "__main__":

    #output_directory_path = initialisation("outputs")
    #plot_directory_path = initialisation("plots")
    settings = Settings()
    pool = "prefork" #if settings.pool else "solo"


    for i in range(len(settings.container_num)):
        print(i)
        print("BEGIN")
        stresstest = StressTestSuite(settings.container_num[i], settings.concurrency_num[i], 0, settings.task_amount, None,pool)
        starting_time = stresstest.up(settings.container_num[i], settings.concurrency_num[i], 0)
        #start = time()
        results = stresstest.manyshort(int(settings.task_amount), settings.container_num, settings.concurrency_num)
        #end = time()
        creating_time = results[0]
        executing_time = results[1]
        peak_cpu = results[2]
        total_time = results[3]
        #stresstest.down()
        print("END")

