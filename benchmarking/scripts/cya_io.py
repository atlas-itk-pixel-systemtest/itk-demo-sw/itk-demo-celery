from __future__ import absolute_import, unicode_literals
import subprocess
import os
import psutil
import threading
import matplotlib
matplotlib.use('TkAgg',force=True)
import matplotlib.pyplot as plt
from time import sleep,time
from celery import group
from celery.result import AsyncResult
from compose_script.settings import Settings
from benchmarking.tasks import (
    write_octet,
)
from compose_script.script import generate_compose_file
import itertools
stop_measure_cpu = False  
cpu_usage_data = []

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/benchmarking"
WRITING_PATH = "/home/jyaker/itk-demo-celery/benchmarking/cya/"

class StressTestSuite:

    def __init__(self,container_number, concurrency_num, sleep_time_num,output_dir,pool,task_amount):
        print("\n----------------------------------------------\n")
        print("container : "+str(container_number))
        print("concurrency : "+str(concurrency_num))
        print("pool : "+pool)
        print("octets : "+str(octets))
        self.queues=container_number
        self.outputs_file = []
        for i in range(task_amount):
            output_file_path = os.path.join(output_dir,f"output_{container_number}_{concurrency_num}_{sleep_time_num}_{pool}_{i}.txt")
            open(output_file_path, "w")  
            self.outputs_file.append(output_file_path)

    def write_output(self, text):
        self.output_file.write(text + "\n")

    def down(self):
        self.output_file.close()
        subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    def up(self,container_number, concurrency_num, sleep_time_num):
        generate_compose_file(container_number, concurrency_num, 0)
        subprocess.run(["docker", "compose", "up","-d"],cwd=f"{ORIGIN_PATH}/stacks/containers", stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        return 1


    def manyshort(self,bytes_num): 
        queues = []
        for i in range(1, self.queues+1):
            queues.append(str(i))
        queue_cycle = itertools.cycle(queues)
        print("I'm here 6 - before creating")
        start_time = time()
        task_group = []
        for file in self.outputs_file:
            path = ""#WRITING_PATH
            #print(path + file)
            #print(os.path.dirname(path + file))
            task_group.append(write_octet.apply_async((path + file,bytes_num),queue=next(queue_cycle)))
        apply_time = time()
        middle_time = apply_time - start_time
        print("I'm here 7 : apply_time : "+str(middle_time))
        for g in task_group:
            g.get()

        end_time = time()
        total_time = end_time - start_time
        print("I am here 9 : total_time = "+str(round(total_time,1)))

        return [0,0,0]

def count_files(directory):
    files = os.listdir(directory)
    num_files = len(files)
    return num_files

def initialisation(dir):
    os.makedirs(dir, exist_ok=True)
    directory = f"{dir}_files_{count_files(dir)+1}"
    directory_path = os.path.join(dir, directory)
    os.makedirs(directory_path, exist_ok=True)
    return directory_path


def measure_cpu_usage():
    global cpu_usage_data
    while not stop_measure_cpu:
        cpu_percent = psutil.cpu_percent(interval=0.1)
        cpu_usage_data.append(cpu_percent)
        sleep(0.1)


if __name__ == "__main__":
    output_directory_path = initialisation("outputs_io")
    #plot_directory_path = initialisation("plots_io")
    settings = Settings()
    pool = "prefork" if settings.pool == 1 else "threads"

    #color_palette = plt.cm.viridis(np.linspace(0, 1, len(settings.concurrency_num)))
    for container_number in settings.container_num:
        for concurrency_num  in settings.concurrency_num:
            for octets in settings.input_output:
                stresstest = StressTestSuite(container_number, concurrency_num, octets, output_directory_path, pool,settings.task_amount)
                stresstest.up(container_number, concurrency_num, octets)
                execution_time = stresstest.manyshort(octets)
