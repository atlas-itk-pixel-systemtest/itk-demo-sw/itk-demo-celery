from __future__ import absolute_import, unicode_literals
import subprocess
import os
import psutil
import matplotlib
matplotlib.use('TkAgg',force=True)
import matplotlib.pyplot as plt
from time import sleep,time
from celery import group,chain, signature, chord
import itertools
from celery.result import AsyncResult
from compose_script.settings import Settings
from benchmarking.tasks import (
    add,minus,nothing,write_datas
)
from compose_script.script import generate_compose_file
import events_monitor

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/benchmarking"


class StressTestSuite:
    def __init__(self,container_number, concurrency_num, sleep_time_num,tasks_num,pool):
        #output_file_path = os.path.join(output_dir,f"output_{container_number}_{concurrency_num}_{sleep_time_num}_{tasks_num}.txt")
        #self.output_file = open(output_file_path, "w")
        self.queues=container_number
        self.task_amount = tasks_num

    def write_output(self, text):
        self.output_file.write(text + "\n")

    def down(self):
        self.output_file.close()
        subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)



    def up(self,container_number, concurrency_num, sleep_time_num):
        start_time_building = time()
        #subprocess.run(["docker", "compose", "down"],cwd="stacks/containers")#, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        generate_compose_file(container_number, concurrency_num, sleep_time_num)
        subprocess.run(["docker", "compose", "up","-d"],cwd=f"{ORIGIN_PATH}/stacks/containers", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        end_time_building = time()
        print("I'm here 3")
        total_time_building = end_time_building - start_time_building
        
        return total_time_building




    def manyshort(self, num_task, num_queue, concurrency): 
        queues = []
        for i in range(1, self.queues+1):
            queues.append(str(i))
        queue_cycle = itertools.cycle(queues)
        print("I'm here 6 - before creating ")
        start_time = time()

        task_chains = []
        for i in range(self.task_amount//10):
            output_file_path = f"output_{container_number}_{concurrency_num}_{i}.txt"
            #print("loop")
            #print(output_file_path)
            queue = next(queue_cycle)
            param1 = int(queue)
            param2 = int(queue) + 1
            datas1 ="adding parameters are : "+queue+" and "+str(param2)
            task1 = write_datas.s(output_file_path,datas1)
            task2 = add.s(param1,param2)
            task3 = write_datas.s(output_file_path)

            param3 = param1+1
            param4 = param2+1
            datas2 ="adding parameters are : "+str(param3)+" and "+str(param4)
            task4 = write_datas.s(file=output_file_path,datas=datas2)
            task5 = add.s(param3,param4)
            task6 = write_datas.s(output_file_path)

            param5 = param3+1
            param6 = param4+1
            datas3 ="adding parameters are : "+str(param5)+" and "+str(param6)+", the result minus 2"
            task7 = write_datas.s(output_file_path,datas3)
            task8 = add.s(param5,param6)
            task9 = minus.s(2)
            task10 = write_datas.s(output_file_path)

            task1.set(queue=queue)
            task2.set(queue=queue) 
            task3.set(queue=queue) 
            task4.set(queue=queue)
            task5.set(queue=queue) 
            task6.set(queue=queue)
            task7.set(queue=queue)
            task8.set(queue=queue) 
            task9.set(queue=queue)
            task10.set(queue=queue)

            my_chain = chain(task1, task2, task3,task4,task5,task6,task7,task8,task9,task10)
            task_chains.append(my_chain)

        
        apply_time = time()
        middle_time = apply_time - start_time
        print("I'm here 7 : creating_time = "+str(middle_time))
        readys_chains = []

        for chain_obj in task_chains:
            readys_chains.append(chain_obj.apply_async())


        end_time_preparing = time()
        total_time_preparing = end_time_preparing - apply_time


        print(f"I am here 8 - preparing_time = {total_time_preparing}")


        for chain_obj in readys_chains:
            chain_obj.get()
       
        end_time = time()
        executing_time = end_time - end_time_preparing
        print("I am here 9 : executing_time = "+str(executing_time))
        total_time = end_time - start_time
        print("I am here 10 : total_time = "+str(round(total_time,1)))

        return [0,0,0]
    
def count_files(directory):

    files = os.listdir(directory)
    num_files = len(files)
    return num_files

def initialisation(dir):
    os.makedirs(dir, exist_ok=True)
    directory = f"{dir}_files_{count_files(dir)+1}"
    directory_path = os.path.join(dir, directory)
    os.makedirs(directory_path, exist_ok=True)
    return directory_path


if __name__ == "__main__":

    #output_directory_path = initialisation("outputs")
    #plot_directory_path = initialisation("plots")
    settings = Settings()
    pool = "prefork" if settings.pool == 1 else "threads"

    for container_number in settings.container_num:
        for concurrency_num  in settings.concurrency_num:
            for sleeping_time in settings.sleep_time:
                print("BEGIN")
                print("container : "+str(container_number))
                print("concurrency : "+str(concurrency_num))
                print("sleeping_time : "+str(sleeping_time))
                stresstest = StressTestSuite(container_number, concurrency_num, sleeping_time, settings.task_amount,pool)
                starting_time = stresstest.up(container_number, concurrency_num, sleeping_time)
                results = stresstest.manyshort(int(settings.task_amount), container_number, concurrency_num)
                print("END")


