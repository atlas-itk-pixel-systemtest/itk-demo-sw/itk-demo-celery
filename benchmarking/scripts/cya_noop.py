from __future__ import absolute_import, unicode_literals
import subprocess
import os
import psutil
import matplotlib
matplotlib.use('TkAgg',force=True)
import matplotlib.pyplot as plt
from time import sleep,time
from celery import group,chain, signature, chord
import itertools
from celery.result import AsyncResult
from compose_script.settings import Settings
from benchmarking.tasks import (
    add,noop_e
)
import random
from compose_script.script import generate_compose_file
import events_monitor_threading

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/benchmarking"


class StressTestSuite:
    def __init__(self,container_number, concurrency_num, sleep_time_num,tasks_num,output_dir,pool):
        output_file_path = os.path.join(output_dir,f"output_{container_number}_{concurrency_num}_{sleep_time_num}_{tasks_num}.txt")
        self.output_file = open(output_file_path, "w")

    def write_output(self, text):
        self.output_file.write(text + "\n")

    def down(self):
        self.output_file.close()
        subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)



    def up(self,container_number, concurrency_num, sleep_time_num):
        start_time_building = time()
        generate_compose_file(container_number, concurrency_num, sleep_time_num)
        subprocess.run(["docker", "compose", "up","-d"],cwd=f"{ORIGIN_PATH}/stacks/containers", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        end_time_building = time()
        print("I'm here 3")
        total_time_building = end_time_building - start_time_building
        self.write_output(f"Numbers of container : {container_number}")
        self.write_output(f"Numbers of concurrecy : {concurrency_num}")
        self.write_output(f"Time of task sleeping : {sleep_time_num}")
        self.write_output(f"Time starting : {total_time_building}")
        self.write_output(f"Pool : {pool}")
        return total_time_building


    def manyshort(self, num_task, num_queue, concurrency): 
        self.write_output(f"Numbers of tasks: {num_task}")
        print("I'm here 6")
        print(num_queue)
        task_group = []
        queues = []
        for i in range(1, num_queue[0]+1):
            queues.append(str(i))
        queue_cycle = itertools.cycle(queues)
        start_time_creating = time()   

        for i in range(num_task):
            task_group.append(noop_e.apply_async(queue=next(queue_cycle)))

        end_time_creating = time()
        total_time_creating = end_time_creating - start_time_creating
        print(f"I am here 7 - tasks created - {total_time_creating}")

        end_time_preparing = time()
        total_time_preparing = end_time_preparing - end_time_creating

        print(f"I am here 8 - tasks ready - {total_time_preparing}")

        for g in task_group:
            (g.get())
       
        end_time_executing = time()
        total_time = end_time_executing - start_time_creating
        total_time_executing = end_time_executing - end_time_preparing
        print(f"I am here 9 - results - {total_time_executing}")

        self.write_output(f"Total time: {round(total_time,1)}")

        return [total_time_creating, total_time_executing, 0,total_time]

def count_files(directory):

    files = os.listdir(directory)
    num_files = len(files)
    return num_files

def initialisation(dir):
    os.makedirs(dir, exist_ok=True)
    directory = f"{dir}_files_{count_files(dir)+1}"
    directory_path = os.path.join(dir, directory)
    os.makedirs(directory_path, exist_ok=True)
    return directory_path


import numpy as np

if __name__ == "__main__":
    output_directory_path = initialisation("outputs")
    plot_directory_path = initialisation("plots")
    settings = Settings()
    pool = "prefork" #if settings.pool else "solo"

    existing_container_number = []
    existing_executing_time = []                
    existing_starting_time = []
    existing_creating_time = []
    existing_total_time = []


    for i in range(len(settings.container_num)):
        print("i : "+str(i))
        for j in range (len(settings.concurrency_num)):
            print("j : "+str(j))

            for k in range (len(settings.sleep_time)):
                print("k : "+str(k))

                print("BEGIN")
                stresstest = StressTestSuite(settings.container_num[i], settings.concurrency_num[j], settings.sleep_time[k], settings.task_amount, output_directory_path,pool)
                starting_time = stresstest.up(settings.container_num[i], settings.concurrency_num[j], settings.sleep_time[k])
                #start = time()
                results = stresstest.manyshort(int(settings.task_amount), settings.container_num, settings.concurrency_num)
                #end = time()
                creating_time = results[0]
                executing_time = results[1]
                peak_cpu = results[2]
                total_time = results[3]
                existing_container_number.append(settings.container_num[i])
                existing_total_time.append(total_time)
                stresstest.down()
                print("END")




#import docker

#client = docker.from_env()

#def get_maxrss(container_id):
#    stats = client.containers.get(container_id).stats(stream=False)
#    maxrss = stats['memory_stats']['max_usage'] / (1024 * 1024)  # Mo
#    return maxrss

