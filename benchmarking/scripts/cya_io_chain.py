from __future__ import absolute_import, unicode_literals
import subprocess
import os
import psutil
import threading
import matplotlib
matplotlib.use('TkAgg',force=True)
import matplotlib.pyplot as plt
from time import sleep,time
from celery import group,chain
from celery.result import AsyncResult
from compose_script.settings import Settings
from benchmarking.tasks import (
    write_octet_c,
)
from compose_script.script import generate_compose_file
import itertools
stop_measure_cpu = False  
cpu_usage_data = []

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/benchmarking"
WRITING_PATH = "/home/jyaker/itk-demo-celery/benchmarking/cya/"

class StressTestSuite:

    def __init__(self,container_number, concurrency_num, sleep_time_num,pool,task_amount):
        print("\n----------------------------------------------\n")
        print("container : "+str(container_number))
        print("concurrency : "+str(concurrency_num))
        print("pool : "+pool)
        print("octets : "+str(octets))
        self.queues=container_number
        self.task_amount = task_amount

    def write_output(self, text):
        self.output_file.write(text + "\n")

    def down(self):
        self.output_file.close()
        subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    def up(self,container_number, concurrency_num, sleep_time_num):
        generate_compose_file(container_number, concurrency_num, 0)
        subprocess.run(["docker", "compose", "up","-d"],cwd=f"{ORIGIN_PATH}/stacks/containers", stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        return 1


    def manyshort(self,bytes_num): 
        queues = []
        for i in range(1, self.queues+1):
            queues.append(str(i))
        queue_cycle = itertools.cycle(queues)
        print("I'm here 6 - before creating ")
        start_time = time()
        task_chains = []
        for i in range(self.task_amount//10):
            output_file_path = f"output_{container_number}_{concurrency_num}_{bytes_num}_{pool}_{i}"
            #print("loop")
            #print(output_file_path)
            queue = next(queue_cycle)
            task1 = write_octet_c.s(None,output_file_path+"_1.txt",bytes_num)
            task2 = write_octet_c.s(output_file_path+"_2.txt",bytes_num)
            task3 = write_octet_c.s(output_file_path+"_3.txt",bytes_num)
            task4 = write_octet_c.s(output_file_path+"_4.txt",bytes_num)
            task5 = write_octet_c.s(output_file_path+"_5.txt",bytes_num)
            task6 = write_octet_c.s(output_file_path+"_6.txt",bytes_num)
            task7 = write_octet_c.s(output_file_path+"_7.txt",bytes_num)
            task8 = write_octet_c.s(output_file_path+"_8.txt",bytes_num)
            task9 = write_octet_c.s(output_file_path+"_9.txt",bytes_num)
            task10 = write_octet_c.s(output_file_path+"_10.txt",bytes_num)
            
            task1.set(queue=queue)
            task2.set(queue=queue) 
            task3.set(queue=queue) 
            task4.set(queue=queue) 
            task5.set(queue=queue) 
            task6.set(queue=queue) 
            task7.set(queue=queue) 
            task8.set(queue=queue) 
            task9.set(queue=queue) 
            task10.set(queue=queue) 

            my_chain = chain(task1, task2, task3, task4, task5, task6, task7, task8, task9, task10)
            task_chains.append(my_chain)

        
        apply_time = time()
        middle_time = apply_time - start_time
        print("I'm here 7 : creating_time = "+str(middle_time))
        readys_chains = []

        for chain_obj in task_chains:
            readys_chains.append(chain_obj.apply_async())


        end_time_preparing = time()
        total_time_preparing = end_time_preparing - apply_time


        print(f"I am here 8 - preparing_time = {total_time_preparing}")


        for chain_obj in readys_chains:
            chain_obj.get()
            #sleep(0.01)
       
        end_time = time()
        executing_time = end_time - end_time_preparing
        print("I am here 9 : executing_time = "+str(executing_time))
        total_time = end_time - start_time
        print("I am here 10 : total_time = "+str(round(total_time,1)))

        return [0,0,0]

def count_files(directory):
    files = os.listdir(directory)
    num_files = len(files)
    return num_files

def initialisation(dir):
    #os.makedirs(dir, exist_ok=True)
    directory = f"{dir}_files_{count_files(dir)+1}"
    directory_path = os.path.join(dir, directory)
    #os.makedirs(directory_path, exist_ok=True)
    return directory_path


def measure_cpu_usage():
    global cpu_usage_data
    while not stop_measure_cpu:
        cpu_percent = psutil.cpu_percent(interval=0.1)
        cpu_usage_data.append(cpu_percent)
        sleep(0.1)


if __name__ == "__main__":
    #output_directory_path = initialisation("outputs_io")
    #plot_directory_path = initialisation("plots_io")
    settings = Settings()
    pool = "prefork" if settings.pool == 1 else "threads"

    #color_palette = plt.cm.viridis(np.linspace(0, 1, len(settings.concurrency_num)))
    for container_number in settings.container_num:
        for concurrency_num  in settings.concurrency_num:
            for octets in settings.input_output:
                stresstest = StressTestSuite(container_number, concurrency_num, octets, pool,settings.task_amount)
                stresstest.up(container_number, concurrency_num, octets)
                execution_time = stresstest.manyshort(octets)
