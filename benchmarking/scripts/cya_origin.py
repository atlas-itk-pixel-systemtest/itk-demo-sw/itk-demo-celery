from __future__ import absolute_import, unicode_literals
import subprocess
import random
import os
import psutil
from time import sleep,time
from celery import group
from compose_script.settings import Settings
from benchmarking.tasks import (
    add, any_, kill, sleeping, exiting,any_returning,
    sleeping_ignore_limits,
)
from compose_script.script import generate_compose_file

class StressTestSuite:
    def __init__(self,container_number, concurrency_num, sleep_time_num,tasks_num,output_dir):
        output_file_path = os.path.join(output_dir,f"output_{container_number}_{concurrency_num}_{sleep_time_num}_{tasks_num}.txt")
        self.output_file = open(output_file_path, "w")  


    def write_output(self, text):
        self.output_file.write(text + "\n")

    def down(self):
        self.output_file.close()
        subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)



    def up(self,container_number, concurrency_num, sleep_time_num):
        start_time_building = time()
        subprocess.run(["docker", "compose", "-f", "../stacks/rabbitmq/compose.yaml", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        subprocess.run(["docker", "compose", "-f", "../stacks/redis/compose.yaml", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)#
        #subprocess.run(["docker", "compose", "-f", "../stacks/flower/compose.yaml", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        subprocess.run(["docker", "compose", "-f", "../stacks/rabbitmq/compose.yaml", "up","-d"]) #, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        subprocess.run(["docker", "compose", "-f", "../stacks/redis/compose.yaml", "up","-d"]) #, stdout=subprocess.PIPE, stderr=subprocess.PIPE)  

        sleep(2)

        #subprocess.run(["docker", "compose", "-f", "../stacks/flower/compose.yaml", "up","-d"]) #, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
        subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        generate_compose_file(container_number, concurrency_num, sleep_time_num)
        subprocess.run(["docker", "compose", "up","-d"]) #, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        end_time_building = time()
        total_time_building = end_time_building - start_time_building
        self.write_output(f"Numbers of container : {container_number}")
        self.write_output(f"Numbers of concurrecy : {concurrency_num}")
        self.write_output(f"Time of task sleeping : {sleep_time_num}")
        self.write_output(f"Time starting : {total_time_building}")

        sleep(2)


    def manyshort(self,num_task): 
        self.write_output(f"Numbers of tasks: {num_task}")
        start_cpu_usage = psutil.cpu_percent()
        start_ram_usage = psutil.virtual_memory().used   
        start_time_creating = time()   
        #results = []     
        task_group = group(add.s(i, i) for i in range(num_task))()
        end_time_creating = time()
        total_time_creating = end_time_creating - start_time_creating

        for result in task_group:
            result.get()
            #pass
        
        end_time_executing = time()
        total_time_executing = end_time_executing - end_time_creating
        self.write_output(f"Time creating: {total_time_creating}")
        self.write_output(f"Total time executing: {total_time_executing}")

        end_cpu_usage = psutil.cpu_percent()
        end_ram_usage = psutil.virtual_memory().used

        self.write_output(f"Peak CPU Usage : {max(start_cpu_usage, end_cpu_usage)}%")
        self.write_output(f"Peak RAM Usage : {(max(start_ram_usage, end_ram_usage) - start_ram_usage)/1000000000} Go")

    # def always_timeout(self):
    #    task_group = self.join(group(sleep.s(1).set(time_limit=0.1) for _ in range(100))(), timeout=10, propagate=False,)

def count_files(directory):
    files = os.listdir(directory)
    num_files = len(files)
    return num_files

def initialisation():
    os.makedirs("outputs", exist_ok=True)
    output_directory = f"output_files_{count_files('outputs')+1}"
    output_directory_path = os.path.join("outputs", output_directory)
    os.makedirs(output_directory_path, exist_ok=True)
    return output_directory_path


if __name__ == "__main__":
    output_directory_path = initialisation()
    settings = Settings()
    for container_number in settings.container_num:
        for concurrency_num in settings.concurrency_num:
            for sleep_time_num in settings.sleep_time:
                for tasks_num in settings.task_amount:
                    stresstest = StressTestSuite(container_number, concurrency_num, sleep_time_num,tasks_num,output_directory_path)
                    stresstest.up(container_number, concurrency_num, sleep_time_num)
                    stresstest.manyshort(tasks_num)
                    stresstest.down()   


    #stress_test = StressTestSuite(30, 5, 0.2)
    #stress_test.manyshort()
    #stress_test.always_timeout()
