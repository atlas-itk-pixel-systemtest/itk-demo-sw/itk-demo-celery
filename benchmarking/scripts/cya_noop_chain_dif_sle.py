from __future__ import absolute_import, unicode_literals
import subprocess
import os
import psutil
import matplotlib
matplotlib.use('TkAgg',force=True)
import matplotlib.pyplot as plt
from time import sleep,time
from celery import group,chain, signature, chord
import itertools
from celery.result import AsyncResult
from compose_script.settings import Settings
from benchmarking.tasks import (
    add,noop_e,noop,minus, nothing
)
from compose_script.script import generate_compose_file
import events_monitor

ORIGIN_PATH = "/home/jyaker/itk-demo-celery/benchmarking"


class StressTestSuite:
    def __init__(self,container_number, concurrency_num, sleep_time_num,tasks_num,output_dir,pool):
        output_file_path = os.path.join(output_dir,f"output_{container_number}_{concurrency_num}_{sleep_time_num}_{tasks_num}.txt")
        self.output_file = open(output_file_path, "w")
        #self.events_monitor = 

    def write_output(self, text):
        self.output_file.write(text + "\n")

    def down(self):
        self.output_file.close()
        subprocess.run(["docker", "compose", "down"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)



    def up(self,container_number, concurrency_num, sleep_time_num):
        start_time_building = time()
        #subprocess.run(["docker", "compose", "down"],cwd="stacks/containers")#, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        generate_compose_file(container_number, concurrency_num, sleep_time_num)
        subprocess.run(["docker", "compose", "up","-d"],cwd=f"{ORIGIN_PATH}/stacks/containers", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        end_time_building = time()
        print("I'm here 3")
        total_time_building = end_time_building - start_time_building
        self.write_output(f"Numbers of container : {container_number}")
        self.write_output(f"Numbers of concurrecy : {concurrency_num}")
        self.write_output(f"Time of chain sleeping : {sleep_time_num*23.5}")
        self.write_output(f"Time starting : {total_time_building}")
        self.write_output(f"Pool : {pool}")
        return total_time_building




    def manyshort(self, num_task, num_queue, concurrency): 
        self.write_output(f"Numbers of tasks: {num_task}")
        start_cpu_usage = psutil.cpu_percent()
        start_ram_usage = psutil.virtual_memory().used   
        start_time_creating = time()   
        print("I'm here 6")
        print(num_queue)
        task_chains = []
        '''for i in range(num_task//10):  
            queue = next(queue_cycle)
            signature_task_1 = signature('noop', args=("message qui sera traité par task_1 puis par task_2",))
            tasks = signature_task_1
            for j in range(i*10):
                tmp = signature('noop')
                tasks = (tasks | tmp)
            task_chain = tasks.apply_async(queue=queue) 
            task_groups.append(task_chain)
            task_lists.append(tasks)'''

        queues = []
        for i in range(1, num_queue[0]+1):
            queues.append(str(i))
        queue_cycle = itertools.cycle(queues)

        for i in range(num_task//10):#num_queue[0]+1):
            queue = next(queue_cycle)
            #for j in range(concurrency[0]+1):
            print(i)
            # Define your tasks
            task1 = add.s(None,1, 2) #0,1*sleep_time
            task2 = add.s(3,5) #0,1*sleep_time
            task3 = add.s(8,1) #0,1*sleep_time
            task4 = add.s(9,2) #0,1*sleep_time
            task5 = add.s(11,5) #0,1*sleep_time
            task6 = minus.s(1) #1*sleep_time
            task7 = minus.s(3) #1*sleep_time
            task8 = minus.s(2) #1*sleep_time
            task9 = nothing.s() #10*sleep_time
            task10 = nothing.s() #10*sleep_time

            task1.set(queue=queue)
            task2.set(queue=queue)
            task3.set(queue=queue)
            task4.set(queue=queue)
            task5.set(queue=queue)
            task6.set(queue=queue)
            task7.set(queue=queue)
            task8.set(queue=queue)
            task9.set(queue=queue)
            task10.set(queue=queue)

            # Create a chain
            my_chain = chain(task1, task2, task3, task4, task5, task6, task7, task8, task9, task10)



            task_chains.append(my_chain)

        end_time_creating = time()  
        total_time_creating = end_time_creating - start_time_creating
        print(f"I am here 7 - tasks created - {total_time_creating}")

        readys_chains = []

        for chain_obj in task_chains:
            readys_chains.append(chain_obj.apply_async())


        end_time_preparing = time()
        total_time_preparing = end_time_preparing - end_time_creating


        print(f"I am here 8 - tasks ready - {total_time_preparing}")


        for chain_obj in readys_chains:
            print(chain_obj.get())
       
        end_time_executing = time()
        total_time = end_time_executing - start_time_creating
        total_time_executing = end_time_executing - end_time_preparing
        print(f"I am here 10 - results - {round(total_time,1)}")

        self.write_output(f"Time creating: {total_time_creating}")
        self.write_output(f"Time preparing: {total_time_preparing}")
        self.write_output(f"Time executing: {total_time_executing}")
        self.write_output(f"Total time: {round(total_time,1)}")

        # Calculate average CPU usage
        #cpu_usages = [psutil.cpu_percent(interval=interval) for _ in range(int(total_time_executing // interval))]
        #avg_cpu = sum(cpu_usages) / len(cpu_usages)
        #self.write_output(f"Average CPU Usage : {avg_cpu}%")

        #end_ram_usage = psutil.virtual_memory().used

        #self.write_output(f"Peak RAM Usage : {(max(start_ram_usage, end_ram_usage) - start_ram_usage) / 1000000000} Go")

        return [total_time_creating, total_time_executing, 0,total_time]

def count_files(directory):

    files = os.listdir(directory)
    num_files = len(files)
    return num_files

def initialisation(dir):
    os.makedirs(dir, exist_ok=True)
    directory = f"{dir}_files_{count_files(dir)+1}"
    directory_path = os.path.join(dir, directory)
    os.makedirs(directory_path, exist_ok=True)
    return directory_path


import numpy as np
import threading
if __name__ == "__main__":
    print(threading.enumerate())
    output_directory_path = initialisation("outputs")
    plot_directory_path = initialisation("plots")
    settings = Settings()
    pool = "prefork" #if settings.pool else "solo"

    # Définir une palette de couleurs
    color_palette = plt.cm.viridis(np.linspace(0, 1, len(settings.concurrency_num)))


    '''fig, ax = plt.subplots()
    fig2, ax2 = plt.subplots()
    fig3, ax3 = plt.subplots()
    fig4, ax4 = plt.subplots()'''

    #for concurrency_num, color in zip(settings.concurrency_num, color_palette):
    existing_container_number = []
    existing_executing_time = []                
    existing_starting_time = []
    existing_creating_time = []
    existing_total_time = []

    for i in range(len(settings.container_num)):
        print("i : "+str(i))
        for j in range (len(settings.concurrency_num)):
            print("j : "+str(j))

            for k in range (len(settings.sleep_time)):
                print("k : "+str(k))
                print(i)
                print("BEGIN")
                stresstest = StressTestSuite(settings.container_num[i], settings.concurrency_num[j], settings.sleep_time[k], settings.task_amount, output_directory_path,pool)
                starting_time = stresstest.up(settings.container_num[i], settings.concurrency_num[i], settings.sleep_time[k])
                #start = time()
                results = stresstest.manyshort(int(settings.task_amount), settings.container_num, settings.concurrency_num)
                #end = time()
                creating_time = results[0]
                executing_time = results[1]
                peak_cpu = results[2]
                total_time = results[3]
                existing_container_number.append(settings.container_num[i])
                existing_executing_time.append(executing_time)
                existing_starting_time.append(starting_time)
                existing_creating_time.append(creating_time)
                existing_total_time.append(total_time)
                stresstest.down()
                print("END")

        # Tracer les points pour chaque valeur de concurrency_num
    '''ax.scatter(existing_container_number, existing_executing_time, label=f'Concurrency {settings.concurrency_num[i]}', marker='o', s=40)
    ax2.scatter(existing_container_number, existing_creating_time, label=f'Concurrency {settings.concurrency_num[i]}', marker='o', s=40)
    ax3.scatter(existing_container_number, existing_starting_time, label=f'Concurrency {settings.concurrency_num[i]}', marker='o', s=40)
    ax4.scatter(existing_container_number, existing_total_time, label=f'Concurrency {settings.concurrency_num[i]}', marker='o', s=40)


    ax.legend()
    ax.set_xlabel('Containers')
    ax.set_ylabel('Time execution')
    ax.set_title(f'Time execution for {settings.task_amount} tasks with {pool} pool and {0} seconds of sleep')
    ax.set_xlim(0, max(existing_creating_time)+1)
    ax.set_ylim(0, max(existing_executing_time))
    plot_file = f"{plot_directory_path}/plot_executing_{settings.task_amount}_tasks_{0}_sleep.png"
    plt.grid()
    plt.savefig(plot_file, format='png', dpi=300)
    plt.close()


    ax2.legend()
    ax2.set_xlabel('Containers')
    ax2.set_ylabel('Time creating')
    ax2.set_title(f'Time creating for {settings.task_amount} tasks with {pool} pool and {0} seconds of sleep')
    ax2.set_xlim(0, max(existing_creating_time)+1)
    ax2.set_ylim(0, max(existing_creating_time))
    plot_file2 = f"{plot_directory_path}/plot_creating_{settings.task_amount}_tasks_{0}_sleep.png"
    
    plt.grid()
    plt.savefig(plot_file2, format='png', dpi=300)
    plt.close()

    ax3.legend()
    ax3.set_xlabel('Containers')
    ax3.set_ylabel('Time building')
    ax3.set_title(f'Time building for {settings.task_amount} tasks with {pool} pool and 0 seconds of sleep')
    ax3.set_xlim(0, max(existing_creating_time)+1)
    ax3.set_ylim(0, max(existing_starting_time))
    plot_file3 = f"{plot_directory_path}/plot_building_{settings.task_amount}_tasks_0_sleep.png"
    plt.grid()  
    plt.savefig(plot_file3, format='png', dpi=300)
    plt.close()

    ax4.legend()
    ax4.set_xlabel('Containers')
    ax4.set_ylabel('Time total')
    ax4.set_title(f'Time total for {settings.task_amount} tasks with {pool} pool and 0 seconds of sleep')
    ax4.set_xlim(0, max(existing_creating_time)+1)
    ax4.set_ylim(0, max(existing_starting_time))
    plot_file4 = f"{plot_directory_path}/plot_total_{settings.task_amount}_tasks_0_sleep.png"
    plt.grid()  
    plt.savefig(plot_file4, format='png', dpi=300)
    plt.close()'''




#import docker

#client = docker.from_env()

#def get_maxrss(container_id):
#    stats = client.containers.get(container_id).stats(stream=False)
#    maxrss = stats['memory_stats']['max_usage'] / (1024 * 1024)  # Mo
#    return maxrss

