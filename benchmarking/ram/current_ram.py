import matplotlib.pyplot as plt
import psutil
from datetime import datetime, timedelta
from time import time

plt.ion()
max_ram = None

def plot_memory_usage(window_size=200, duration=10, interval=1):
    fig, ax = plt.subplots()
    ax.set_xlabel('current second')
    ax.set_ylabel('RAM used (%)')
    ax.set_ylim(0, 100)
    ax.set_title('RAM usage on real-time')

    line, = ax.plot([], [], 'b-')

    x_data = []
    y_data = []
    hundred_end = None
    start_time = datetime.now()
    while True:
        current_time = datetime.now()
        memory_percent = psutil.virtual_memory().percent
        global max_ram
        if max_ram is None or max_ram < memory_percent:
            max_ram = memory_percent
            print("Max RAM"+str(max_ram))
        #print(memory_percent)
        '''if memory_percent == 100 and hundred_start is None:
            hundred_start = time()
        elif memory_percent < 100 and hundred_start is not None:
            hundred_end = time()
            hundred_time = hundred_end - hundred_start
            print(hundred_time)'''
        elapsed_seconds = (current_time - start_time).total_seconds()
        x_data.append(elapsed_seconds)
        y_data.append(memory_percent)

        if len(x_data) > window_size:
            x_data.pop(0)
            y_data.pop(0)

        if elapsed_seconds > duration:
            ax.set_xlim(elapsed_seconds - duration, elapsed_seconds)

        line.set_data(x_data, y_data)
        ax.relim()
        ax.autoscale_view()

        plt.pause(0.01)
plot_memory_usage()
