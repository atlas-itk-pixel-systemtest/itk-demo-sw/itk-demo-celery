#!/usr/bin/env python

import threading
import time
from datetime import datetime
from benchmarking.app import app
from compose_script.settings import Settings
import matplotlib.pyplot as plt

class CeleryEventsHandler:
    
    def __init__(self, celery_app, stop_event):
        self._app = celery_app
        self._state = celery_app.events.State()
        self.event_counts = {
            "task-sent": 0,
            "task-received": 0,
            "task-started": 0,
            "task-succeeded": 0,
            "task-failed": 0
        }
        self.event_timers = {event_type: None for event_type in self.event_counts}
        self.event_last_timestamp = {event_type: None for event_type in self.event_counts}
        self.tasks_executed = 0
        self.stop_event = stop_event  
        self.first_timestamp_sent = None
        self.first_timestamp_received = None
        self.first_timestamp_started = None
        self.first_timestamp_succeeded = None
        self.event_count_sent = {}
        self.event_count_received = {}
        self.event_count_started = {}
        self.event_count_succeeded = {}

    def _event_handler(self, event):
        self._state.event(event)
        event_type = event["type"]
        settings = Settings()
        num = settings.task_amount
        if event_type in self.event_counts:
            self.event_counts[event_type] += 1
            current_timestamp = time.time()
            if self.event_timers[event_type] is None:
                if self.first_timestamp_sent is None:
                    self.first_timestamp_sent = current_timestamp
                elif self.first_timestamp_received is None:
                    self.first_timestamp_received = current_timestamp
                elif self.first_timestamp_started is None:
                    self.first_timestamp_started = current_timestamp
                elif self.first_timestamp_succeeded is None:
                    self.first_timestamp_succeeded = current_timestamp
                self.event_timers[event_type] = current_timestamp
            self.event_last_timestamp[event_type] = current_timestamp 


            if self.event_counts[event_type] == 1:
                self.event_timers[event_type] = current_timestamp 
            elif self.event_counts["task-succeeded"] == num:
                elapsed_time = current_timestamp - self.event_timers[event_type]
                self.handle_threshold_reached(event_type, elapsed_time)
            
            if event_type == "task-sent":
                elapsed_time = current_timestamp - self.first_timestamp_sent

                elapsed_seconds = int(elapsed_time % 60)
                self.event_count_sent[elapsed_seconds] = self.event_count_sent.get(elapsed_seconds, 0) + 1
            if event_type == "task-received":
                elapsed_time = current_timestamp - self.first_timestamp_received

                elapsed_seconds = int(elapsed_time % 60)
                self.event_count_received[elapsed_seconds] = self.event_count_received.get(elapsed_seconds, 0) + 1
            if event_type == "task-started":
                elapsed_time = current_timestamp - self.first_timestamp_started

                elapsed_seconds = int(elapsed_time % 60)
                self.event_count_started[elapsed_seconds] = self.event_count_started.get(elapsed_seconds, 0) + 1
            if event_type == "task-succeeded":
                elapsed_time = current_timestamp - self.first_timestamp_succeeded

                elapsed_seconds = int(elapsed_time % 60)
                self.event_count_succeeded[elapsed_seconds] = self.event_count_succeeded.get(elapsed_seconds, 0) + 1
        '''if event_type == "task-succeeded":
            self.tasks_executed += 1
            if self.tasks_executed >= 1850: 
                #print(f"{self.tasks_executed}")
                self.stop_event.set()
                #print(f"Nombre de tâches exécutées atteint. Arrêt du thread {threading.get_ident.__name__}.")'''

    def handle_threshold_reached(self, event_type, elapsed_time):
        print(f"Threshold reached for {event_type}. Count: {self.event_counts[event_type]}. Elapsed time: {elapsed_time} seconds.")
        print("Last timestamps for each event type:")
        for event_type, timestamp in self.event_last_timestamp.items():
            if timestamp is None :
                pass
            else:
                formatted_time = datetime.fromtimestamp(timestamp).strftime("%H:%M:%S.%f")[:-4]
                print(f"{event_type}: {formatted_time}")
        self.plot_hist()

    def plot_hist(self):
        events_to_plot = ["task-sent", "task-received", "task-started", "task-succeeded", "task-failed"]

        colors = ['blue', 'green', 'orange', 'red', 'purple']

        plt.figure(figsize=(10, 6))

        plt.bar(list(self.event_count_sent.keys()), list(self.event_count_sent.values()), width=0.2, align='center', label="task-sent", color='blue', alpha=0.7)
        plt.bar([x + 0.2 for x in self.event_count_received.keys()], list(self.event_count_received.values()), width=0.2, align='center', label="task-received", color='green', alpha=0.7)
        plt.bar([x + 0.4 for x in self.event_count_started.keys()], list(self.event_count_started.values()), width=0.2, align='center', label="task-started", color='orange', alpha=0.7)
        plt.bar([x + 0.6 for x in self.event_count_succeeded.keys()], list(self.event_count_succeeded.values()), width=0.2, align='center', label="task-succeeded", color='red', alpha=0.7)

        plt.legend()

        plt.xlabel('Time (seconds)')
        plt.ylabel('Number of Events')
        plt.title('Histogram of Task Events (Per Second)')

        plt.tight_layout()
        plt.show()


    def start_listening(self):
            with self._app.connection() as connection:
                handlers = {
                    "task-sent": self._event_handler,
                    "task-received": self._event_handler,
                    "task-started": self._event_handler,
                    "task-succeeded": self._event_handler,
                    "task-failed": self._event_handler
                }     

                recv = self._app.events.Receiver(connection, handlers=handlers)
                recv.capture(limit=None, timeout=8)

      

class MonitorThread(threading.Thread):
    def __init__(self, celery_app, interval=1):
        threading.Thread.__init__(self)
        self.celery_app = celery_app
        self.interval = interval
        self.stop_event = threading.Event()

    def run(self):
        events_handler = CeleryEventsHandler(self.celery_app,self.stop_event)
            #print("okok")
        events_handler.start_listening()
            #time.sleep(self.interval)

    #def stop(self):
    #    print("STOOOOP")
    #    self.join()

if __name__ == "__main__":
    monitor_thread = MonitorThread(app)
    monitor_thread.start()
    print(threading.enumerate())

    monitor_thread.join()
    print(threading.enumerate())
