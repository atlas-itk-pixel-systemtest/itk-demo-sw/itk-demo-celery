
# Benchmarking Results

## Results private PC Gerhard

OS: Ubuntu 20.04.6 LTS on Windows 10 x86_64
Kernel: 5.15.146.1-microsoft-standard-WSL2
Packages: 924 (dpkg)
Shell: bash 5.0.17
CPU: AMD Ryzen 9 5950X (32) @ 3.393GHz
GPU: 250f:00:00.0 Microsoft Corporation Device 008e
Memory: 7190MiB / 32055MiB

### Worker containers up/down

36 services (1 container with 1 worker)

docker compose up -d     ~ 8.4s
docker compose down      ~12.4s

36 replicas of 1 service (1 container with 1 worker)

docker compose up -d     ~11.7s
docker compose down      ~13.3s

## Results on wupp-charon

### Noop
#### 100000 Noop tasks without group/chain through only one container (Backend Redis)

- Sleep_time = 0s : - Concurrency = 1 : 175s
                    - Concurrency = 20 : 283s //TODO again
- Sleep_time = 0.1s : - Concurrency = 1 : 2h53min14s
                      - Concurrency = 20 : 8min35s

#### 100 Noop tasks without group/chain through only one container(Backend Redis)

- Sleep_time = 0s : - Concurrency = 1 : 1.9s
                    - Concurrency = 20 : 2.3s

- Sleep_time = 0.05s : - Concurrency = 1 : 7.1s
                       - Concurrency = 20 : 2.5s

- Sleep_time = 0.1s : - Concurrency = 1 : 12.2s
                      - Concurrency = 20 : 2.8s

- Sleep_time = 0.5s : - Concurrency = 1 : 52.1s
                      - Concurrency = 20 : 4.8s
                      
- Sleep_time = 1s : - Concurrency = 1 : 102.2s
                    - Concurrency = 20 : 7.3s

#### 100 Noop tasks without group/chain through 20 containers (Backend Redis)

- Sleep_time = 0s : - Concurrency = 1 : 18.8s #TODO AGAIN : first script error
                    - Concurrency = 20 : 79.8s 
- Sleep_time = 0.05s : - Concurrency = 1 : 15.1s
                       - Concurrency = 20 : 72.8s
- Sleep_time = 0.1s : - Concurrency = 1 : 4.4s
                      - Concurrency = 20 : 18.6s
- Sleep_time = 0.5s : - Concurrency = 1 : 21.3s
                      - Concurrency = 20 : 83.2s
- Sleep_time = 1s : - Concurrency = 1 : 17.8
                    - Concurrency = 20 : 77.7s

#### 1000 Noop tasks without group/chain through 20 containers (Backend Redis)

- Sleep_time = 0s : - Concurrency = 1 : 18.8s #TODO AGAIN : first script error
                    - Concurrency = 20 : 79.8s 
- Sleep_time = 0.05s : - Concurrency = 1 : 15.1s
                       - Concurrency = 20 : 72.8s
- Sleep_time = 0.1s : - Concurrency = 1 : 4.4s
                      - Concurrency = 20 : 18.6s
- Sleep_time = 0.5s : - Concurrency = 1 : 21.3s
                      - Concurrency = 20 : 83.2s
- Sleep_time = 1s : - Concurrency = 1 : 17.8
                    - Concurrency = 20 : 77.7s

#### 100 Noop tasks without group/chain through only one container(Backend RPC)

- Sleep_time = 0s : - Concurrency = 1 : 1.9s
                    - Concurrency = 20 : 13.2s 
- Sleep_time = 0.05s : - Concurrency = 1 : 7.1s
                       - Concurrency = 20 : 17.5s
- Sleep_time = 0.1s : - Concurrency = 1 : 12.18s
                      - Concurrency = 20 : 17.8s
- Sleep_time = 0.5s : - Concurrency = 1 : 52s
                      - Concurrency = 20 : 19.8s
- Sleep_time = 1s : - Concurrency = 1 : 102s
                    - Concurrency = 20 : 22.2s

#### 100 CPU100 tasks (cpu_time = sleep_time) without group/chain through only one container(Backend Redis)

- Sleep_time = 0s : - Concurrency = 1 : 9.9s
                    - Concurrency = 20 : 3s 
- Sleep_time = 0.05s : - Concurrency = 1 : 
                       - Concurrency = 20 : 
- Sleep_time = 0.1s : - Concurrency = 1 : 10s
                      - Concurrency = 20 : 3s
- Sleep_time = 0.5s : - Concurrency = 1 : 
                      - Concurrency = 20 : 
- Sleep_time = 1s : - Concurrency = 1 : 131.2s
                    - Concurrency = 20 : 13s

#### 100 Noop tasks chained by 10, each task on a different queue

##### Container 1, concurrency 1

Time of task sleeping : 0
    Time creating: 0.009
    Time preparing: 0.06
    Time executing: 1.87
Total time: 1.94

Time of task sleeping : 0.05
Total time: 7.4

Time of task sleeping : 0.1
Total time: 12.4

Time of task sleeping : 0.5
Total time: 52.4

Time of task sleeping : 1
Total time: 102.4

##### Container 1, concurrency 20


##### Container 20, concurrency 1


##### Container 20, concurrency 20

#### Container 36, Concurrency 4

#### 100 Noop tasks chained by 10, each task on a different queue

Time of task sleeping : 0
    Time creating: 0.06
    Time preparing: 0.3
    Time executing: 2.4
Total time: 2.8

Time of task sleeping : 0.05
Total time: 3.4

Time of task sleeping : 0.1
Total time: 3.6

Time of task sleeping : 0.5
Total time: 8.2

Time of task sleeping : 1
Total time: 13.1

Time of task sleeping : 10
Total time: 112

##### 1000 noop tasks chained by 10, 23.5s*time_sleep per chain (5*add(0.1) + 3*minus(1) + 2*nothing(10))


Time of chain sleeping : 2.35
    Time creating: 0.09
    Time preparing: 0.3
    Time executing: 5.4
Total time: 5.8s

Time of chain sleeping : 23.5
Total time: 35.8s

Time of chain sleeping : 235.0
Total time: 337s

##### 1000 noop tasks chained by 10, all the chain on the same queue, 23.5s*time_sleep per chain (5*add(0.1) + 3*minus(1) + 2*nothing(10))

Time of chain sleeping : 2.35
Total time: 5.3s

Time of chain sleeping : 23.5
Total time: 26.3s

Time of chain sleeping : 235.0
Total time: 237.2s

### I/O without chain

#### 100kB * 1000 tasks
##### Prefork
container 1 concurrency 1 : 1.7s
container 1 concurrency 20 : 1.5s
container 20 concurrency 1 : 1.6s
container 20 concurrency 20 : 1.5s

##### Threads
container 1 concurrency 1 : 29.2s
container 1 concurrency 20 : 1.3s
container 20 concurrency 1 : 1.4s
container 20 concurrency 20 : 1.4s

#### 1MB * 1000 tasks
##### Prefork
container 1 concurrency 1 : 4.9s
container 1 concurrency 20 : 1.5s
container 20 concurrency 1 : 1.6s
container 20 concurrency 20 : 1.6s

##### Threads
container 1 concurrency 1 : 35.5s
container 1 concurrency 20 : 1.6s
container 20 concurrency 1 : 1.8s
container 20 concurrency 20 : 1.5s

#### 100MB * 1000 tasks
##### Prefork
container 1 concurrency 1 : 387.1s
container 1 concurrency 20 : 98.1s
container 20 concurrency 1 : 108.6s
container 20 concurrency 20 : 126.8s

##### Threads
container 1 concurrency 1 : 382.2s
container 1 concurrency 20 : 104s
container 20 concurrency 1 : 98.2s
container 20 concurrency 20 : 119.4s

### 1440 tasks 36 containers 4 concurrency without chain

#### Prefork 
100kB : 2.2s
1MB :2.4s
100MB : 123.2s

#### Threads 
100kB : 2.2s
1MB : 2.4s
100MB : 164.9s

### I/O chained by 10, 1 chain executed in 1 queue

Creating tasks (task.s() + set a queue) : always less than 0.1s
Preparing tasks (apply_async) : always between 0.12 and 0.22

#### 100kB * 1000 tasks
##### Prefork
container 1 concurrency 1 : 2.5s
container 1 concurrency 20 : 1s
container 20 concurrency 1 : 1.4s
container 20 concurrency 20 : 1.8s

##### Threads
container 1 concurrency 1 : 23.2s
container 1 concurrency 20 : 2.3s
container 20 concurrency 1 : creating  time : 0.09s
                             preparing_time : 0.2s
                             executing_time : 0.7s
                             total_time = 1s
container 20 concurrency 20 : 1.7s

#### 1MB * 1000 tasks
##### Prefork
container 1 concurrency 1 : 6.5s
container 1 concurrency 20 : 1.2s
container 20 concurrency 1 : 2.4s
container 20 concurrency 20 : 2.8s

##### Threads
container 1 concurrency 1 : 27.1s
container 1 concurrency 20 : 2.8s
container 20 concurrency 1 : 2.2s
container 20 concurrency 20 : 1.9s

#### 100MB * 1000 tasks
##### Prefork
container 1 concurrency 1 : 417s
container 1 concurrency 20 : 100.3s
container 20 concurrency 1 : 99.8s
container 20 concurrency 20 : 100.7s

##### Threads
container 1 concurrency 1 : 422.2s
container 1 concurrency 20 : 119.4s // 88.5s

container 20 concurrency 1 : 102.8s
container 20 concurrency 20 : 86.4s


### 1440 tasks 36 containers 4 concurrency

#### Prefork 
100kB : 3.3s
1MB : 5.2s
100MB : 148.7s

#### Threads 
100kB : 1.9s
1MB : 3.4s
100MB : 139.8s

## RAM wupp-charon 
Prefork workers take more RAM than Threads Workers
### 36 containers 4 concurrency input_output: 100Mo

#### Prefork 
task_amount: 500
Initial RAM : 30.1%
Peak RAM 38.2%

task_amount: 1440
Initial RAM : 30.2%
Peak RAM 53.9%

#### Threads

task_amount: 500
Initial RAM : 26.2%
Peak RAM 34.1%

task_amount: 1440
Initial RAM : 26.2%
Peak RAM 49.3%

### 1 container 1 concurrency 100 tasks input_output: 100Mo

#### Prefork 
Initial RAM : 26.8%
Peak RAM 27.1%

#### Threads
Initial RAM : 26.7%
Peak RAM 27%

## Noop + I/O chained

### 1 container 1 concurrency 1000 tasks chained by by 10 (5*noop task(total : 0s) +I/O task writing the result)

#### threads
creating_time = 0.07s
preparing_time = 0.2s
executing_time = 15.8s
total_time = 16.1s

#### prefork
creating_time = 0.06s
preparing_time = 0.2s
executing_time = 3.7s
total_time = 4s

### 1 container 1 concurrency 1000 tasks chained by by 10 (5*noop task(total : 1.4s) +I/O task writing the result)

#### threads
creating_time = 0.06s
preparing_time = 0.2s
executing_time = 738s
total_time = 738.2s

#### prefork
creating_time = 0.06s
preparing_time = 0.2s
executing_time = 738s
total_time = 738.2s

### 1 container 1 concurrency 1000 tasks chained by by 10 (5*noop task(total : 1.4s) +I/O task writing the result)

#### threads
total_time = 7.9s

#### prefork
total_time = 9.6s

### 36 container 4 concurrency 5000 chained by 10 (5*noop task(total : 0s) +I/O task writing the result)

#### threads 
creating_time = 0.4s
preparing_time = 1.2s
executing_time = 4.8s
total_time = 6.5s

#### prefork
creating_time = 0.4s
preparing_time = 1.6s
executing_time = 7.6s
total_time = 9.6s

### 5000 chained by 10 (5*noop task(add) +I/O task writing the result)
#### prefork
container 1 concurrency 1
total_time = 12.7s

container 1 concurrency 20
total_time = 7.9s

container 20 concurrency 1
total_time = 12.5s

container 20 concurrency 20
total_time = 9.7s

#### Threads
container 1 concurrency 1
total_time = 65.5s

container 1 concurrency 20
total_time = 13s

container 20 concurrency 1
total_time = 6.4s

container 20 concurrency 20
total_time = 6.5s