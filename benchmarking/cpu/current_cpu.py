import matplotlib.pyplot as plt
import psutil
from datetime import datetime, timedelta
from time import time

plt.ion()

def plot_cpu_usage(window_size=200, duration=10, interval=1):
    fig, ax = plt.subplots()
    ax.set_xlabel('Seconde actuelle')
    ax.set_ylabel('Utilisation CPU (%)')
    ax.set_ylim(0, 100)
    ax.set_title('Utilisation CPU en temps réel')

    line, = ax.plot([], [], 'b-')

    x_data = []
    y_data = []
    hundred_start = None
    hundred_end = None
    start_time = datetime.now()
    while True:
        current_time = datetime.now()
        cpu_percent = psutil.cpu_percent(interval=interval)
        if cpu_percent == 100 and hundred_start is None:
            hundred_start = time()
        elif cpu_percent < 100 and hundred_start is not None :
            hundred_end = time()    
            hundred_time = hundred_end - hundred_start
            print(hundred_time)
        cpu_percent_per_core = psutil.cpu_percent(percpu=True)
        for i, percent in enumerate(cpu_percent_per_core):
            print(f"Core {i+1}: {percent}%")
        elapsed_seconds = (current_time - start_time).total_seconds()
        x_data.append(elapsed_seconds)
        y_data.append(cpu_percent)

        if len(x_data) > window_size:
            x_data.pop(0)
            y_data.pop(0)

        if elapsed_seconds > duration:
            ax.set_xlim(elapsed_seconds - duration, elapsed_seconds)

        line.set_data(x_data, y_data)
        ax.relim()
        ax.autoscale_view()

        plt.pause(0.01)


plot_cpu_usage()
