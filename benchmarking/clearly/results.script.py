import subprocess

# Exécuter la commande clearlycli.capture() dans le conteneur Docker et récupérer la sortie
command = "docker exec clearly-client python -c 'from clearlycli import clearlycli; print(clearlycli.capture())'"
output = subprocess.check_output(command, shell=True, universal_newlines=True)

# Stocker les sorties dans un fichier
output_file = "output.txt"
with open(output_file, "w") as file:
    file.write(output)

print("Les sorties ont été stockées dans", output_file)
