import logging
import sys
import signal
import click
from celery.bin.base import CeleryCommand, handle_preload_options

from demice.app import Demice

logger = logging.getLogger(__name__)

def sigterm_handler(signum, _):
    logger.info('%s detected, shutting down', signum)
    sys.exit(0)

@click.command(cls=CeleryCommand,
               context_settings={
                #    'ignore_unknown_options': True
               })
@click.pass_context
@handle_preload_options
def demice(ctx, **kwargs):
    """D e M i C e - stress testing and benchmarking client"""

    print("command.demice")

    # def __init__(self, app=None, *args, **kwargs):
    #     if app is None or app.main == "default":
    #         app = demice_app

    app = ctx.obj.app

    print(app)
    # app.loader.import_default_modules()

    demice_app = Demice(capp=app)

    signal.signal(signal.SIGTERM, sigterm_handler)
    
    if not ctx.obj.quiet:
        # print_banner(app, 'ssl_options' in settings)
        print("D e M i C e")

    try:
        demice_app.start()
    except (KeyboardInterrupt, SystemExit):
        pass

    #     app.set_current()
    #     app.set_default()
    #     super(demice, self).__init__(app, *args, **kwargs)

    # def run(self, *names, **options):
    #     try:
    #         return self.run_suite(names, **options)
    #     except KeyboardInterrupt:
    #         print("### interrupted by user: exiting...", file=self.stdout)

    # def run_suite(self, names, suite,
    #               block_timeout=None, no_color=False, **options):
    #     return symbol_by_name(suite)(
    #         self.app,
    #         block_timeout=block_timeout,
    #         no_color=no_color,
    #     ).run(names, **options)

    # def get_options(self):
    #     print("setting options")
    #     return (
            # Option('-i', '--iterations', type='int', default=50,
            #        help='Number of iterations for each test'),
            # Option('-n', '--numtests', type='int', default=None,
            #        help='Number of tests to execute'),
            # Option('-o', '--offset', type='int', default=0,
            #        help='Start at custom offset'),
            # Option('--block-timeout', type='int', default=30 * 60),
            # Option(
            #     "-l",
            #     "--list",
            #     action="store_true",
            #     dest="list_all",
            #     default=False,
            #     help="List all tests",
            # ),
            # Option('-r', '--repeat', type='float', default=0,
            #        help='Number of times to repeat the test suite'),
            # Option('-g', '--group', default='all',
            #        help='Specify test group (all|green|redis)'),
            # Option('--diag', default=False, action='store_true',
            #        help='Enable diagnostics (slow)'),
            # Option('-J', '--no-join', default=False, action='store_true',
            #        help='Do not wait for task results'),
            # Option('-S', '--suite',
            #        default=self.app.demice_suite,
            #        help='Specify test suite to execute (path to class)'),
        # )


# def main(argv=None):
#     return demice().main(argv)


# if __name__ == "__main__":
#     main()
