import sys
from celery.bin.celery import main as _main, celery
from demice.command import demice


def main():
    celery.add_command(demice)
    sys.exit(_main())


if __name__ == "__main__":
    main()
