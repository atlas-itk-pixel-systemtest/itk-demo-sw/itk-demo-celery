import sys
import logging
from typing import Any

import celery
from celery import signals
import click

from demice.templates import template_names, use_template

logger = logging.getLogger(__name__)


class Demice():
    """The ``celery demice`` program."""

#     demice_suite = "demice.suites.default:Default"
    template_selected = False

    def __init__(self, capp, options=None, **kwargs):

        self.capp = capp 
        # or celery.Celery()

#         print(f"[demice] {args} {kwargs}")

#         self.template = kwargs.pop("template", None)
#         super(App, self).__init__(*args, **kwargs)
        self.capp.user_options["preload"].add(
            click.Option(
                ("-Z", "--template"),
                default="default",
                # type=str,
                help=f"Configuration template to use: {template_names()}",
            ),
        )

    def start(self):
        print("starting -DeMiCe-")
        self.capp.finalize()


    @signals.user_preload_options.connect
    def on_preload_parsed(self, options, **kwargs):
        print("on_preload_parsed")
        self.capp.on_configure.connect(self._maybe_use_default_template)

#     def on_preload_parsed(self, options=None, **kwargs):
#         self.use_template(options["template"])

    def use_template(self, name="default"):
        if self.template_selected:
            raise RuntimeError("App already configured")
        self.template_selected = True
        use_template(self, name)

    def _maybe_use_default_template(self, **kwargs):
        if not self.template_selected:
            self.use_template("default")



# app = App("demice", set_as_current=False)
