import os

from kombu import Queue
from kombu.utils import symbol_by_name

DEMICE_TRANS = os.environ.get('DEMICE_TRANS', False)
default_queue = 'c.stress.trans' if DEMICE_TRANS else 'c.stress'
DEMICE_QUEUE = os.environ.get('DEMICE_QUEUE', default_queue)

templates = {}


def template(name=None):

    def _register(cls):
        templates[name or cls.__name__] = '.'.join([__name__, cls.__name__])
        return cls
    return _register



def use_template(app, template='default'):
    if isinstance(template, list) and len(template) == 1:
        # weird argparse thing
        template = template[0]
    template = template.split(',')

    # mixin the rest of the templates when the config is needed
    @app.on_after_configure.connect(weak=False)
    def load_template(sender, source, **kwargs):
        mixin_templates(template[1:], source)

    app.config_from_object(templates[template[0]])

def mixin_templates(templates, conf):
    return [mixin_template(template, conf) for template in templates]


def mixin_template(template, conf):
    cls = symbol_by_name(templates[template])
    conf.update(dict(
        (k, v) for k, v in items(vars(cls))
        if not k.startswith('_')
    ))


def template_names():
    return ', '.join(templates)


@template()
class default(object):
    accept_content = ['json']
    broker_url = os.environ.get('DEMICE_BROKER', 'pyamqp://')
    broker_heartbeat = 30
    result_backend = os.environ.get('DEMICE_BACKEND', 'rpc://')
    result_serializer = 'json'
    result_persistent = True
    result_expires = 300
    max_cached_results = 100
    default_queue = DEMICE_QUEUE
    imports = ['demice.tasks']
    track_started = True
    queues = [
        Queue(DEMICE_QUEUE,
              durable=not DEMICE_TRANS,
              no_ack=DEMICE_TRANS),
    ]
    task_serializer = 'json'
    task_publish_retry_policy = {
        'max_retries': 100,
        'interval_max': 2,
        'interval_step': 0.1,
    }
    task_protocol = 2
    if DEMICE_TRANS:
        default_delivery_mode = 1
    celeryd_prefetch_multiplier = int(os.environ.get('DEMICE_PREFETCH', 10))


@template()
class redis(default):
    broker_url = os.environ.get('DEMICE_BROKER', 'redis://')
    broker_transport_options = {
        'fanout_prefix': True,
        'fanout_patterns': True,
    }
    result_backend = os.environ.get('DEMICE_BACKEND', 'redis://')


@template()
class redistore(default):
    RESULT_BACKEND = 'redis://'


@template()
class acks_late(default):
    acks_late = True


@template()
class pickle(default):
    accept_content = ['pickle', 'json']
    task_serializer = 'pickle'
    result_serializer = 'pickle'


@template()
class confirms(default):
    broker_url = 'pyamqp://'
    broker_transport_options = {'confirm_publish': True}


@template()
class events(default):
    send_events = True
    send_task_sent_event = True


@template()
class execv(default):
    celeryd_force_execv = True

