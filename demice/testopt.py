from celery import Celery
from celery import signals
from click import Option
# from celery.bin.base import CeleryOption

app = Celery()

app.user_options["preload"].add(
    Option(
        ("-Z", "--template"), default="default", help="Configuration template to use."
    )
)

# def add_preload_options(parser):
#     parser.add_argument(
#         "-Z", "--template", default="default", help="Configuration template to use.",
#     )

# app.user_options["preload"].add(add_preload_options)

@signals.user_preload_options.connect
def on_preload_parsed(options, **kwargs):
    print(f"on_preload_parsed {options}")
    # use_template(options["template"])

