#!/usr/bin/env python

import json

from rich import print

# from itk_demo_celery.celery.celery import app
from celery.app import app_or_default
app = app_or_default()

class CeleryEventsHandler:
    def __init__(self, celery_app):
        self._app = celery_app
        self._state = celery_app.events.State()

    def _event_handler(self, event):
        self._state.event(event)
        
        # if event["type"] != "worker-heartbeat":
        #     print(json.dumps(event, default=str))
        
        if event["type"].startswith("task-"):
            task = self._state.tasks.get(event["uuid"])
            print(task)
            # print(json.dumps(vars(task), default=str))
        # handler(self, event)
            
        print(self._state)

    def start_listening(self):
        with self._app.connection() as connection:
            recv = self._app.events.Receiver(
                connection, handlers={"*": self._event_handler}
            )
            recv.capture(limit=None, timeout=3600)


if __name__ == "__main__":
    events_handler = CeleryEventsHandler(app)
    events_handler.start_listening()
#