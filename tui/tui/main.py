import logging
from textual.app import App, ComposeResult
from textual.widgets import Header, Footer, Static, DataTable, TabbedContent, Markdown, TabPane

# from textual.containers import ScrollableContainer, VerticalScroll
# from textual.logging import TextualHandler
# from textual.binding import Binding, BindingType
# from textual import log
from textual.events import Key

### Use logging with Textual.
# logging.basicConfig(
#     level="NOTSET",
#     handlers=[TextualHandler()],
# )

ROWS = [
    ("UUID", "Worker", "Task", "Time", "State"),
]


class TaskList(DataTable):
    """The celery task list"""

    # BINDINGS = [
    #     Binding("up", "scroll_up", "Scroll Up", show=False),
    #     Binding("down", "scroll_down", "Scroll Down", show=False),
    #     Binding("left", "scroll_left", "Scroll Up", show=False),
    #     Binding("right", "scroll_right", "Scroll Right", show=False),
    #     Binding("home", "scroll_home", "Scroll Home", show=False),
    #     Binding("end", "scroll_end", "Scroll End", show=False),
    #     Binding("pageup", "page_up", "Page Up", show=False),
    #     Binding("pagedown", "page_down", "Page Down", show=False),
    # ]

    def compose(self) -> ComposeResult:
        """Create task list widgets"""
        return super().compose()


# class TopTabs(TabbedContent)

WORKERS = """
# Workers
Celery workers online
"""
TASKS = """
# Tasks
Celery Tasks
"""

EVENTS = """
# Events
Celery Events Dump
"""

BROKER = """
# Broker
MQ Broker Info
"""

WORKER_POOL="""
# Pool
Worker pool options
"""
WORKER_BROKER="""
# Broker
Broker options
"""
WORKER_QUEUES="""
# Queues
Active queues
"""
WORKER_TASKS="""
# Tasks
Processed tasks
"""
WORKER_LIMITS="""
# Limits
Task limits
"""
WORKER_CONFIG="""
# Config
Configuration Options
"""
WORKER_SYSTEM="""
# System
System usage statistics
"""
WORKER_OTHER="""
# Other
Other statistics
"""

class CeleryTui(App):
    """A Textual app to manage celery apps."""

    CSS_PATH = "celerytui.css"
    TITLE = "Celery Textual TUI"
    SUB_TITLE = "celery events successor"
    BINDINGS = [
        # celery event legacy bindings
        ("d", "toggle_dark", "Toggle dark mode"),
        ("j", "toggle_down", "down"),
        ("k", "toggle_up", "up"),
        ("c", "toggle_revoke", "revoke"),
        ("t", "toggle_traceback", "traceback"),
        ("r", "toggle_result", "result"),
        ("i", "toggle_info", "info"),
        ("l", "toggle_rate_limit", "rate limit"),
        # tabbed content
        ("w", "show_tab('workers')", "Workers"),
        ("b", "show_tab('tasks')", "Tasks"),
        ("b", "show_tab('events')", "Events"),
        ("b", "show_tab('broker')", "Broker"),

    ]

    def compose(self) -> ComposeResult:
        """Create child widgets for the app."""
        yield Header()
        yield Footer()
        # yield TaskList() #ScrollableContainer(TaskList())
        with TabbedContent(initial='tasks'):
            with TabPane("Workers", id='workers'):
                yield Markdown(WORKERS)
                with TabbedContent(initial='pool'):
                    with TabPane("Pool", id='pool'):
                        yield Markdown(WORKER_POOL)
                    with TabPane("Broker", id='broker'):
                        yield Markdown(WORKER_BROKER)
                    with TabPane("Queues", id='queues'):
                        yield Markdown(WORKER_QUEUES)
                    with TabPane("Tasks", id='tasks'):
                        yield Markdown(WORKER_TASKS)
                    with TabPane("Limits", id='limits'):
                        yield Markdown(WORKER_LIMITS)
                    with TabPane("Config", id='config'):
                        yield Markdown(WORKER_CONFIG)
                    with TabPane("System", id='system'):
                        yield Markdown(WORKER_SYSTEM)
                    with TabPane("Other", id='other'):
                        yield Markdown(WORKER_OTHER)
            with TabPane("Tasks", id='tasks'):
                yield Markdown(TASKS)
            with TabPane("Events", id='events'):
                yield Markdown(EVENTS)
            with TabPane("Broker", id='broker'):
                yield Markdown(BROKER)

    def on_mount(self) -> None:
        self.log("Starting Celery TUI")

        # table = self.query_one(TaskList)
        # table.add_columns(*ROWS[0])
        # table.add_rows(ROWS[1:])

    def on_key(self, event: Key):
        self.title = event.key
        self.sub_title = f"You just pressed {event.key}!"

    def action_toggle_dark(self) -> None:
        """An action to toggle dark mode."""
        self.dark = not self.dark


def main():
    app = CeleryTui()
    app.run()


if __name__ == "__main__":
    main()
