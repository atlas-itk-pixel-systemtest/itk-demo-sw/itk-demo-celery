
import sys

__all__ = ('main',)

def main() -> None:
    """Entrypoint"""
    from tui.main import main as _main
    sys.exit(_main())

if __name__ == '__main__':
    main()

