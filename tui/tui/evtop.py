from collections.abc import Callable, Iterable, Mapping
import sys
import time
from typing import Any

# from celery import Celery
from celery import VERSION_BANNER, states
from celery.app import app_or_default
from rich import print
from time import sleep
import threading
from datetime import datetime

class CeleryMonitor:

    def __init__(self, state, app):
        self.app = app
        self.state = state
        self.lock = threading.RLock()

    # def workers(self):
    #     with self.lock:
    #         print(" ".join([repr(w) for w in state.workers]))

    # def tasks(self):
    #     with self.lock:
    #         for tid in self.state.tasks:
    #             print(tid)
    #             # details(task)
    #             # details(state, task_id)
    #             task = state.tasks[tid]
    #             info = task.info()
    #             print(info)

    def display_task_row(self,task):
        timestamp = datetime.utcfromtimestamp(
            task.timestamp or time(),
        )
        timef = timestamp.strftime('%H:%M:%S')
        hostname = task.worker.hostname if task.worker else '*NONE*'
        line = f'{task.uuid} {task.name} {hostname} {timef} {task.state}'
        print(line)

    def selection_info(self):
        if not self.selected_task:
            return

        task = self.state.tasks[self.selected_task]
        info = task.info(extra=['state'])
        infoitems = [
            ('args', info.pop('args', None)),
            ('kwargs', info.pop('kwargs', None))
        ] + list(info.items())
        for key, value in infoitems:
            if key is None:
                continue
            value = str(value)
            keys = key + ': '
            print(f"{keys}{value}")

    @property
    def limit(self):
        return 40

    @property
    def tasks(self):
        return list(self.state.tasks_by_time(limit=self.limit))

    @property
    def workers(self):
        return [hostname for hostname, w in self.state.workers.items()
                if w.alive]

    # def dump():
    #     with self.lock:

def capture_events(app, state):
    def on_connection_error(exc, interval):
        print(
            "Connection Error: {!r}.  Retry in {}s.".format(exc, interval),
            file=sys.stderr,
        )

    while 1:
        print("-> evtop: starting capture...", file=sys.stderr)
        with app.connection_for_read() as conn:
            try:
                conn.ensure_connection(
                    on_connection_error, app.conf.broker_connection_max_retries
                )
                recv = app.events.Receiver(conn, handlers={"*": state.event})
                # display.resetscreen()
                # display.init_screen()

                print(state)

                recv.capture()
            except conn.connection_errors + conn.channel_errors as exc:
                print(f"Connection lost: {exc!r}", file=sys.stderr)


class DisplayThread(threading.Thread):
    def __init__(self, monitor):
        self.monitor = monitor
        self.app = monitor.app
        self.state = monitor.state
        self.shutdown = False
        self.lock = threading.RLock()
        super().__init__()

    def run(self):
        while not self.shutdown:
            # print(self.state)
            # with self.lock:
            print(monitor.workers)
            for (uuid, task) in monitor.tasks:
                # print(vars(task))
                monitor.display_task_row(task)
                monitor.selected_task = uuid
                monitor.selection_info()
            sleep(1)


if __name__ == "__main__":
    # app = Celery(broker='amqp://guest:guest@localhost:5672//')
    app = app_or_default()
    state = app.events.State()
    monitor = CeleryMonitor(state, app)
    refresher = DisplayThread(monitor)
    refresher.start()

    print(VERSION_BANNER)
    print(app)

    try:
        capture_events(app, state)
    except Exception:
        refresher.shutdown = True
        refresher.join()
        raise
    except (KeyboardInterrupt, SystemExit):
        refresher.shutdown = True
        refresher.join()

    sys.exit(0)
