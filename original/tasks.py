from celery import Celery

app = Celery("tasks", broker="amqp://") #redis://127.0.0.1:6379/0")

# transport_path="/home/gbrandt/itk-demo-sw/itk-demo-celery/test"
# app = Celery(__name__)
# app.conf.update({
#     'broker_url': 'filesystem://',
#     'broker_transport_options': {
#         'data_folder_in': transport_path+'/out',
#         'data_folder_out': transport_path+'/out',
#         'data_folder_processed': transport_path+'/processed'
#     },
#     'imports': ('tasks',),
#     'result_persistent': False,
#     'task_serializer': 'json',
#     'result_serializer': 'json',
#     'accept_content': ['json']
# })

@app.task
def add(x, y):
    return x + y

