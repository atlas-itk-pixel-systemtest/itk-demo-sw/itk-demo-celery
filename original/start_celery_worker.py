#!/usr/bin/env python

# How to start celery worker from python
# List of options for the optionDict: https://docs.celeryq.dev/en/stable/reference/cli.html#celery-worker
# celery.apps.multi API reference: https://docs.celeryq.dev/en/stable/reference/celery.apps.multi.html
from celery.apps import multi
import subprocess

hostname = subprocess.check_output(
    "hostname", stderr=subprocess.STDOUT, shell=True, text=True
)
optionDict = {}

optionDict["w1"] = {
    "-A": "itk_demo_celery",
    "-c": "5",
    "-Q": "celery,queue1",
    "--pidfile": "/home/itkpixdaq/itk-demo-sw/itk-demo-celery/var/run/celery/%n.pid",
    "--logfile": "/home/itkpixdaq/itk-demo-sw/itk-demo-celery/var/log/celery/%n%I.log",
    "--loglevel": "info",
    "-E": " ",
}

optionDict["w2"] = {
    "-A": "itk_demo_celery",
    "-c": "3",
    "-Q": "queue2",
    "--pidfile": "/home/itkpixdaq/itk-demo-sw/itk-demo-celery/var/run/celery/%n.pid",
    "--logfile": "/home/itkpixdaq/itk-demo-sw/itk-demo-celery/var/log/celery/%n%I.log",
}

w1 = multi.Node("w1@" + hostname, options=optionDict["w1"])
w2 = multi.Node("w2@" + hostname, options=optionDict["w2"])


workers = multi.Cluster([w1, w2])

workers.start()
