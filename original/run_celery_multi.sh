#!/usr/bin/env bash

_root=$(dirname "${BASH_SOURCE[0]}")
_root=$(cd "${_root}/" >/dev/null 2>&1 && pwd)
m_cmd=${1:-start}
n_worker=${2:-4}
if ((n_worker >= 1 && n_worker <= 10)); then
  n_worker=$((n_worker))
fi
# echo "$n_worker"
# CELERY_WORKDIR=$_root/itk_demo_celery
# CELERY_NODE_NAME=card #DAQ card basename
CELERY_PID_FILE="$_root/var/run/celery/%n.pid"
CELERY_LOG_FILE="$_root/var/log/celery/%n%I.log"

mkdir -p "$CELERY_PID_FILE"
mkdir -p "$CELERY_LOG_FILE"

qnames=""
q=""
for ((i = 1; i <= n_worker; i++)); do
  qnames="$qnames -Q:${i} card${i}"
  q="$q card${i}" #@localhost"
done
echo "command: $m_cmd"
echo "nodes:  $q"
echo "queues: $qnames"
# --workdir="$CELERY_WORKDIR" \
run_celery() {
  celery -A itk_demo_celery \
    multi $m_cmd $q -l INFO $qnames \
    --pidfile="$CELERY_PID_FILE" \
    --logfile="$CELERY_LOG_FILE" \
    -P solo -c 1 -E
}
# declare -f run_celery
# type run_celery
run_celery
# echo
# echo "      poetry run celery flower --broker=amqp://$q --port=5555"
# echo
