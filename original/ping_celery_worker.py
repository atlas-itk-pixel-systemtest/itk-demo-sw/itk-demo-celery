#!/usr/bin/env python

# How to check if celery workers are running from python
# celery.app.control API reference: https://docs.celeryq.dev/en/stable/reference/celery.app.control.html
from itk_demo_celery.cel.app import app
import subprocess

hostname = subprocess.check_output(
    "hostname", stderr=subprocess.STDOUT, shell=True, text=True
)
hostname = hostname.split("\n")[0]


def ping_all():
    pong = app.control.ping()
    return pong


def ping_specific_workers(names):
    pong = app.control.ping(names)
    return pong


workernames = ["w1@" + hostname, "w2@" + hostname]
pong = ping_specific_workers(workernames)
print(pong)
