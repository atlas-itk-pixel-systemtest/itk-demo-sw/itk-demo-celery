# itk-demo-celery

Simple example running N mock up DAQ tasks on one node

# Installation

1. Clone this git repo.

```shell
git clone https://gitlab.cern.ch/itk-demo-sw/itk-demo-celery.git
```

2. Navigate to the root directory of the project 

```
cd itk-demo-celery/original
```


3. Locally install Python toolchain and package dependencies.

```shell
   demi python install
```

4. Activate the venv

```shell
   poetry shell
```


5. Download and start the rabbitmq server and flower monitoring tool

   ```shell
      docker-compose up
   ```

   - The RabbitMQ management console can be found at http://localhost:15672
   - The flower UI to monitor the system can be found at http://localhost:5555

# Running

1. Start the celery work queues and flower server

   ```shell
   ./run_celery_multi.sh
   ```

   the script takes all the commands of `celery multi`, eg. to shut down do:

   ```shell
   ./run_celery_multi.sh stop
   ```

2. Submit a scan to the workers

   ```shell
   ./python/run_daq.py
   ```


