#!/usr/bin/env python

import json

# from celery.utils.log import get_logger
# logger = get_logger(__name__)
import logging
import os
from inspect import signature
from pprint import pprint

from celery import group, signature

from itk_demo_celery.cel.app import app
from itk_demo_celery.cel.tasks import cleanup, error_handler

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

cards = 4
configdir = os.path.dirname(os.path.abspath(__file__))
configfile = configdir + "/data/scanConfig.json"
resultfile = configdir + "/data/scanResult.json"
config = None
result = None

with open(configfile, "r") as f:
    config = json.load(f)

with open(resultfile, "r") as f:
    result = json.load(f)

task_list = list()


def on_raw_message(body):
    print(body)
    # if body["status"] == "PROGRESS":
    #     result = body["result"]
    #     print(body["task_id"], result["stage"])


def send_tasks():
    for i in range(1, cards + 1):
        name = "card%d" % i
        logger.info(f"Start Card {name}")
        task = signature(
            "itk_demo_celery.cel.tasks.run",
            ("config.json", "result.json"),
            kwargs={},
            queue=name,
        )
        task_list.append(task)
        logger.info("signature:")
        pprint(dict(task))

    task_group = group(task_list)
    async_result = task_group.apply_async()
    logger.info(f"Sent {len(task_list)} task(s)")

    return async_result


# when running outside Docker need to connect to bound ports on localhost
# app.conf.broker_url = "pyamqp://guest:guest@localhost",
# app.conf.result_backend = "redis://localhost:6379/0"

try:
    async_result = send_tasks()

    res = async_result.get(
        on_message=on_raw_message,
        propagate=False,
    )

    print(res)
except Exception as e:
    logger.error(f"Sending task raised: {type(e).__name__} {e.args}")
